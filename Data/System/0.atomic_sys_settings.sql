USE Atomic

--创建表。
/*******************************************************************************************************************/
---------------------------------------------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[atomic_sys_settings]') AND type in (N'U'))
	DROP TABLE [dbo].[atomic_sys_settings]
GO

--用户基本信息表。
CREATE TABLE atomic_sys_settings(
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,		--编号。
	[Name] [nvarchar](50) NOT NULL,						--属性名。
	[Value] [nvarchar](200) NOT NULL,					--属性值。
)

---------------------------------------------------------------------------------------------------------------------

--创建初始化数据。
/*******************************************************************************************************************/

INSERT INTO atomic_sys_settings(Name, Value) VALUES ('Name', 'Atomic')
INSERT INTO atomic_sys_settings(Name, Value) VALUES ('Theme', 'Default')
INSERT INTO atomic_sys_settings(Name, Value) VALUES ('PageSize', '10')
INSERT INTO atomic_sys_settings(Name, Value) VALUES ('HomePage', '/')
INSERT INTO atomic_sys_settings(Name, Value) VALUES ('LoginPage', '/account/login')
INSERT INTO atomic_sys_settings(Name, Value) VALUES ('Error403Page', '/error/403')
INSERT INTO atomic_sys_settings(Name, Value) VALUES ('Error404Page', '/error/404')
INSERT INTO atomic_sys_settings(Name, Value) VALUES ('Error500Page', '/error/500')
INSERT INTO atomic_sys_settings(Name, Value) VALUES ('ImageUrl', '/')
INSERT INTO atomic_sys_settings(Name, Value) VALUES ('IconDefaultUrl', 'http://localhost/Content/Default/Icon.jpg')
INSERT INTO atomic_sys_settings(Name, Value) VALUES ('IconAreaWidth', 400)
INSERT INTO atomic_sys_settings(Name, Value) VALUES ('IconAreaHeight', 300)
INSERT INTO atomic_sys_settings(Name, Value) VALUES ('SaveIconPath', '')

---------------------------------------------------------------------------------------------------------------------

--分页存储过程。
IF EXISTS(SELECT * FROM sysobjects WHERE id=OBJECT_ID('P_ItemPage') and xtype='p')
	DROP PROCEDURE [dbo].[P_ItemPage]
GO

CREATE PROCEDURE [dbo].[P_ItemPage] 
	@pageIndex INT = 1,						--指定当前为第几页
	@pageSize INT = 10,						--每页多少条记录
	@tableName NVARCHAR(50),				--表名
	@primary NVARCHAR(50),					--关键字
	@fields NVARCHAR(500) = '*',			--搜索出的字段
	@sqlWhere NVARCHAR(500) = Null,			--条件语句(不用加where)
	@orderby NVARCHAR(8) = 'DESC',			--排序方式(desc：倒序，asc：顺序)
	@top INT = 0							--搜索前多少条数据。
AS
BEGIN

--SQL 分页脚本。
DECLARE @sql NVARCHAR(4000)
--SQL 搜索脚本。
DECLARE @sqlSearch NVARCHAR(2000)
--条件。
DECLARE @where NVARCHAR(500)
--开始位置行数。
DECLARE @startRecord INT
--结束位置行数。
DECLARE @endRecord INT
--返回前多少条数据。
DECLARE @returnTop NVARCHAR(20)
--总页数
DECLARE @totalPage INT

IF(@pageIndex < 1)
BEGIN
	SET @pageIndex = 1
END

IF(@pageSize < 1)
BEGIN
	SET @pageSize = 10
END

--设置开始位置行数。
SET @startRecord = (@pageIndex - 1) * @pageSize
--设置结束位置行数。
SET @endRecord = @startRecord + @pageSize + 1

IF(@top IS NOT NULL AND @top > 0)
BEGIN
	SET @returnTop = 'TOP ' + STR(@top)
END
ELSE
BEGIN
	SET @returnTop = '';
END

IF(@sqlWhere IS NOT NULL AND @sqlWhere <> '')
BEGIN
	SET @where = ' WHERE ' + @sqlWhere
END
ELSE
BEGIN
	SET @where = '';
END

IF(@pageIndex = 1)
BEGIN
	SET @sql = 'SELECT TOP ' + STR(@pageSize) + ' ' + @fields + ' FROM ' + @tableName + ' with(nolock) ' + @where
	EXEC(@sql)
END
ELSE
BEGIN
	SET @sqlSearch = 'SELECT ' + @returnTop + ' ' + @fields + ' FROM ' + @tableName + ' with(nolock) ' + @where
	SET @sql = 'SELECT ' + @fields + ' FROM(SELECT ' + @fields + ', ROW_NUMBER() OVER(ORDER BY ' + @primary + ' ' + @orderby + ') AS rowNumber FROM (' + @sqlSearch + ') AS t) t1 WHERE rowNumber > ' + STR(@startRecord) + ' AND rowNumber < ' + STR(@endRecord)
	EXEC(@sql)
END

END
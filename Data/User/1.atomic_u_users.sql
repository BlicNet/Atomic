USE Atomic

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[atomic_u_users]') AND type in (N'U'))
	DROP TABLE [dbo].[atomic_u_users]
GO

--用户基本信息表。
CREATE TABLE atomic_u_users(
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,					--用户编号。
	[AccountNumber] [varchar](20) NOT NULL DEFAULT(''),				--账号。
	[Password] [varchar](32) NOT NULL DEFAULT(''),					--密码。
	[Name] [nvarchar](50) NOT NULL DEFAULT(''),						--昵称。
	[Email] [varchar](150) NOT NULL DEFAULT(''),					--电子邮箱。
	[PasswordQuestion] [nvarchar](50) NOT NULL DEFAULT(''),			--密码保护问题。
	[PasswordAnswer] [nvarchar](50) NOT NULL DEFAULT(''),			--密码保护答案。
	[Image] [varchar](200) NOT NULL DEFAULT(''),					--头像。
	[Birthday] [datetime] NOT NULL DEFAULT('1970-01-01'),			--生日。
	[Sex] [int] NOT NULL DEFAULT(0),								--性别(0：保密，1：男，2：女。)。
	[Description] [nvarchar](100) NOT NULL  DEFAULT(''),			--简介。
	[RegisterTime] [datetime] NOT NULL DEFAULT GETDATE(),			--注册时间。
	[Disable] [int] NOT NULL DEFAULT(0),							--是否可用（即：是否被封号），0为可用，1为不可用。
	[Roles] [varchar](1000) NOT NULL DEFAULT('')					--角色列表字符串。（例如：1,2,4,5,7,8。其中，数字是角色编号）
)
USE Atomic

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[atomic_u_roles]') AND type in (N'U'))
	DROP TABLE [dbo].[atomic_u_roles]
GO

--角色表。
CREATE TABLE atomic_u_roles(
	[Id] [int] NOT NULL PRIMARY KEY,							--角色编号。
	[Name] [nvarchar](20) NOT NULL DEFAULT(''),					--角色名称。
	[DisplayName] [nvarchar](20) NOT NULL DEFAULT(''),			--角色显示名称。
	[Photo] [varchar](200) NOT NULL DEFAULT(''),				--角色图片。
	[Type] [int] NOT NULL DEFAULT(0),							--角色类型。（0：普通，1：管理员）
	[Permissions] [varchar](1000) DEFAULT('')					--权限列表字符串。（例如：1,2,4,5,7,8。其中，数字是权限编号）
)
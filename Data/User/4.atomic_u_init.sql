USE Atomic

GO

--清空数据。
DELETE FROM atomic_u_permissions
DELETE FROM atomic_u_roles
DELETE FROM atomic_u_users

GO

--创建初始化数据。
/*******************************************************************************************************************/

--初始化权限。

----管理员权限。

------配置
INSERT INTO atomic_u_permissions(Id, Name, DisplayName, Description) VALUES (1, 'IsLoginAdmin', '是否允许进入管理后台', '是否允许用户进入管理后台。')

------用户管理
INSERT INTO atomic_u_permissions(Id, Name, DisplayName, Description) VALUES (10, 'Role_IsShowRoleList', '是否允许查看角色列表', '是否允许查看角色列表。')
INSERT INTO atomic_u_permissions(Id, Name, DisplayName, Description) VALUES (11, 'Role_Add', '是否允许增加角色', '是否允许增加角色。')
INSERT INTO atomic_u_permissions(Id, Name, DisplayName, Description) VALUES (12, 'Role_Edit', '是否允许编辑角色', '是否允许编辑角色。')
INSERT INTO atomic_u_permissions(Id, Name, DisplayName, Description) VALUES (13, 'Role_Authorize', '是否允许给角色授权', '是否允许给角色授权。')

----用户权限。

------用户管理。
INSERT INTO atomic_u_permissions(Id, Name, DisplayName, Description) VALUES (101, 'User_Register', '注册新用户', '是否放开注册新用户。')
INSERT INTO atomic_u_permissions(Id, Name, DisplayName, Description) VALUES (102, 'User_SignIn', '用户登录', '是否允许用户登录。')
INSERT INTO atomic_u_permissions(Id, Name, DisplayName, Description) VALUES (103, 'User_ChangePassword', '修改密码', '是否允许修改密码。')
INSERT INTO atomic_u_permissions(Id, Name, DisplayName, Description) VALUES (104, 'User_Settings', '个人设置', '用户修改自己的个人信息。')

--初始化角色。

--管理员用户。
INSERT INTO atomic_u_roles(Id, Name, DisplayName, Photo, [Type], [Permissions]) VALUES (1, '管理员', '管理员', '', 1, '1,10,11,12,13,102,103,104')

--普通用户。
INSERT INTO atomic_u_roles(Id, Name, DisplayName, Photo, [Type], [Permissions]) VALUES (2, 'Anonymous', '匿名用户', '', 2, '101,102')
INSERT INTO atomic_u_roles(Id, Name, DisplayName, Photo, [Type], [Permissions]) VALUES (3, '一星会员', '一星会员', '', 0, '103,104')
INSERT INTO atomic_u_roles(Id, Name, DisplayName, Photo, [Type], [Permissions]) VALUES (4, 'Disable', '禁封用户', '', 2, '')

---------------------------------------------------------------------------------------------------------------------

--创建默认管理员账号。
/*******************************************************************************************************************/
--密码：123456
INSERT INTO atomic_u_users([AccountNumber], [Password], [Name], [Email], [Roles]) VALUES ('cjnmy36723', '96669d4aa2272f9b676f632b7db935a9', '天使之羽', 'cjnmy36723@gmail.com', '1,3')

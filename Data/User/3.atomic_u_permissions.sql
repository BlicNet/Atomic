USE Atomic

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[atomic_u_permissions]') AND type in (N'U'))
	DROP TABLE [dbo].[atomic_u_permissions]
GO

--权限表。
CREATE TABLE atomic_u_permissions(
	[Id] [int] NOT NULL PRIMARY KEY,							--权限编号
	[Name] [nvarchar](20) NOT NULL DEFAULT(''),					--权限名称。
	[DisplayName] [nvarchar](20) NOT NULL DEFAULT(''),			--权限显示名称。
	[Description] [nvarchar](50) NOT NULL DEFAULT('')			--权限描述。
)
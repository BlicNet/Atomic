﻿namespace Atomic.Mvc.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Atomic.Mvc.Properties;

    /// <summary>
    /// 控制器工厂。
    /// </summary>
    public class AtomicControllerFactory : DefaultControllerFactory
    {
        /// <summary>
        /// 控制器类型字典。
        /// </summary>
        private readonly static IDictionary<string, Type> _controllers = new Dictionary<string, Type>();

        /// <summary>
        /// 控制器类型字典。
        /// </summary>
        public static IDictionary<string, Type> ControllerTypes
        {
            get
            {
                return _controllers;
            }
        }

        /// <summary>
        /// 获得控制器类型。
        /// </summary>
        /// <param name="requestContext">请求信息。</param>
        /// <param name="controllerName">控制器名称。</param>
        /// <returns>控制器类型。</returns>
        protected override Type GetControllerType(RequestContext requestContext, string controllerName)
        {
            string key = this.GetControllerTypeKey(requestContext, controllerName);

            if (AtomicControllerFactory.ControllerTypes.ContainsKey(key))
            {
                var controllerType = AtomicControllerFactory.ControllerTypes[key];

                if (controllerType != null)
                {
                    return controllerType;
                }
            }

            return base.GetControllerType(requestContext, controllerName);
        }

        /// <summary>
        /// 检索指定请求上下文和控制器类型的控制器实例。
        /// </summary>
        /// <param name="requestContext">HTTP 请求的上下文，其中包括 HTTP 上下文和路由数据。</param>
        /// <param name="controllerType">控制器类型。</param>
        /// <returns>控制器实例。</returns>
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType != null && AtomicContainer.Container.IsRegisterd(controllerType))
            {
                var controller = AtomicContainer.Container.Reslve(controllerType) as IController;

                if(controller != null)
                {
                    return controller;
                }
            }
            
            return base.GetControllerInstance(requestContext, controllerType);
        }

        /// <summary>
        /// 获得缓存控制器类型的 KEY。
        /// </summary>
        /// <param name="requestContext">请求信息。</param>
        /// <param name="controllerName">控制器名称。</param>
        /// <returns>控制器类型的 KEY。</returns>
        private string GetControllerTypeKey(RequestContext requestContext, string controllerName)
        {
            if (requestContext.RouteData.Values.ContainsKey(Resources.VIEW_PLUGIN_NAME))
            {
                return requestContext.RouteData.Values[Resources.VIEW_PLUGIN_NAME].ToString() + "-" + controllerName + "Controller";
            }

            if (requestContext.RouteData.Values.ContainsKey(Areas.Area))
            {
                return requestContext.RouteData.Values[Areas.Area].ToString() + "-" + controllerName + "Controller";
            }

            return controllerName + "Controller";
        }
    }
}
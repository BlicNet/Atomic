﻿namespace Atomic.Mvc.ModelBinders
{
    using System;
    using System.Web.Mvc;

    using Atomic.Extensions;
    using Atomic.Mvc.Validators;

    /// <summary>
    /// 
    /// </summary>
    public class AtomicModelBinder : DefaultModelBinder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="bindingContext"></param>
        protected override void OnModelUpdated(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            base.OnModelUpdated(controllerContext, bindingContext);

            var validator = EntityValidator.Current.Validate(bindingContext.Model);

            if (validator != null && !validator.IsValid())
            {
                validator.GetInvalidMessages().ForEach(error => bindingContext.ModelState.AddModelError(error.PropertyName ?? error.Key, error.Message));
            }
        }
    }
}
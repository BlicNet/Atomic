﻿namespace Atomic.Mvc.ModelBinders
{
    using System;
    using System.Web.Mvc;

    /// <summary>
    /// 模型绑定器描述。
    /// </summary>
    public class ModelBinderDescriptor
    {
        /// <summary>
        /// 绑定实体类型。
        /// </summary>
        public Type Type { get; set; }

        /// <summary>
        /// 模型绑定器。
        /// </summary>
        public IModelBinder ModelBinder { get; set; }
    }
}
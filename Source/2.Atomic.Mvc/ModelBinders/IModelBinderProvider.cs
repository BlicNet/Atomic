﻿namespace Atomic.Mvc.ModelBinders
{
    using System.Collections.Generic;

    /// <summary>
    /// 模型绑定器提供程序。
    /// </summary>
    public interface IModelBinderProvider
    {
        /// <summary>
        /// 获得扩展模型绑定器。
        /// </summary>
        /// <returns></returns>
        IEnumerable<ModelBinderDescriptor> GetModelBinders();
    }
}
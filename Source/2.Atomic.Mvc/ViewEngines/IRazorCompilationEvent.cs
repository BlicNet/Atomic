﻿namespace Atomic.Mvc.ViewEngines
{
    using System.Web.Razor.Generator;
    using System.Web.WebPages.Razor;

    /// <summary>
    /// Razor 模板编译事件。
    /// </summary>
    public interface IRazorCompilationEvent
    {
        /// <summary>
        /// 代码编译开始。
        /// </summary>
        /// <param name="provider"></param>
        void CodeGenerationStarted(RazorBuildProvider provider);

        /// <summary>
        /// 代码编译完成。
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="e"></param>
        void CodeGenerationCompleted(RazorBuildProvider provider, CodeGenerationCompleteEventArgs e);
    }
}
﻿namespace Atomic.Mvc.ViewEngines
{
    using System.Web.Mvc;

    /// <summary>
    /// Rzaor 视图引擎集合适配器。
    /// </summary>
    public class ViewEngineCollectionWrapper : IViewEngine
    {
        /// <summary>
        /// Rzaor 视图引擎提供程序。
        /// </summary>
        private readonly IRzaorViewEngineProvider _rzaorViewEngineProvider = null;

        /// <summary>
        /// 初始化Rzaor 视图引擎集合适配器。
        /// </summary>
        /// <param name="rzaorViewEngineProvider">Rzaor 视图引擎提供程序。</param>
        public ViewEngineCollectionWrapper(IRzaorViewEngineProvider rzaorViewEngineProvider)
        {
            this._rzaorViewEngineProvider = rzaorViewEngineProvider;

            AtomicRzaorProvider.EnsureInitialized();
        }

        /// <summary>
        /// 使用指定的控制器上下文查找指定的分部视图。
        /// </summary>
        /// <param name="controllerContext">控制器上下文。</param>
        /// <param name="partialViewName">分部视图的名称。</param>
        /// <param name="useCache">若指定视图引擎返回缓存的视图（如果存在缓存的视图），则为 true；否则为 false。</param>
        /// <returns>分部视图。</returns>
        public ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
        {
            var viewEngine = this._rzaorViewEngineProvider.CreateViewEngine(controllerContext);

            return viewEngine.FindPartialView(controllerContext, partialViewName, useCache);
        }

        /// <summary>
        /// 使用指定的控制器上下文和母版视图名称来查找指定的视图。
        /// </summary>
        /// <param name="controllerContext">控制器上下文。</param>
        /// <param name="viewName">视图的名称。</param>
        /// <param name="masterName">母版视图的名称。</param>
        /// <param name="useCache">若为 true，则使用缓存的视图。</param>
        /// <returns>页视图。</returns>
        public ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
        {
            var viewEngine = this._rzaorViewEngineProvider.CreateViewEngine(controllerContext);

            return viewEngine.FindView(controllerContext, viewName, masterName, useCache);
        }

        /// <summary>
        /// 使用指定的控制器上下文来释放指定的视图。
        /// </summary>
        /// <param name="controllerContext">控制器上下文。</param>
        /// <param name="view">视图。</param>
        public void ReleaseView(ControllerContext controllerContext, IView view)
        {
            var viewEngine = this._rzaorViewEngineProvider.CreateViewEngine(controllerContext);

            viewEngine.ReleaseView(controllerContext, view);
        }
    }
}
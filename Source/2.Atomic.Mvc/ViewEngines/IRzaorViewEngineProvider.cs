﻿namespace Atomic.Mvc.ViewEngines
{
    using System.Web.Mvc;

    /// <summary>
    /// Rzaor 视图引擎提供程序接口。
    /// </summary>
    public interface IRzaorViewEngineProvider
    {
        /// <summary>
        /// 创建视图引擎。
        /// </summary>
        /// <param name="controllerContext">控制器上下文。</param>
        /// <returns>视图引擎。</returns>
        IViewEngine CreateViewEngine(ControllerContext controllerContext);
    }
}
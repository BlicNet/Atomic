﻿namespace Atomic.Mvc.ViewEngines
{
    using System;

    using Atomic.Mvc.Settings;

    /// <summary>
    /// 插件布局相关的辅助类。
    /// </summary>
    public class PluginLayout
    {
        /// <summary>
        /// 获得指定插件里生成搜索路径。
        /// </summary>
        /// <param name="pluginId">插件唯一标识。</param>
        /// <param name="path">搜索路径。</param>
        /// <returns>相对主站点完整虚拟搜索路径。</returns>
        public static string[] GetLayout(string pluginId, string path)
        {
            return GetLayout(pluginId, "Plugins", path);
        }

        /// <summary>
        /// 获得指定插件里生成搜索路径。
        /// </summary>
        /// <param name="pluginId">插件唯一标识。</param>
        /// <param name="pluginFolder">插件目录。</param>
        /// <param name="path">搜索路径。</param>
        /// <returns>相对主站点完整虚拟搜索路径。</returns>
        public static string[] GetLayout(string pluginId, string pluginFolder, string path)
        {
            if (path.Contains("\\"))
            {
                throw new ArgumentException("paths 字符串存在不合法字符。");
            }

            if (path.StartsWith("~/"))
            {
                return new string[]
                {
                    string.Format("/{1}/{0}/{2}", pluginId, pluginFolder, path.Substring(2))
                };
            }

            if (path.StartsWith("/"))
            {
                return new string[]
                {
                    string.Format("/{1}/{0}/{2}", pluginId, pluginFolder, path.Substring(1))
                };
            }

            return null;
        }

        /// <summary>
        /// 获得指定插件里主题生成搜索路径。
        /// </summary>
        /// <param name="pluginId">插件唯一标识。</param>
        /// <param name="path">主题目录下的搜索路径。</param>
        /// <returns>相对主站点完整虚拟搜索路径。</returns>
        public static string[] GetThemeLayout(string pluginId, string path)
        {
            return GetThemeLayout(pluginId, "Plugins", path);
        }

        /// <summary>
        /// 获得指定插件里主题生成搜索路径。
        /// </summary>
        /// <param name="pluginId">插件唯一标识。</param>
        /// <param name="pluginFolder">插件目录。</param>
        /// <param name="path">主题目录下的搜索路径。</param>
        /// <returns>相对主站点完整虚拟搜索路径。</returns>
        public static string[] GetThemeLayout(string pluginId, string pluginFolder, string path)
        {
            var site = SiteFactory.Current;

            if (path.Contains("\\"))
            {
                throw new ArgumentException("paths 字符串存在不合法字符。");
            }

            if (path.StartsWith("~/"))
            {
                return new string[]
                {
                    string.Format("~/{1}/{0}/Themes/{2}/{3}", pluginId, pluginFolder, site.Theme, path.Substring(2)),
                    string.Format("~/{1}/{0}/Themes/Default/{2}", pluginId, pluginFolder, path.Substring(2))
                };
            }

            if (path.StartsWith("/"))
            {
                return new string[]
                {
                    string.Format("~/{1}/{0}/Themes/{2}/{3}", pluginId, pluginFolder, site.Theme, path.Substring(1)),
                    string.Format("~/{1}/{0}/Themes/Default/{2}", pluginId, pluginFolder, path.Substring(1))
                };
            }

            return null;
        }
    }
}
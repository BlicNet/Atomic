﻿namespace Atomic.Mvc.ViewEngines
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    /// <summary>
    /// 默认布局管理器。
    /// </summary>
    public class DefaultLayoutManager : ILayoutManager
    {
        /// <summary>
        /// 布局信息字典。
        /// </summary>
        private readonly IDictionary<string, LayoutDescriptor> _layouts = new Dictionary<string, LayoutDescriptor>();

        /// <summary>
        /// 增加指定的布局信息。
        /// </summary>
        /// <param name="layout">布局信息。</param>
        public void Add(LayoutDescriptor layout)
        {
            if (layout == null || string.IsNullOrWhiteSpace(layout.Name))
            {
                throw new ArgumentNullException("layout", "布局信息的不可以是空的。");
            }

            this._layouts.Add(layout.Name, layout);
        }

        /// <summary>
        /// 获得布局信息。
        /// </summary>
        /// <param name="name">布局信息名称。</param>
        /// <returns>布局信息。</returns>
        public LayoutDescriptor Get(string name)
        {
            if (this._layouts.ContainsKey(name))
            {
                return this._layouts[name];
            }

            return null;
        }

        /// <summary>
        /// 获得所有布局模板。
        /// </summary>
        /// <returns>布局模板集合。</returns>
        public IEnumerable<LayoutDescriptor> GetAll()
        {
            return this._layouts.Values.ToList() ;
        }
    }
}
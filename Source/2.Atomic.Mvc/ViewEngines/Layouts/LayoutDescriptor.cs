﻿namespace Atomic.Mvc.ViewEngines
{
    using System.Linq;
    using System.Collections.Generic;

    using Atomic.Extensions;

    /// <summary>
    /// 布局信息。
    /// </summary>
    public class LayoutDescriptor
    {
        /// <summary>
        /// 初始化布局信息。
        /// </summary>
        /// <param name="name">名称。</param>
        /// <param name="pluginId">插件唯一标识。</param>
        /// <param name="path">主题目录下布局搜索路径。</param>
        /// <param name="isTheme">是否支持主题。</param>
        public LayoutDescriptor(string name, string pluginId, string path, bool isTheme)
            : this(name, pluginId, "Plugins", path, isTheme)
        { }

        /// <summary>
        /// 初始化布局信息。
        /// </summary>
        /// <param name="name">名称。</param>
        /// <param name="pluginId">插件唯一标识。</param>
        /// <param name="pluginFolder">插件目录。</param>
        /// <param name="path">主题目录下布局搜索路径。</param>
        /// <param name="isTheme">是否支持主题。</param>
        public LayoutDescriptor(string name, string pluginId, string pluginFolder, string path, bool isTheme)
        {
            this.Name = name;
            this.PluginId = pluginId;
            this.PluginFolder = pluginFolder;
            this.Path = path;
            this.IsTheme = isTheme;
        }

        /// <summary>
        /// 名称。
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// 插件唯一标识。
        /// </summary>
        public string PluginId { get; private set; }

        /// <summary>
        /// 插件目录。
        /// </summary>
        public string PluginFolder { get; private set; }

        /// <summary>
        /// 布局搜索路径。
        /// </summary>
        public string Path { get; private set; }

        /// <summary>
        /// 是否支持主题。
        /// </summary>
        public bool IsTheme { get; private set; }

        /// <summary>
        /// 获得主题支持布局搜索路径。
        /// </summary>
        /// <returns>布局搜索路径。</returns>
        public string[] GetThemePaths()
        {
            return this.IsTheme ? 
                PluginLayout.GetThemeLayout(this.PluginId, this.PluginFolder, this.Path) : PluginLayout.GetLayout(this.PluginId,this.PluginFolder,this.Path);
        }
    }
}
﻿namespace Atomic.Mvc.ViewEngines
{
    using System.Collections.Generic;

    /// <summary>
    /// 布局管理器。
    /// </summary>
    public interface ILayoutManager
    {
        /// <summary>
        /// 增加指定的布局信息。
        /// </summary>
        /// <param name="layout">布局信息。</param>
        void Add(LayoutDescriptor layout);

        /// <summary>
        /// 获得布局信息。
        /// </summary>
        /// <param name="name">布局信息名称。</param>
        /// <returns>布局信息。</returns>
        LayoutDescriptor Get(string name);

        /// <summary>
        /// 获得所有布局模板。
        /// </summary>
        /// <returns>布局模板集合。</returns>
        IEnumerable<LayoutDescriptor> GetAll();
    }
}
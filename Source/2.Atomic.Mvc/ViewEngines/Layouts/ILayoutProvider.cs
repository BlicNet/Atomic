﻿namespace Atomic.Mvc.ViewEngines
{
    using System.Collections.Generic;

    /// <summary>
    /// 布局信息提供程序。
    /// </summary>
    public interface ILayoutProvider
    {
        /// <summary>
        /// 获得布局信息集合。
        /// </summary>
        /// <returns>布局信息集合。</returns>
        IEnumerable<LayoutDescriptor> GetLayouts();
    }
}
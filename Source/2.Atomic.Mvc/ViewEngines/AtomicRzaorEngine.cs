﻿namespace Atomic.Mvc.ViewEngines
{
    using System.Linq;
    using System.Web.Mvc;

    using Atomic.Mvc.Properties;

    /// <summary>
    /// Rzaor 视图引擎。
    /// </summary>
    public class AtomicRzaorEngine : RazorViewEngine
    {
        /// <summary>
        /// 布局管理器。
        /// </summary>
        private readonly ILayoutManager _layoutManager = null;

        /// <summary>
        /// 初始化 Rzaor 视图引擎。
        /// </summary>
        /// <param name="layoutManager">布局管理器。</param>
        public AtomicRzaorEngine(ILayoutManager layoutManager)
        {
            this._layoutManager = layoutManager;
        }

        /// <summary>
        /// 使用指定的控制器上下文来查找指定的分部视图。
        /// </summary>
        /// <param name="controllerContext">控制器上下文。</param>
        /// <param name="partialViewName">分部视图的名称。</param>
        /// <param name="useCache">若为 true，则使用缓存的分部视图。</param>
        /// <returns>分部视图。</returns>
        public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
        {
            return base.FindPartialView(controllerContext, partialViewName, useCache);
        }

        /// <summary>
        /// 使用指定的控制器上下文和母版视图名称来查找指定的视图。
        /// </summary>
        /// <param name="controllerContext">控制器上下文。</param>
        /// <param name="viewName">视图的名称。</param>
        /// <param name="masterName">母版视图的名称。</param>
        /// <param name="useCache">若为 true，则使用缓存的视图。</param>
        /// <returns>页视图。</returns>
        public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
        {
            if (!string.IsNullOrWhiteSpace(masterName))
            {
                var layout = this._layoutManager.Get(masterName);

                if (layout != null)
                {
                    this.MasterLocationFormats = layout.GetThemePaths();
                    this.AreaMasterLocationFormats = this.MasterLocationFormats;
                }
            }

            return base.FindView(controllerContext, viewName, masterName, useCache);
        }
    }
}
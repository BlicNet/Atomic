﻿namespace Atomic.Mvc.Filters
{
    using System.Web.Mvc;

    using Atomic.Mvc.ViewEngines;

    /// <summary>
    /// 布局过虑器。
    /// </summary>
    public class LayoutAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// 母板页名称。
        /// </summary>
        private readonly string _masterName = string.Empty;

        /// <summary>
        /// 初始化布局过虑器。
        /// </summary>
        public LayoutAttribute()
            : this("_Layout")
        {
        }

        /// <summary>
        /// 初始化布局过虑器。
        /// </summary>
        /// <param name="masterName">母板页名称。</param>
        public LayoutAttribute(string masterName)
        {
            _masterName = masterName;
        }

        /// <summary>
        /// Action 方法执行结束时处理。
        /// </summary>
        /// <param name="filterContext">提供 System.Web.Mvc.ActionFilterAttribute 类的 ActionExecuted 方法的上下文。</param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            var result = filterContext.Result as ViewResult;
            if (result != null)
            {
                result.MasterName = this._masterName;
            }
        }
    }
}
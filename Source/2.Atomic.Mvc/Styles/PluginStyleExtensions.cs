﻿namespace Atomic.Mvc
{
    using System;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Atomic.Mvc.Settings;
    using Atomic.Mvc.Properties;

    /// <summary>
    /// 插件样式表。
    /// </summary>
    public static class PluginStyleExtensions
    {
        #region 插件根目录路径

        /// <summary>
        /// 获得当前页面所执行的插件的名称。
        /// </summary>
        /// <param name="htmlHelper">表示支持在视图中呈现 HTML 控件。</param>
        /// <returns>当前页面所执行的插件的名称。</returns>
        public static string GetPluginName(this HtmlHelper htmlHelper)
        {
            string pluginName = string.Empty;

            if (htmlHelper.ViewContext.RouteData.Values.ContainsKey(Resources.VIEW_PLUGIN_NAME))
            {
                pluginName = htmlHelper.ViewContext.RouteData.GetRequiredString(Resources.VIEW_PLUGIN_NAME);
            }

            return pluginName;
        }

        /// <summary>
        /// 获得当前页面所执行的插件所在目录。
        /// </summary>
        /// <remarks>
        /// 是 Core 还是 Plugins。
        /// </remarks>
        /// <param name="htmlHelper">表示支持在视图中呈现 HTML 控件。</param>
        /// <returns>获得当前页面所执行的插件所在目录。</returns>
        public static string GetPluginFolder(this HtmlHelper htmlHelper)
        {
            string pluginFolder = string.Empty;

            if (htmlHelper.ViewContext.RouteData.Values.ContainsKey(Resources.VIEW_PLUGIN_FOLDER))
            {
                pluginFolder = htmlHelper.ViewContext.RouteData.GetRequiredString(Resources.VIEW_PLUGIN_FOLDER);
            }

            return pluginFolder;
        }

        /// <summary>
        /// 获得应用插件根目录路径。
        /// </summary>
        /// <param name="htmlHelper">表示支持在视图中呈现 HTML 控件。</param>
        /// <param name="pluginName">插件名称。</param>
        /// <returns>插件根目录路径。</returns>
        public static string GetPluginPath(this HtmlHelper htmlHelper, string pluginName)
        {
            //return htmlHelper.GetPluginPath(htmlHelper.GetPluginFolder(), htmlHelper.GetPluginName());
            return htmlHelper.GetPluginPath("Plugins", pluginName);
        }

        /// <summary>
        /// 获得核心插件根目录路径。
        /// </summary>
        /// <param name="htmlHelper">表示支持在视图中呈现 HTML 控件。</param>
        /// <param name="pluginName">插件名称。</param>
        /// <returns>插件根目录路径。</returns>
        public static string GetCorePluginPath(this HtmlHelper htmlHelper, string pluginName)
        {
            return htmlHelper.GetPluginPath("Core", pluginName);
        }

        /// <summary>
        /// 获得指定插件根目录路径。
        /// </summary>
        /// <param name="htmlHelper">表示支持在视图中呈现 HTML 控件。</param>
        /// <param name="pluginFolder">插件所在目录（Core 还是 Plugins）。</param>
        /// <param name="pluginName">插件名称。</param>
        /// <returns>插件根目录路径。</returns>
        private static string GetPluginPath(this HtmlHelper htmlHelper, string pluginFolder, string pluginName)
        {
            if (!string.IsNullOrWhiteSpace(pluginName) && !string.IsNullOrWhiteSpace(pluginFolder))
            {
                return string.Format("{0}/{1}", pluginFolder, pluginName);
            }

            return string.Empty;
        }

        #endregion
    }
}
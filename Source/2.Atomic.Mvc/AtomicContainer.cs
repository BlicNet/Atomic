﻿namespace Atomic.Mvc
{
    /// <summary>
    /// 容器。
    /// </summary>
    public static class AtomicContainer
    {
        /// <summary>
        /// 容器。
        /// </summary>
        private static IAtomicContainer _container = null;

        /// <summary>
        /// 注册容器。
        /// </summary>
        /// <param name="container">容器。</param>
        public static void RegisterContainer(IAtomicContainer container)
        {
            _container = container;
        }

        /// <summary>
        /// 容器。
        /// </summary>
        public static IAtomicContainer Container
        {
            get 
            {
                return _container;
            }
        }

        /// <summary>
        /// 从容器中取出一个 TService 类型的实例。
        /// </summary>
        /// <typeparam name="TService">类型。</typeparam>
        /// <returns>TService 类型的实例。</returns>
        public static TService Resolve<TService>()
        {
            return _container.Resolve<TService>();
        }
    }
}
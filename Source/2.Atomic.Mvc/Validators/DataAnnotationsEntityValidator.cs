﻿namespace Atomic.Mvc.Validators
{
    using System;

    /// <summary>
    /// 解析实体类型验证。
    /// </summary>
    public class DataAnnotationsEntityValidator<TEntity> : IEntityValidator where TEntity : class
    {
        /// <summary>
        /// 实体验证对象。
        /// </summary>
        private readonly EntityValidator _validator = null;

        /// <summary>
        /// 验证实体的类型。
        /// </summary>
        private readonly Type _type = null;

        /// <summary>
        /// 初始化解析实体类型验证。
        /// </summary>
        /// <param name="validator">实体验证对象。</param>
        /// <param name="type">验证实体的类型。</param>
        public DataAnnotationsEntityValidator(EntityValidator validator, Type type)
        {
            this._validator = validator;
            this._type = type;
        }

        /// <summary>
        /// 验证实体。
        /// </summary>
        /// <param name="item">实体对象。</param>
        /// <returns>实体验证状态。</returns>
        public ValidatorState Validate(object item)
        {
            return _validator.Validate((TEntity)item);
        }
    }
}
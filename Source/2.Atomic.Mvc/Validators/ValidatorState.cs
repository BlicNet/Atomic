﻿namespace Atomic.Mvc.Validators
{
    using System.Linq;
    using System.Collections.Generic;

    using Atomic.Extensions;

    /// <summary>
    /// 验证状态。
    /// </summary>
    public class ValidatorState
    {
        /// <summary>
        /// 验证错误信息集合。
        /// </summary>
        private readonly IList<ValidatorError> _errors = new List<ValidatorError>();

        /// <summary>
        /// 是否验证通过。
        /// </summary>
        /// <returns>通过则为 true。</returns>
        public bool IsValid()
        {
            return this._errors != null && !this._errors.Any();
        }

        /// <summary>
        /// 获得错误信息集合。
        /// </summary>
        /// <returns>错误信息集合。</returns>
        public IEnumerable<ValidatorError> GetInvalidMessages()
        {
            return _errors.ToReadOnlyCollection();
        }

        /// <summary>
        /// 增加错误信息。
        /// </summary>
        /// <param name="error">错误信息集合。</param>
        public virtual void AddValidatorError(ValidatorError error)
        {
            _errors.Add(error);
        }

        /// <summary>
        /// 增加错误信息。
        /// </summary>
        /// <param name="key">错误信息唯一标识。</param>
        /// <param name="propertyName">验证错误对象的属性名。</param>
        /// <param name="errorMessage">错误信息字符串。</param>
        /// <param name="args">错误信息字符串参数。</param>
        public virtual void AddValidatorError(string key, string propertyName, string errorMessage, params object[] args)
        {
            _errors.Add(new ValidatorError { Key = key, PropertyName = propertyName, Message = string.Format(errorMessage, args) });
        }
    }
}
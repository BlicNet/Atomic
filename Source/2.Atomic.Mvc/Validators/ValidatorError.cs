﻿namespace Atomic.Mvc.Validators
{
    /// <summary>
    /// 验证错误信息实体。
    /// </summary>
    public class ValidatorError
    {
        /// <summary>
        /// 唯一标识。
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 属性名。
        /// </summary>
        public string PropertyName { get; set; }

        /// <summary>
        /// 错误信息。
        /// </summary>
        public string Message { get; set; }
    }
}
﻿namespace Atomic.Mvc.Validators
{
    /// <summary>
    /// 实体验证接口。
    /// </summary>
    public interface IEntityValidator
    {
        /// <summary>
        /// 验证实体。
        /// </summary>
        /// <param name="item">实体对象。</param>
        /// <returns>实体验证状态。</returns>
        ValidatorState Validate(object item);
    }

    /// <summary>
    /// 验证接口。
    /// </summary>
    /// <typeparam name="TEntity">实体类型。</typeparam>
    public interface IValidator<TEntity> where TEntity : class
    {
        /// <summary>
        /// 验证实体。
        /// </summary>
        /// <param name="item">实体对象。</param>
        /// <returns>实体验证状态。</returns>
        ValidatorState Validate(TEntity item);
    }
}
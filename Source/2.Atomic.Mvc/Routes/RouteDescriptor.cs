﻿namespace Atomic.Mvc.Routes
{
    /// <summary>
    /// Route 规则描述。
    /// </summary>
    public class RouteDescriptor
    {
        /// <summary>
        /// 初始化 Route 规则描述。
        /// </summary>
        public RouteDescriptor()
        {
            this.Area = true;
        }

        /// <summary>
        /// 要映射的路由的名称。
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 路由的 URL 模式。
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 一个包含默认路由值的对象。
        /// </summary>
        public object Defaults { get; set; }

        /// <summary>
        /// 一组表达式，用于指定 url 参数的值的条件。
        /// </summary>
        public object Constraints { get; set; }

        /// <summary>
        /// 应用程序的命名空间集合。
        /// </summary>
        public string[] Namespaces { get; set; }

        /// <summary>
        /// 是否启用区域。
        /// </summary>
        public bool Area { get; set; }
    }
}
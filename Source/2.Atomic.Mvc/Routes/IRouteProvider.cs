﻿namespace Atomic.Mvc.Routes
{
    using System.Collections.Generic;

    /// <summary>
    /// 路由规则提供程序接口。
    /// </summary>
    public interface IRouteProvider
    {
        /// <summary>
        /// 获取路由规则。
        /// </summary>
        /// <returns>路由规则描述集合。</returns>
        IEnumerable<RouteDescriptor> GetRoutes();
    }
}
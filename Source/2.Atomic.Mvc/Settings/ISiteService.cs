﻿namespace Atomic.Mvc.Settings
{
    /// <summary>
    /// 站点服务
    /// </summary>
    public interface ISiteService
    {
        /// <summary>
        /// 获得站点信息。
        /// </summary>
        /// <returns>站点信息。</returns>
        ISite Get();
    }
}
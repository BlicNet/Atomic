﻿namespace Atomic.Mvc.Settings
{
    using System;

    /// <summary>
    /// 站点信息工厂。
    /// </summary>
    public class SiteFactory
    {
        /// <summary>
        /// 站点信息服务。
        /// </summary>
        private static ISiteService _siteService = null;

        #region Properties

        /// <summary>
        /// 当前站点信息
        /// </summary>
        public static ISite Current
        {
            get
            {
                ISite current = new SafeModeSite();

                if (_siteService != null)
                {
                    current = _siteService.Get();
                }

                return current;
            }
        }

        #endregion

        /// <summary>
        /// 设置站点服务。
        /// </summary>
        /// <param name="siteService">站点服务。</param>
        public static void SetSiteService(ISiteService siteService)
        {
            _siteService = siteService;
        }
    }
}
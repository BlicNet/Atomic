﻿namespace Atomic.Mvc.Settings
{
    /// <summary>
    /// 站点信息。
    /// </summary>
    public interface ISite
    {
        /// <summary>
        /// 站点名称。
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// 主题。
        /// </summary>
        string Theme { get; set; }

        /// <summary>
        /// 默认页大小。
        /// </summary>
        int PageSize { get; set; }

        /// <summary>
        /// 首页地址。
        /// </summary>
        string HomePage { get; set; }

        /// <summary>
        /// 登录页地址。
        /// </summary>
        string LoginPage { get; set; }

        /// <summary>
        /// 403 错误页地址。
        /// </summary>
        string Error403Page { get; set; }

        /// <summary>
        /// 404 错误页地址。
        /// </summary>
        string Error404Page { get; set; }

        /// <summary>
        /// 500 错误页地址。
        /// </summary>
        string Error500Page { get; set; }

        /// <summary>
        /// 图片地址。
        /// </summary>
        string ImageUrl { get; set; }

        /// <summary>
        /// 用户默认头像地址。
        /// </summary>
        string IconDefaultUrl { get; set; }

        /// <summary>
        /// 保存头像位置。
        /// </summary>
        /// <remarks>
        /// 头像保存的物理位置。
        /// </remarks>
        string SaveIconPath { get; set; }

        /// <summary>
        /// 头像预览区域的最大宽度。
        /// </summary>
        int IconAreaWidth { get; set; }

        /// <summary>
        /// 头像预览区域的最大高度。
        /// </summary>
        int IconAreaHeight { get; set; }
    }
}
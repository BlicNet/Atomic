﻿namespace Atomic.Mvc.Settings
{
    /// <summary>
    /// 默认站点信息。
    /// </summary>
    public class SafeModeSite : ISite
    {
        /// <summary>
        /// 初始化。
        /// </summary>
        public SafeModeSite()
        {
            this.Name = "Atomic";
            this.Theme = string.IsNullOrWhiteSpace(this.Theme) ? "Default" : this.Theme;
            this.PageSize = 10;
            this.HomePage = "/";
            this.LoginPage = "/account/login";
            this.Error403Page = "/error/403";
            this.Error404Page = "/error/404";
            this.Error500Page = "/error/500";
            this.ImageUrl = "/";
            this.IconDefaultUrl = "/Content/Images/Default/Icon.jpg";
            this.IconAreaWidth = 400;
            this.IconAreaHeight = 300;
            this.SaveIconPath = "";
        }

        /// <summary>
        /// 网站名称。
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 主题。
        /// </summary>
        public string Theme { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string HomePage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LoginPage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Error403Page { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Error404Page { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Error500Page { get; set; }

        /// <summary>
        /// 图片地址。
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// 用户默认头像地址。
        /// </summary>
        public string IconDefaultUrl { get; set; }

        /// <summary>
        /// 头像预览区域的最大宽度。
        /// </summary>
        public int IconAreaWidth { get; set; }

        /// <summary>
        /// 头像预览区域的最大高度。
        /// </summary>
        public int IconAreaHeight { get; set; }

        /// <summary>
        /// 保存头像位置。
        /// </summary>
        /// <remarks>
        /// 头像保存的物理位置。
        /// </remarks>
        public string SaveIconPath { get; set; }
    }
}
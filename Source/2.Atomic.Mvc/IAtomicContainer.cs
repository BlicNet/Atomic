﻿namespace Atomic.Mvc
{
    using System;
    using System.Reflection;

    /// <summary>
    /// IoC 容器。
    /// </summary>
    public interface IAtomicContainer
    {
        /// <summary>
        /// 注册类型到容器。
        /// </summary>
        /// <param name="serviceType">类型。</param>
        void RegisterType(Type serviceType);

        /// <summary>
        /// 注册类型到容器。
        /// </summary>
        /// <typeparam name="TSource">类型</typeparam>
        void RegisterType<TSource>();

        /// <summary>
        /// 注册类型到容器。
        /// </summary>
        /// <typeparam name="TFrom">父类型。</typeparam>
        /// <param name="serviceType">实现类型。</param>
        void RegisterType<TFrom>(Type serviceType);

        /// <summary>
        /// 注册类型到容器。
        /// </summary>
        /// <typeparam name="TFrom">父类型。</typeparam>
        /// <typeparam name="TTo">实现类型。</typeparam>
        void RegisterType<TFrom, TTo>();

        /// <summary>
        /// 注册类型到容器，使用单例形式。
        /// </summary>
        /// <typeparam name="TFrom">父类型。</typeparam>
        /// <typeparam name="TTo">实现类型。</typeparam>
        void RegisterInstance<TFrom, TTo>();

        ///// <summary>
        ///// 注册指定程序集的所有控制器。
        ///// </summary>
        ///// <param name="assemblies">程序集列表。</param>
        //void RegisterControllers(params Assembly[] assemblies);

        /// <summary>
        /// 注册指定程序集的所有类型。
        /// </summary>
        /// <param name="assemblies">程序集列表。</param>
        void RegisterAssemblyTypes(params Assembly[] assemblies);

        /// <summary>
        /// 注册指定程序集的所有类型到它所实现的接口。
        /// </summary>
        /// <param name="assemblies">程序集列表。</param>
        void RegisterAssemblyTypesAsImplementedInterfaces(params Assembly[] assemblies);

        /// <summary>
        /// 判断指定类型是否注册。
        /// </summary>
        /// <typeparam name="TSource">父类型。</typeparam>
        /// <returns></returns>
        bool IsRegisterd<TSource>();

        /// <summary>
        /// 判断指定类型是否注册。
        /// </summary>
        /// <param name="serviceType">类型。</param>
        bool IsRegisterd(Type serviceType);

        /// <summary>
        /// 从容器中取出一个 serviceType 类型的实例。
        /// </summary>
        /// <param name="serviceType">类型。</param>
        /// <returns>serviceType 类型的实例。</returns>
        object Reslve(Type serviceType);

        /// <summary>
        /// 从容器中取出一个 TService 类型的实例。
        /// </summary>
        /// <typeparam name="TService">类型。</typeparam>
        /// <returns>TService 类型的实例。</returns>
        TService Resolve<TService>();
    }
}
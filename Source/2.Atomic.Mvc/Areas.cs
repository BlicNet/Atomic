﻿namespace Atomic.Mvc
{
    /// <summary>
    /// 区域描述。
    /// </summary>
    public class Areas
    {
        /// <summary>
        /// 区域关键字。
        /// </summary>
        public const string Area = "area";
    }
}
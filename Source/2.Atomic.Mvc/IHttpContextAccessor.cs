﻿namespace Atomic.Mvc
{
    using System.Web;

    /// <summary>
    /// HttpContext 访问器。
    /// </summary>
    public interface IHttpContextAccessor
    {
        /// <summary>
        /// 当前 HttpContext 上下文。
        /// </summary>
        /// <returns></returns>
        HttpContextBase Current();
    }
}
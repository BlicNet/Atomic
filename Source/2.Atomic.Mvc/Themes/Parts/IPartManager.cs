﻿namespace Atomic.Mvc.Themes
{
    /// <summary>
    /// 部件管理器。
    /// </summary>
    public interface IPartManager
    {
        /// <summary>
        /// 获得部件。
        /// </summary>
        /// <param name="id">部件唯一标识。</param>
        /// <returns>部件位置。</returns>
        string Get(string id);

        /// <summary>
        /// 增加部件。
        /// </summary>
        /// <param name="part">部件对象。</param>
        void Add(IPart part);
    }
}
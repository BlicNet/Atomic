﻿namespace Atomic.Mvc.Themes
{
    /// <summary>
    /// 部件。
    /// </summary>
    public class Part : IPart
    {
        /// <summary>
        /// 初始化部件。
        /// </summary>
        /// <param name="id">唯一标识。</param>
        /// <param name="pluginId">插件唯一标识。</param>
        /// <param name="pluginFolder">插件目录名称。</param>
        /// <param name="virtualPath">虚拟路径。</param>
        public Part(string id, string pluginId, string pluginFolder, string virtualPath)
            : this(id, pluginId, pluginFolder, string.Empty, virtualPath)
        {
        }

        /// <summary>
        /// 初始化部件。
        /// </summary>
        /// <param name="id">唯一标识。</param>
        /// <param name="pluginId">插件唯一标识。</param>
        /// <param name="pluginFolder">插件目录名称。</param>
        /// <param name="themeName">主题名称，不在主题内则留空。</param>
        /// <param name="virtualPath">虚拟路径。</param>
        public Part(string id, string pluginId, string pluginFolder, string themeName, string virtualPath)
        {
            this.Id = id;
            this.PluginId = pluginId;
            this.PluginFolder = pluginFolder;
            this.VirtualPath = virtualPath;
        }

        /// <summary>
        /// 唯一标识。
        /// </summary>
        public string Id { get; private set; }

        /// <summary>
        /// 插件唯一标识。
        /// </summary>
        public string PluginId { get; private set; }

        /// <summary>
        /// 插件目录名称。
        /// </summary>
        public string PluginFolder { get; private set; }

        /// <summary>
        /// 主题名称。
        /// </summary>
        /// <remarks>
        /// 不在主题内则留空。
        /// </remarks>
        public string ThemeName { get; private set; }

        /// <summary>
        /// 虚拟路径。
        /// </summary>
        public string VirtualPath { get; private set; }
    }
}
﻿namespace Atomic.Mvc.Themes
{
    /// <summary>
    /// 部件接口。
    /// </summary>
    public interface IPart
    {
        /// <summary>
        /// 唯一标识。
        /// </summary>
        string Id { get; }

        /// <summary>
        /// 插件唯一标识。
        /// </summary>
        string PluginId { get; }

        /// <summary>
        /// 插件目录名称。
        /// </summary>
        string PluginFolder { get; }

        /// <summary>
        /// 虚拟路径。
        /// </summary>
        string VirtualPath { get; }
    }
}
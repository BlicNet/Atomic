﻿namespace Atomic.Mvc.Themes
{
    using System.Collections.Generic;

    /// <summary>
    /// 部件提供程序接口。
    /// </summary>
    public interface IPartProvider
    {
        /// <summary>
        /// 获取部件。
        /// </summary>
        /// <returns>部件集合。</returns>
        IEnumerable<IPart> GetParts();
    }
}
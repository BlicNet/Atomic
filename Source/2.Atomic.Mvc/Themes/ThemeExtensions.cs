﻿namespace Atomic.Mvc
{
    using System.Web.Mvc;
    using System.Web.Mvc.Html;

    using Atomic.Mvc.Themes;

    /// <summary>
    /// 主题扩展。
    /// </summary>
    public static class ThemeExtensions
    {
        /// <summary>
        /// 部件管理器。
        /// </summary>
        private static IPartManager _partManager = null;

        /// <summary>
        /// 部件管理器。
        /// </summary>
        private static IPartManager PartManager
        {
            get
            {
                if(_partManager == null)
                {
                    _partManager = AtomicContainer.Resolve<IPartManager>();
                }

                return _partManager;
            }
        }

        /// <summary>
        /// 输出部件。
        /// </summary>
        /// <param name="htmlHelper">表示支持在视图中呈现 HTML 控件。</param>
        /// <param name="id">部件标识。</param>
        /// <returns>Html 标记。</returns>
        public static MvcHtmlString Part(this HtmlHelper htmlHelper, string id)
        {
            var part = PartManager.Get(id);

            if (!string.IsNullOrWhiteSpace(part))
            {
                return htmlHelper.Partial(part);
            }
            else
            {
                return new MvcHtmlString(string.Empty);
            }
        }
    }
}
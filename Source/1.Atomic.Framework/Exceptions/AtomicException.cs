﻿namespace Atomic.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Atomic 默认异常。
    /// </summary>
    [Serializable]
    public class AtomicException : Exception
    {
        /// <summary>
        /// 始化 System.Exception 类的新实例。
        /// </summary>
        public AtomicException()
        {
        }

        /// <summary>
        /// 使用指定的错误信息初始化 System.Exception 类的新实例。
        /// </summary>
        /// <param name="message">描述错误的消息。</param>
        public AtomicException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// 用序列化数据初始化 System.Exception 类的新实例。
        /// </summary>
        /// <param name="info">System.Runtime.Serialization.SerializationInfo，它存有有关所引发的异常的序列化对象数据。</param>
        /// <param name="context">System.Runtime.Serialization.StreamingContext，它包含有关源或目标的上下文信息。</param>
        protected AtomicException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// 使用指定错误消息和对作为此异常原因的内部异常的引用来初始化 System.Exception 类的新实例。
        /// </summary>
        /// <param name="message">解释异常原因的错误信息。</param>
        /// <param name="innerException">导致当前异常的异常；如果未指定内部异常，则是一个 null 引用（在 Visual Basic 中为 Nothing）。</param>
        public AtomicException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
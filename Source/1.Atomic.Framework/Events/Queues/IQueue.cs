﻿namespace Atomic.Events.Queues
{
    using System.Collections.Generic;

    /// <summary>
    /// 队列接口。
    /// </summary>
    /// <typeparam name="T">项的类型。</typeparam>
    public interface IQueue<T>
    {
        /// <summary>
        /// 增加项到队列。
        /// </summary>
        /// <param name="item">项。</param>
        void Add(T item);

        /// <summary>
        /// 获取项。
        /// </summary>
        /// <returns>项。</returns>
        T Get();

        /// <summary>
        /// 获得指定数量的项。
        /// </summary>
        /// <param name="count">数量。</param>
        /// <returns>项的集合。</returns>
        IEnumerable<T> Get(int count);

        /// <summary>
        /// 获取所有项的数组。
        /// </summary>
        /// <returns>所有项的数组。</returns>
        T[] ToArray();

        /// <summary>
        /// 获得指定数量的数组。
        /// </summary>
        /// <returns>所有项的数组。</returns>
        T[] ToArray(int count);
    }
}
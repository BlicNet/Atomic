﻿namespace Atomic.Events
{
    using System;

    /// <summary>
    /// 事件处理器
    /// </summary>
    public interface IEventHandler
    {
        /// <summary>
        /// 唯一标识。
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// 事件类型。
        /// </summary>
        Type EventType { get; }

        /// <summary>
        /// 处理器等级。
        /// </summary>
        /// <remarks>
        /// 主处理器。
        /// 子处理器。
        /// </remarks>
        EventHandlerLevel Level { get; }

        /// <summary>
        /// 事件处理
        /// </summary>
        /// <param name="_event">事件信息</param>
        /// <returns>是否处理完成。</returns>
        bool Handle(object _event);
    }

    /// <summary>
    /// 事件处理器
    /// </summary>
    /// <typeparam name="TEvent">事件类型</typeparam>
    public interface IEventHandler<TEvent> : IEventHandler where TEvent : IEvent
    {
        /// <summary>
        /// 事件处理
        /// </summary>
        /// <param name="_event">事件信息</param>
        /// <returns>是否处理完成。</returns>
        bool Handle(TEvent _event);
    }

    /// <summary>
    /// 事件处理器等级。
    /// </summary>
    public enum EventHandlerLevel
    {
        /// <summary>
        /// 主处理器。
        /// </summary>
        Master = 1,

        /// <summary>
        /// 子处理器。
        /// </summary>
        Child = 2
    }
}
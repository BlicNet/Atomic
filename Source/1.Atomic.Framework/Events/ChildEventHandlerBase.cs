﻿namespace Atomic.Events
{
    using Atomic.Logging;

    /// <summary>
    /// 子事件处理器基类
    /// </summary>
    /// <typeparam name="TEvent">事件类型</typeparam>
    public abstract class ChildEventHandlerBase<TEvent> : EventHandlerBase<TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// 初始化子事件处理器基类。
        /// </summary>
        public ChildEventHandlerBase()
            : base(EventHandlerLevel.Child)
        {
            this.Logger = LoggerFactory.Current.Create();
        }

        /// <summary>
        /// 日志。
        /// </summary>
        protected ILogger Logger { get; private set; }

        /// <summary>
        /// 事件处理
        /// </summary>
        /// <param name="event">事件信息</param>
        /// <returns>是否处理完成。</returns>
        public override bool Handle(TEvent @event)
        {
            var complete = base.Handle(@event);

            //处理失败。
            if (complete == false)
            {
                //TODO: 记录操作失败信息，并重试。
                //写入处理失败基本日志。
                this.Logger.LogError("[{0}] 事件 [{1}] 处理器执行失败。", @event.GetType(), this.GetType());
            }

            return complete;
        }
    }
}
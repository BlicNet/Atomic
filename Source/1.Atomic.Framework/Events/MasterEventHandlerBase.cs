﻿namespace Atomic.Events
{
    /// <summary>
    /// 主事件处理器基类
    /// </summary>
    /// <remarks>
    /// 主要处理业务，只要指定事件的所有主处理器都执行成功，则代表该业务一定能成功。
    /// 那么，指定事件的所有子处理器就一定可以处理成功，如果子处理器存在处理失败的情况，则代表系统出现了异常。
    /// </remarks>
    /// <typeparam name="TEvent">事件类型</typeparam>
    public abstract class MasterEventHandlerBase<TEvent> : EventHandlerBase<TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// 初始化主事件处理器基类。
        /// </summary>
        public MasterEventHandlerBase()
            : base(EventHandlerLevel.Master)
        {
        }
    }
}
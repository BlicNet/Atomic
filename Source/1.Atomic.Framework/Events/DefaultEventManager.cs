﻿namespace Atomic.Events
{
    /// <summary>
    /// 默认事件管理器
    /// </summary>
    public class GenericEventManager<TEvent> : IEventManager where TEvent : IEvent
    {
        /// <summary>
        /// 填充事件处理器。
        /// </summary>
        /// <param name="eventHandler">事件处理器。</param>
        public void EventHandlersWith(IEventHandler eventHandler)
        {
            EventManager.Current.EventHandlersWith<TEvent>((IEventHandler<TEvent>)eventHandler);
        }

        /// <summary>
        /// 发布事件。
        /// </summary>
        /// <param name="_event">事件信息实体。</param>
        public void Publish(object _event)
        {
            EventManager.Current.Publish<TEvent>((TEvent)_event);
        }

        /// <summary>
        /// 清空事件。
        /// </summary>
        public void Clear()
        {
            EventManager.Current.Clear<TEvent>();
        }
    }

    /// <summary>
    /// 默认事件管理器
    /// </summary>
    /// <typeparam name="TEvent">事件类型</typeparam>
    public class DefaultEventManager<TEvent> : EventManagerBase<TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// 事件分发器。
        /// </summary>
        private IEventDispatcher<TEvent> _eventDispatcher = null;

        /// <summary>
        /// 发布事件
        /// </summary>
        /// <param name="_event">事件信息</param>
        public override void Publish(TEvent _event)
        {
            if (this._eventDispatcher == null)
            {
                this._eventDispatcher = new DefaultEventDispatcher<TEvent>(this._eventHandlerGroup);
            }

            this._eventDispatcher.Handle(_event);
        }
    }
}
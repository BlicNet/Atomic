﻿namespace Atomic.Events
{
    using System;

    /// <summary>
    /// 事件处理器基类。
    /// </summary>
    public abstract class EventHandlerBase : IEventHandler
    {
        /// <summary>
        /// 初始化事件处理器基类。
        /// </summary>
        /// <param name="level">处理器等级。</param>
        public EventHandlerBase(EventHandlerLevel level)
        {
            this.Level = level;
        }

        /// <summary>
        /// 唯一标识。
        /// </summary>
        public Guid Id { get; protected set; }

        /// <summary>
        /// 事件类型。
        /// </summary>
        public Type EventType { get; protected set; }

        /// <summary>
        /// 处理器等级。
        /// </summary>
        /// <remarks>
        /// 主处理器。
        /// 子处理器。
        /// </remarks>
        public EventHandlerLevel Level { get; protected set; }

        /// <summary>
        /// 处理事件。
        /// </summary>
        /// <param name="_event">事件信息</param>
        /// <returns>是否处理完成。</returns>
        public abstract bool Handle(object _event);
    }

    /// <summary>
    /// 事件处理器基类。
    /// </summary>
    /// <typeparam name="TEvent">事件类型。</typeparam>
    public abstract class EventHandlerBase<TEvent> : EventHandlerBase, IEventHandler<TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// 初始化。
        /// </summary>
        public EventHandlerBase()
            : this(EventHandlerLevel.Master)
        {
        }

        /// <summary>
        /// 初始化。
        /// </summary>
        public EventHandlerBase(EventHandlerLevel level)
            : base(level)
        {
            this.Id = Guid.NewGuid();

            this.EventType = typeof(TEvent);
        }

        /// <summary>
        /// 事件处理
        /// </summary>
        /// <param name="event">事件信息</param>
        /// <returns>是否处理完成。</returns>
        public virtual bool Handle(TEvent @event)
        {
            if (@event == null)
            {
                throw new ArgumentNullException("@event");
            }

            return this.OnHandle(@event);
        }

        /// <summary>
        /// 处理事件。
        /// </summary>
        /// <param name="_event">事件信息</param>
        /// <returns>是否处理完成。</returns>
        public override bool Handle(object _event)
        {
            return this.Handle((TEvent)_event);
        }

        /// <summary>
        /// 处理事件。
        /// </summary>
        /// <param name="event">事件信息</param>
        /// <returns>是否处理完成。</returns>
        protected abstract bool OnHandle(TEvent @event);
    }
}
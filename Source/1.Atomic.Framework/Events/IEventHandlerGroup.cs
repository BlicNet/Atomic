﻿namespace Atomic.Events
{
    using System.Collections.Generic;

    /// <summary>
    /// 事件处理器组接口
    /// </summary>
    /// <typeparam name="TEvent">事件类型</typeparam>
    public interface IEventHandlerGroup<TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// 填充事件处理器
        /// </summary>
        /// <param name="eventHandlers">事件处理器</param>
        /// <returns>事件处理器组</returns>
        IEventHandlerGroup<TEvent> EventHandlersWith(params IEventHandler<TEvent>[] eventHandlers);

        /// <summary>
        /// 获取所有同步事件处理器
        /// </summary>
        /// <returns>事件处理器集合</returns>
        IEnumerable<IEventHandler<TEvent>> GetMasterEventHandlers();

        /// <summary>
        /// 获取所有异步事件处理器
        /// </summary>
        /// <returns>事件处理器集合</returns>
        IEnumerable<IEventHandler<TEvent>> GetChildEventHandlers();

        /// <summary>
        /// 清理事件处理器。
        /// </summary>
        void Clear();
    }
}
﻿namespace Atomic.Events
{
    /// <summary>
    /// 事件管理器
    /// </summary>
    public interface IEventManager
    {
        /// <summary>
        /// 填充事件处理器
        /// </summary>
        /// <param name="eventHandler">事件处理器</param>
        /// <returns>事件处理器组</returns>
        void EventHandlersWith(IEventHandler eventHandler);

        /// <summary>
        /// 发布事件
        /// </summary>
        /// <param name="_event">事件信息</param>
        void Publish(object _event);

        /// <summary>
        /// 清理事件处理器。
        /// </summary>
        void Clear();
    }

    /// <summary>
    /// 事件管理器
    /// </summary>
    /// <typeparam name="TEvent">事件类型</typeparam>
    public interface IEventManager<TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// 填充事件处理器
        /// </summary>
        /// <param name="eventHandlers">事件处理器</param>
        /// <returns>事件处理器组</returns>
        IEventHandlerGroup<TEvent> EventHandlersWith(params IEventHandler<TEvent>[] eventHandlers);

        /// <summary>
        /// 发布事件
        /// </summary>
        /// <param name="_event">事件信息</param>
        void Publish(TEvent _event);

        /// <summary>
        /// 清理事件处理器。
        /// </summary>
        void Clear();
    }

    /// <summary>
    /// 异步事件管理器
    /// </summary>
    /// <typeparam name="TEvent">事件类型</typeparam>
    public interface IEventBusAsync<TEvent> : IEventManager<TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// 执行处理
        /// </summary>
        void Execute();
    }
}
﻿namespace Atomic.Events
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 事件总线容器。
    /// </summary>
    public static class EventManagerContainer
    {
        /// <summary>
        /// 实例
        /// </summary>
        public static IDictionary<Type, object> _instance = new Dictionary<Type, object>();

        /// <summary>
        /// 获得事件管理器实例。
        /// </summary>
        /// <typeparam name="TEvent">事件类型。</typeparam>
        /// <returns></returns>
        public static IEventManager<TEvent> GetInstance<TEvent>() where TEvent : IEvent
        {
            Type key = typeof(TEvent);

            if (_instance.ContainsKey(key))
            {
                return _instance[key] as IEventManager<TEvent>;
            }

            IEventManager<TEvent> eventManager = new DefaultEventManager<TEvent>();

            _instance[key] = eventManager;

            return eventManager;
        }
    }

    /// <summary>
    /// 事件总线容器。
    /// </summary>
    /// <typeparam name="TEvent">事件类型。</typeparam>
    public static class EventManagerContainer<TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// 实例。
        /// </summary>
        public static IEventManager<TEvent> Instance { get; set; }
    }
}
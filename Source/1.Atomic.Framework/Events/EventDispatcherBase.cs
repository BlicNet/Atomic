﻿namespace Atomic.Events
{
    /// <summary>
    /// 事件分发器基类
    /// </summary>
    /// <typeparam name="TEvent">事件类型</typeparam>
    public abstract class EventDispatcherBase<TEvent> : IEventDispatcher<TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// 事件处理器组
        /// </summary>
        protected IEventHandlerGroup<TEvent> _eventHandlerGroup = null;

        /// <summary>
        /// 初始化事件分发器
        /// </summary>
        /// <param name="eventHandlerGroup">事件处理器组</param>
        public EventDispatcherBase(IEventHandlerGroup<TEvent> eventHandlerGroup)
        {
            this._eventHandlerGroup = eventHandlerGroup;
        }

        /// <summary>
        /// 处理事件
        /// </summary>
        /// <param name="event">事件信息</param>
        public void Handle(TEvent @event)
        {
            if (@event != null)
            {
                this.OnHandle(@event);
            }
        }

        /// <summary>
        /// 处理事件
        /// </summary>
        /// <param name="event">事件信息</param>
        protected abstract void OnHandle(TEvent @event);
    }
}
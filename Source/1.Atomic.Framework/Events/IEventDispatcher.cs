﻿namespace Atomic.Events
{
    /// <summary>
    /// 事件分发器
    /// </summary>
    /// <typeparam name="TEvent">事件类型</typeparam>
    public interface IEventDispatcher<TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// 处理事件
        /// </summary>
        /// <param name="event">事件列表</param>
        void Handle(TEvent @event);
    }

    /// <summary>
    /// 事件分发器
    /// </summary>
    /// <typeparam name="TEvent">事件类型</typeparam>
    public interface IBatchEventDispatcher<TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// 处理事件
        /// </summary>
        /// <param name="events">事件列表</param>
        void Handle(TEvent[] events);
    }
}
﻿namespace Atomic.Events
{
    using System.Collections.Generic;

    /// <summary>
    /// 事件处理器组
    /// </summary>
    /// <typeparam name="TEvent">事件类型</typeparam>
    public class EventHandlerGroup<TEvent> : IEventHandlerGroup<TEvent> where TEvent : IEvent
    {
        /// <summary>
        /// 主事件处理器组。
        /// </summary>
        private List<IEventHandler<TEvent>> _masterEventHandlers = null;

        /// <summary>
        /// 子事件处理器组。
        /// </summary>
        private List<IEventHandler<TEvent>> _childEventHandlers = null;

        /// <summary>
        /// 构造器。
        /// </summary>
        /// <param name="eventHandlers">事件处理器数组。</param>
        public EventHandlerGroup(IEventHandler<TEvent>[] eventHandlers)
        {
            this.AddEventHandlers(eventHandlers);
        }

        /// <summary>
        /// 填充事件处理器
        /// </summary>
        /// <param name="eventHandlers">事件处理器</param>
        /// <returns>事件处理器组</returns>
        public IEventHandlerGroup<TEvent> EventHandlersWith(params IEventHandler<TEvent>[] eventHandlers)
        {
            if (eventHandlers != null)
            {
                this.AddEventHandlers(eventHandlers);
            }

            return this;
        }

        /// <summary>
        /// 获取所有同步事件处理器
        /// </summary>
        /// <returns>事件处理器集合</returns>
        public IEnumerable<IEventHandler<TEvent>> GetMasterEventHandlers()
        {
            return _masterEventHandlers;
        }

        /// <summary>
        /// 获取所有异步事件处理器
        /// </summary>
        /// <returns>事件处理器集合</returns>
        public IEnumerable<IEventHandler<TEvent>> GetChildEventHandlers()
        {
            return this._childEventHandlers;
        }

        /// <summary>
        /// 清理事件处理器。
        /// </summary>
        public void Clear()
        {
            //同步事件处理器组
            this._masterEventHandlers.Clear();

            //异步事件处理器组
            this._childEventHandlers.Clear();
        }

        /// <summary>
        /// 增加事件处理器
        /// </summary>
        /// <param name="eventHandlers">事件处理器组</param>
        private void AddEventHandlers(IEventHandler<TEvent>[] eventHandlers)
        {
            if (eventHandlers != null)
            {
                if (this._masterEventHandlers == null)
                {
                    this._masterEventHandlers = new List<IEventHandler<TEvent>>();
                }

                if (this._childEventHandlers == null)
                {
                    this._childEventHandlers = new List<IEventHandler<TEvent>>();
                }

                foreach (var eventHandler in eventHandlers)
                {
                    if (EventHandlerLevel.Master == eventHandler.Level)
                    {
                        this._masterEventHandlers.Add(eventHandler);
                    }

                    if (EventHandlerLevel.Child == eventHandler.Level)
                    {
                        this._childEventHandlers.Add(eventHandler);
                    }
                }
            }
        }
    }
}
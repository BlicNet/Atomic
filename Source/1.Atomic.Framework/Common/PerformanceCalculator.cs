﻿namespace Atomic.Common
{
    using System;
    using System.Diagnostics;
    
    /// <summary>
    /// 性能计算器
    /// </summary>
    public static class PerformanceCalculator
    {
        /// <summary>
        /// 计算消耗时间
        /// </summary>
        /// <param name="body">将为每个迭代调用一次的委托。</param>
        /// <returns>执行时间（单位：毫秒）</returns>
        public static long ExpendTime(Action body)
        {
            Stopwatch watch = new Stopwatch();

            watch.Start();

            body();

            watch.Stop();

            return watch.ElapsedMilliseconds;
        }

        /// <summary>
        /// 计算消耗时间
        /// </summary>
        /// <param name="fromInclusive">开始索引。</param>
        /// <param name="toExclusive">结束索引。</param>
        /// <param name="body">将为每个迭代调用一次的委托。</param>
        /// <returns>执行时间（单位：毫秒）</returns>
        public static long ExpendTime(int fromInclusive, int toExclusive, Action body)
        {
            Stopwatch watch = new Stopwatch();

            watch.Start();

            for (int index = fromInclusive; index < toExclusive; index++)
            {
                body();
            }

            watch.Stop();

            return watch.ElapsedMilliseconds;
        }
    }
}
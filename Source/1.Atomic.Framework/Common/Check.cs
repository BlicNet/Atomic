﻿namespace Atomic.Common
{
    using System;
    using System.Globalization;

    /// <summary>
    /// 验证。
    /// </summary>
    internal class Check
    {
        /// <summary>
        /// 验证对象不为 null。
        /// </summary>
        /// <typeparam name="T">验证对象的类型。</typeparam>
        /// <param name="value">验证对象。</param>
        /// <param name="parameterName">异常信息参数名。</param>
        /// <returns>返回验证对象。</returns>
        public static T NotNull<T>(T value, string parameterName) where T : class
        {
            if (value == null)
            {
                throw new ArgumentNullException(parameterName);
            }

            return value;
        }

        /// <summary>
        /// 验证对象不为 null。
        /// </summary>
        /// <typeparam name="T">验证对象的类型。</typeparam>
        /// <param name="value">验证对象。</param>
        /// <param name="parameterName">异常信息参数名。</param>
        /// <param name="message">异常信息。</param>
        /// <param name="args">异常信息的字符串参数。</param>
        /// <returns>返回验证对象。</returns>
        public static T NotNull<T>(T value, string parameterName, string message, params object[] args) where T : class
        {
            if (value == null)
            {
                throw new ArgumentNullException(parameterName, string.Format(CultureInfo.InvariantCulture, message, args));
            }

            return value;
        }

        /// <summary>
        /// 验证 struct 不为 null。
        /// </summary>
        /// <typeparam name="T">验证 struct 的类型。</typeparam>
        /// <param name="value">验证 struct。</param>
        /// <param name="parameterName">异常信息参数名。</param>
        /// <returns>返回 struct。</returns>
        public static T? NotNull<T>(T? value, string parameterName) where T : struct
        {
            if (value == null)
            {
                throw new ArgumentNullException(parameterName);
            }

            return value;
        }

        /// <summary>
        /// 验证 struct 不为 null。
        /// </summary>
        /// <typeparam name="T">验证 struct 的类型。</typeparam>
        /// <param name="value">验证 struct。</param>
        /// <param name="parameterName">异常信息参数名。</param>
        /// <param name="message">异常信息。</param>
        /// <param name="args">异常信息的字符串参数。</param>
        /// <returns>返回验证 struct。</returns>
        public static T? NotNull<T>(T? value, string parameterName, string message, params object[] args) where T : struct
        {
            if (value == null)
            {
                throw new ArgumentNullException(parameterName, string.Format(CultureInfo.InvariantCulture, message, args));
            }

            return value;
        }

        /// <summary>
        /// 验证字符串不是 null、空字符串和空白字符串中的一个。
        /// </summary>
        /// <param name="value">要验证的字符串。</param>
        /// <param name="parameterName">异常信息参数名。</param>
        /// <returns>要验证的字符串。</returns>
        public static string NotEmpty(string value, string parameterName)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException(parameterName);
            }

            return value;
        }
    }
}
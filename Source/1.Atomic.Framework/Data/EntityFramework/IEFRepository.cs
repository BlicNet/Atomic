﻿namespace Atomic.Data.EntityFramework
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// Entity Framework 数据仓库。
    /// </summary>
    /// <typeparam name="TEntityType">实体类型。</typeparam>
    public interface IEFRepository<TEntityType> : IRepository where TEntityType : class
    {
        /// <summary>
        /// 创建实体。
        /// </summary>
        /// <param name="entity">实体信息。</param>
        void Create(TEntityType entity);

        /// <summary>
        /// 更新实体。
        /// </summary>
        /// <param name="entity">实体信息。</param>
        void Update(TEntityType entity);

        /// <summary>
        /// 删除实体。
        /// </summary>
        /// <param name="entity">实体信息。</param>
        void Delete(TEntityType entity);

        /// <summary>
        /// 提交。
        /// </summary>
        void Flush();

        /// <summary>
        /// 根据 id 获得单个实体。
        /// </summary>
        /// <param name="id">唯一标识。</param>
        /// <returns>实体信息。</returns>
        TEntityType Get(object id);

        /// <summary>
        /// 根据表达式条件获得单个实体。
        /// </summary>
        /// <param name="predicate">表达式条件。</param>
        /// <returns>实体信息。</returns>
        TEntityType Get(Expression<Func<TEntityType, bool>> predicate);

        /// <summary>
        /// 查询。
        /// </summary>
        /// <returns>查询集合。</returns>
        IQueryable<TEntityType> Query();
    }
}
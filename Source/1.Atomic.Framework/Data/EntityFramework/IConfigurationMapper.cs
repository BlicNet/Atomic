﻿namespace Atomic.Data.EntityFramework
{
    using System.Data.Entity;

    /// <summary>
    /// 实体信息配置映射器接口。
    /// </summary>
    public interface IConfigurationMapper
    {
        /// <summary>
        /// 执行映射。
        /// </summary>
        /// <param name="modelBuilder">实体模型绑定器。</param>
        void Execute(DbModelBuilder modelBuilder);
    }
}
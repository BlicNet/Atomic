﻿namespace Atomic.Data.EntityFramework
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// 配置自动进行数据迁移。
    /// </summary>
    internal sealed class AutoDbMigrationsConfiguration : DbMigrationsConfiguration<AtomicObjectContext>
    {
        /// <summary>
        /// 初始化配置自动进行数据迁移。
        /// </summary>
        public AutoDbMigrationsConfiguration()
        {
            //任何的实体修改都会自动进行数据迁移。
            AutomaticMigrationsEnabled = true;

            //设置数据丢失判断，如果是 true 表示出现数据丢失的情况时不提示并继续执行，是 false 则代表会出现数据丢失之前抛出异常。
            //即：
            //设置成 true 时在系统异常时有可能出现数据丢失并且不提示。
            //设置成 false 时在系统异常时并且会出现数据丢失时抛出异常，不会造成数据丢失的情况。
            AutomaticMigrationDataLossAllowed = false;
        }

        /// <summary>
        /// 在升级到最新迁移以允许更新种子数据后运行。
        /// </summary>
        /// <param name="context">要用于更新种子数据的上下文。</param>
        protected override void Seed(AtomicObjectContext context)
        {
            base.Seed(context);
        }
    }
}
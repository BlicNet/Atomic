﻿namespace Atomic.Data.EntityFramework
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    /// <summary>
    /// EF 数据上下文。
    /// </summary>
    public interface IDbContext
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        DbEntityEntry Entry(object entity);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// 数据访问器。
        /// </summary>
        /// <typeparam name="TEntity">实体类型。</typeparam>
        /// <returns>数据访问器。</returns>
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>
        /// 提交。
        /// </summary>
        /// <returns></returns>
        int SaveChanges();

        /// <summary>
        /// 创建一个原始 SQL 查询，该查询将返回给定类型的元素。类型可以是包含与从查询返回的列名匹配的属性的任何类型，也可以是简单的基元类型。
        /// </summary>
        /// <typeparam name="TElement">实体类型。</typeparam>
        /// <param name="sql">命令字符串。</param>
        /// <param name="parameters">要应用于命令字符串的参数。</param>
        /// <returns>实体集合。</returns>
        IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters);

        /// <summary>
        /// 对数据库执行给定的 DDL/DML 命令。
        /// </summary>
        /// <param name="sql">命令字符串。</param>
        /// <param name="parameters">要应用于命令字符串的参数。</param>
        /// <returns>执行命令后由数据库返回的结果。</returns>
        int ExecuteSqlCommand(string sql, params object[] parameters);
    }
}
﻿namespace Atomic.Data.EntityFramework
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    /// <summary>
    /// DbContext 扩展。
    /// </summary>
    public static class DbContextExtensions
    {
        /// <summary>
        /// 获取给定实体的 System.Data.Entity.Infrastructure.DbEntityEntry 对象，以便提供对与该实体有关的信息的访问以及对实体执行操作的功能。
        /// </summary>
        /// <typeparam name="TDbContext">EF 上下文类型。</typeparam>
        /// <param name="context">EF 上下文。</param>
        /// <param name="entity">实体。</param>
        /// <returns>实体信息。</returns>
        public static DbEntityEntry Entry<TDbContext>(this TDbContext context, object entity) where TDbContext : DbContext, IDbContext
        {
            return context.Entry(entity);
        }
    }
}
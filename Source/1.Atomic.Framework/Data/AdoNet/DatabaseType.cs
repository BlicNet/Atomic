﻿namespace Atomic.Data.AdoNet
{
    /// <summary>
    /// 数据库类型。
    /// </summary>
    public enum DatabaseType
    {
        /// <summary>
        /// SQLServer 数据库。
        /// </summary>
        SQLServer = 1,

        /// <summary>
        /// MySQL 数据库。
        /// </summary>
        MySQL = 2,

        /// <summary>
        /// Oracle 数据库。
        /// </summary>
        Oracle = 3,
    }
}

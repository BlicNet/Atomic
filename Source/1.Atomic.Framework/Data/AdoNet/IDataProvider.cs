﻿namespace Atomic.Data.AdoNet
{
    using System.Data;
    using System.Data.Common;
    using System.Threading.Tasks;

    /// <summary>
    /// 数据提供程序接口。
    /// </summary>
    public interface IDataProvider
    {
        /// <summary>
        /// 查询数据。
        /// </summary>
        /// <param name="commandType">命令类型。</param>
        /// <param name="sql">SQL 语句或者存储过程名称。</param>
        /// <param name="commandParameters">SQL 参数。</param>
        /// <returns>数据读取器。</returns>
        IDataReader SqlQuery(CommandType commandType, string sql, params IDataParameter[] commandParameters);

        /// <summary>
        /// 查询数据。
        /// </summary>
        /// <param name="commandType">命令类型。</param>
        /// <param name="sql">SQL 语句或者存储过程名称。</param>
        /// <param name="commandParameters">SQL 参数。</param>
        /// <returns>数据读取器。</returns>
        Task<DbDataReader> SqlQueryAsync(CommandType commandType, string sql, params IDataParameter[] commandParameters);

        /// <summary>
        /// 查询单条数据。
        /// </summary>
        /// <param name="commandType">命令类型。</param>
        /// <param name="sql">SQL 语句或者存储过程名称。</param>
        /// <param name="commandParameters">SQL 参数。</param>
        /// <returns>查询结果。</returns>
        object Single(CommandType commandType, string sql, params IDataParameter[] commandParameters);

        /// <summary>
        /// 查询单条数据。
        /// </summary>
        /// <param name="commandType">命令类型。</param>
        /// <param name="sql">SQL 语句或者存储过程名称。</param>
        /// <param name="commandParameters">SQL 参数。</param>
        /// <returns>查询结果。</returns>
        Task<object> SingleAsync(CommandType commandType, string sql, params IDataParameter[] commandParameters);

        /// <summary>
        /// 执行 SQL 命令。
        /// </summary>
        /// <param name="commandType">命令类型。</param>
        /// <param name="sql">SQL 语句或者存储过程名称。</param>
        /// <param name="commandParameters">SQL 参数。</param>
        /// <returns>影响行数。</returns>
        int ExecuteSqlCommand(CommandType commandType, string sql, params IDataParameter[] commandParameters);

        /// <summary>
        /// 执行 SQL 命令。
        /// </summary>
        /// <param name="commandType">命令类型。</param>
        /// <param name="sql">SQL 语句或者存储过程名称。</param>
        /// <param name="commandParameters">SQL 参数。</param>
        /// <returns>影响行数。</returns>
        Task<int> ExecuteSqlCommandAsync(CommandType commandType, string sql, params IDataParameter[] commandParameters);
    }
}
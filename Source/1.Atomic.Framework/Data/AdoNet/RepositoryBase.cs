﻿namespace Atomic.Data.AdoNet
{
    /// <summary>
    /// 数据仓库基类。
    /// </summary>
    public abstract class RepositoryBase : IRepository
    {
        /// <summary>
        /// 初始化数据仓库基类。
        /// </summary>
        public RepositoryBase()
            : this(DatabaseType.SQLServer)
        {
        }

        /// <summary>
        /// 初始化数据仓库基类。
        /// </summary>
        /// <param name="databaseType">数据库类型。</param>
        public RepositoryBase(DatabaseType databaseType)
            : this(string.Empty, databaseType)
        {
        }

        /// <summary>
        /// 初始化数据仓库基类。
        /// </summary>
        /// <param name="connectionString">数据库连接字符串。</param>
        /// <param name="databaseType">数据库类型。</param>
        public RepositoryBase(string connectionString, DatabaseType databaseType)
        {
            switch (databaseType)
            {
                case DatabaseType.SQLServer:
                    this.DataProvider = new SQLServerDataProvider(connectionString);
                    break;
                //case DatabaseType.MySQL:
                //    this.DataProvider = new MySQLDataProvider(connectionString);
                //    break;
                default:
                    this.DataProvider = new SQLServerDataProvider(connectionString);
                    break;
            }
        }

        /// <summary>
        /// 数据提供者。
        /// </summary>
        protected IDataProvider DataProvider { get; private set; }
    }
}
﻿namespace Atomic.Data.AdoNet
{
    using System.Data.Common;

    using MySql.Data.MySqlClient;

    /// <summary>
    /// MySQL 数据库访问器。
    /// </summary>
    public class MySQLDataProvider : DataProviderBase
    {
        /// <summary>
        /// 初始化SQL MySQL 数据库访问器。
        /// </summary>
        public MySQLDataProvider()
        {
        }

        /// <summary>
        /// 初始化SQL MySQL 数据库访问器。
        /// </summary>
        /// <param name="connectionString">数据库连接字符串。</param>
        public MySQLDataProvider(string connectionString)
            : base(connectionString)
        {
        }

        /// <summary>
        /// 获取数据库连接对象工厂。
        /// </summary>
        /// <returns>数据库连接对象工厂。</returns>
        protected override DbProviderFactory GetDbProviderFactory()
        {
            return MySqlClientFactory.Instance;
        }
    }
}
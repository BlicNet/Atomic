﻿namespace Atomic.Data.AdoNet
{
    using System.Data.Common;
    using System.Data.SqlClient;

    /// <summary>
    /// SQL Server 数据库访问器。
    /// </summary>
    public class SQLServerDataProvider : DataProviderBase
    {
        /// <summary>
        /// 初始化SQL Server 数据库访问器。
        /// </summary>
        public SQLServerDataProvider()
        {
        }

        /// <summary>
        /// 初始化SQL Server 数据库访问器。
        /// </summary>
        /// <param name="connectionString">数据库连接字符串。</param>
        public SQLServerDataProvider(string connectionString)
            : base(connectionString)
        {
        }

        /// <summary>
        /// 获取数据库连接对象工厂。
        /// </summary>
        /// <returns>数据库连接对象工厂。</returns>
        protected override DbProviderFactory GetDbProviderFactory()
        {
            return SqlClientFactory.Instance;
        }
    }
}
﻿namespace Atomic.Data
{
    using System.Configuration;
    using System.Collections.Specialized;

    /// <summary>
    /// 配置管理器
    /// </summary>
    public class DefaultConfigurationManager : IConfigurationManager
    {
        /// <summary>
        /// 数据库链接名称。
        /// </summary>
        public string ConnectionName 
        {
            get 
            {
                return "DefaultConnection";
            }
        }

        /// <summary>
        /// 获得 AppSettings 节点对象。
        /// </summary>
        /// <returns>AppSettings 节点对象。</returns>
        public NameValueCollection GetAppSettings()
        {
            return ConfigurationManager.AppSettings;
        }

        /// <summary>
        /// 获得 ConnectionStrings 节点对象。
        /// </summary>
        /// <returns>ConnectionStrings 节点对象。</returns>
        public ConnectionStringSettingsCollection GetConnectionStrings()
        {
            return ConfigurationManager.ConnectionStrings;
        }

        /// <summary>
        /// 获取数据库连接
        /// </summary>
        /// <returns>数据库连接配置。</returns>
        public ConnectionStringSettings GetDefaultConnection()
        {
            return ConfigurationManager.ConnectionStrings[this.ConnectionName];
        }
    }
}
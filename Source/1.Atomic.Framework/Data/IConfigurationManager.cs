﻿namespace Atomic.Data
{
    using System.Collections.Specialized;
    using System.Configuration;

    /// <summary>
    /// 配置管理器
    /// </summary>
    public interface IConfigurationManager
    {
        /// <summary>
        /// 数据库链接名称。
        /// </summary>
        string ConnectionName { get; }

        /// <summary>
        /// 获得 AppSettings 节点对象。
        /// </summary>
        /// <returns>AppSettings 节点对象。</returns>
        NameValueCollection GetAppSettings();

        /// <summary>
        /// 获得 ConnectionStrings 节点对象。
        /// </summary>
        /// <returns>ConnectionStrings 节点对象。</returns>
        ConnectionStringSettingsCollection GetConnectionStrings();

        /// <summary>
        /// 获取默认数据库连接配置。
        /// </summary>
        /// <returns>数据库连接配置。</returns>
        ConnectionStringSettings GetDefaultConnection();
    }
}

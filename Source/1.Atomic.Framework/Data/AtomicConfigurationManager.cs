﻿namespace Atomic.Data
{
    using System;

    /// <summary>
    /// 配置管理器。
    /// </summary>
    public class AtomicConfigurationManager
    {
        #region Members

        /// <summary>
        /// 配置管理器实例。
        /// </summary>
        private static AtomicConfigurationManager _instance = new AtomicConfigurationManager();

        /// <summary>
        /// 当前配置管理器。
        /// </summary>
        private IConfigurationManager _current = new DefaultConfigurationManager();

        #endregion

        #region Properties

        /// <summary>
        /// 当前配置管理器。
        /// </summary>
        public static IConfigurationManager Current
        {
            get
            {
                return _instance.InnerCurrent;
            }
        }

        /// <summary>
        /// 当前配置管理器。
        /// </summary>
        private IConfigurationManager InnerCurrent
        {
            get
            {
                return _current;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 设置当前使用的配置管理器。
        /// </summary>
        /// <param name="configurationManager">配置管理器。</param>
        public static void SetConfigurationManager(IConfigurationManager configurationManager)
        {
            _instance.InnerSetConfigurationManager(configurationManager);
        }

        #endregion

        #region Pricate Method

        /// <summary>
        /// 设置当前使用的配置管理器。
        /// </summary>
        /// <param name="configurationManager">配置管理器。</param>
        private void InnerSetConfigurationManager(IConfigurationManager configurationManager)
        {
            if (configurationManager == null)
            {
                throw new ArgumentNullException("configurationManager");
            }

            _current = configurationManager;
        }

        #endregion
    }
}
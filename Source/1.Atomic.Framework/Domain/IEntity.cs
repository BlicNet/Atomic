﻿namespace Atomic.Domain
{
    /// <summary>
    /// 实体接口
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// 实体编号。
        /// </summary>
        int Id { get; }

        /// <summary>
        /// 表示该实体是否是临时的。
        /// </summary>
        /// <returns>临时的则返回 true，否则返回 false。</returns>
        bool IsTransient();

        /// <summary>
        /// 改变当前实体编号。
        /// </summary>
        /// <param name="identity">实体编号。</param>
        void ChangeCurrentIdentity(int identity);
    }
}
﻿namespace Atomic.Plugins
{
    using System.Linq;
    using System.Collections.Generic;

    using Atomic.Extensions;
    using Atomic.Plugins.Files;

    /// <summary>
    /// 默认插件管理器。
    /// </summary>
    public class DefaultPluginManager : IPluginManager
    {
        /// <summary>
        /// 插件加载器。
        /// </summary>
        private readonly IPluginLoader _pluginLoader = null;

        /// <summary>
        /// 插件目录集合。
        /// </summary>
        private readonly IEnumerable<IPluginFolder> _pluginFolders = null;

        /// <summary>
        /// 初始化默认插件管理器。
        /// </summary>
        /// <param name="pluginLoader">插件加载器。</param>
        /// <param name="pluginFolders">插件目录列表。</param>
        public DefaultPluginManager(IPluginLoader pluginLoader, IEnumerable<IPluginFolder> pluginFolders)
        {
            this._pluginFolders = pluginFolders;
            this._pluginLoader = pluginLoader;
        }

        /// <summary>
        /// 获得可用的插件集合。
        /// </summary>
        /// <returns>插件描述集合。</returns>
        public IEnumerable<PluginDescriptor> AvailablePlugins()
        {
            return this._pluginFolders.SelectMany(folder => folder.AvailablePlugins()).ToReadOnlyCollection();
        }

        /// <summary>
        /// 启动所有插件。
        /// </summary>
        public void EnabledPlugins()
        {
            //激活插件。
            this.AvailablePlugins().ForEach(plugin => this._pluginLoader.Activate(plugin));
        }
    }
}
﻿namespace Atomic.Plugins
{
    using System;

    /// <summary>
    /// 插件信息。
    /// </summary>
    public class PluginDescriptor
    {
        /// <summary>
        /// 唯一标识。
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 名称。
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 版本号。
        /// </summary>
        public Version Version { get; set; }

        /// <summary>
        /// 描述。
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 插件网址。
        /// </summary>
        public string WebSite { get; set; }

        /// <summary>
        /// 作者。
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 插件位置。
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// 关联。
        /// </summary>
        public DependencyDescriptor Dependency { get; set; }
    }
}
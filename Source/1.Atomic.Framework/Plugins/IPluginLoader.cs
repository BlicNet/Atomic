﻿namespace Atomic.Plugins
{
    /// <summary>
    /// 插件加载器。
    /// </summary>
    public interface IPluginLoader
    {
        /// <summary>
        /// 激活。
        /// </summary>
        /// <param name="pluginDescriptor">插件描述信息。</param>
        void Activate(PluginDescriptor pluginDescriptor);
    }
}
﻿namespace Atomic.Plugins
{
    using System.Collections.Generic;

    /// <summary>
    /// 插件目录。
    /// </summary>
    public class PluginFolder : IPluginFolder
    {
        /// <summary>
        /// 插件根目录。
        /// </summary>
        private readonly IEnumerable<string> _paths = null;

        /// <summary>
        /// 插件获得者。
        /// </summary>
        private readonly IPluginHarvester _pluginHarvester = null;

        /// <summary>
        /// 初始化插件目录。
        /// </summary>
        /// <param name="pluginHarvester">插件获得者。</param>
        public PluginFolder(IPluginHarvester pluginHarvester)
        {
            this._pluginHarvester = pluginHarvester;

            this._paths = new[] { "~/Plugins" };
        }

        /// <summary>
        /// 获得可用的插件集合。
        /// </summary>
        /// <returns>插件描述信息集合。</returns>
        public IEnumerable<PluginDescriptor> AvailablePlugins()
        {
            return this._pluginHarvester.HarvestPlugins(this._paths, "plugin.txt");
        }
    }
}
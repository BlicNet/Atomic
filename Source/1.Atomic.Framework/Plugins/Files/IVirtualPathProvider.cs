﻿namespace Atomic.Plugins.Files
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// 虚拟路径提供程序。
    /// </summary>
    public interface IVirtualPathProvider
    {
        /// <summary>
        /// 将路径数组组合成一个路径。
        /// </summary>
        /// <param name="paths">路径数组。</param>
        /// <returns>路径。</returns>
        string Combine(params string[] paths);

        /// <summary>
        /// 将虚拟路径转换成物理路径。
        /// </summary>
        /// <param name="virtualPath">虚拟路径。</param>
        /// <returns>物理路径。</returns>
        string MapPath(string virtualPath);

        /// <summary>
        /// 判断文件是否存在于虚拟系统里。
        /// </summary>
        /// <param name="virtualPath">虚拟路径。</param>
        /// <returns>文件是否存在。</returns>
        bool FileExists(string virtualPath);

        /// <summary>
        /// 从虚拟文件系统里获取一个虚拟文件并打开。
        /// </summary>
        /// <param name="virtualPath">虚拟路径。</param>
        /// <returns>流</returns>
        Stream OpenFile(string virtualPath);

        /// <summary>
        /// 创建一个文本文件。
        /// </summary>
        /// <param name="virtualPath">虚拟路径。</param>
        /// <returns></returns>
        StreamWriter CreateText(string virtualPath);

        /// <summary>
        /// 创建一个文件。
        /// </summary>
        /// <param name="virtualPath">虚拟路径。</param>
        /// <returns></returns>
        Stream CreateFile(string virtualPath);

        /// <summary>
        /// 返回上次写入指定的文件或目录的日期和时间，其格式为协调世界时 (UTC)。
        /// </summary>
        /// <param name="virtualPath">虚拟路径。</param>
        /// <returns>UTC 时间。</returns>
        DateTime GetFileLastWriteTimeUtc(string virtualPath);

        /// <summary>
        /// 判断目录是否存在于虚拟系统里。
        /// </summary>
        /// <param name="virtualPath">虚拟路径。</param>
        /// <returns>目录是否存在。</returns>
        bool DirectoryExists(string virtualPath);

        /// <summary>
        /// 创建目录。
        /// </summary>
        /// <param name="virtualPath">虚拟路径。</param>
        void CreateDirectory(string virtualPath);

        /// <summary>
        /// 获得目录名称。
        /// </summary>
        /// <param name="virtualPath">虚拟路径。</param>
        /// <returns>目录名称。</returns>
        string GetDirectoryName(string virtualPath);

        /// <summary>
        /// 删除目录。
        /// </summary>
        /// <param name="virtualPath">虚拟路径。</param>
        void DeleteDirectory(string virtualPath);

        /// <summary>
        /// 指定路径的所有文件列表。
        /// </summary>
        /// <param name="path">路径。</param>
        /// <returns>文件列表。</returns>
        IEnumerable<string> ListFiles(string path);

        /// <summary>
        /// 指定路径的所有子目录列表。
        /// </summary>
        /// <param name="path">路径。</param>
        /// <returns>目录列表。</returns>
        IEnumerable<string> ListDirectories(string path);
    }
}
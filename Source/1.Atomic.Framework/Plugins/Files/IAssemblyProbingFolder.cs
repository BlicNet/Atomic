﻿namespace Atomic.Plugins.Files
{
    using System;
    using System.Reflection;

    /// <summary>
    /// 程序集探测目录。
    /// </summary>
    public interface IAssemblyProbingFolder
    {
        /// <summary>
        /// 判断程序集是否存在。
        /// </summary>
        /// <param name="name">程序集名称。</param>
        /// <returns>是否存在。</returns>
        bool AssemblyExists(string name);

        /// <summary>
        /// 获得指定名称所在的虚拟路径。
        /// </summary>
        /// <param name="name">程序集名称。</param>
        /// <returns>虚拟路径。</returns>
        string GetAssemblyVirtualPath(string name);

        /// <summary>
        /// 加载程序集。
        /// </summary>
        /// <param name="name">程序集名称。</param>
        /// <returns>程序集。</returns>
        Assembly LoadAssembly(string name);

        /// <summary>
        /// 删除程序集。
        /// </summary>
        /// <param name="name">程序集名称。</param>
        void DeleteAssembly(string name);

        /// <summary>
        /// 保存程序集。
        /// </summary>
        /// <remarks>
        /// 将指定名称的程序集复制到 fileName 位置。
        /// </remarks>
        /// <param name="name">程序集名称。</param>
        /// <param name="fileName">保存位置。</param>
        void StoreAssembly(string name, string fileName);

        /// <summary>
        /// 获取程序集最后写入的 UTC 时间。
        /// </summary>
        /// <param name="name">程序集名称。</param>
        /// <returns>UTC 时间。</returns>
        DateTime GetAssemblyDateTimeUtc(string name);
    }
}
﻿namespace Atomic.Plugins.Files
{
    using System.Reflection;

    /// <summary>
    /// 程序集加载器
    /// </summary>
    public interface IAssemblyLoader
    {
        /// <summary>
        /// 加载程序集。
        /// </summary>
        /// <param name="name">程序集名称。</param>
        Assembly Load(string name);

        /// <summary>
        /// 卸载程序集。
        /// </summary>
        /// <param name="name">程序集名称。</param>
        void Unload(string name);
    }
}
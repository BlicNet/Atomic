﻿namespace Atomic.Plugins.Files
{
    using System;
    using System.IO;
    using System.Reflection;

    /// <summary>
    /// 默认程序集加载器。
    /// </summary>
    public class DefaultAssemblyLoader : IAssemblyLoader
    {
        private readonly static AppDomain pluginAppDomain = AppDomain.CreateDomain("pluginAppDomain");

        /// <summary>
        /// 加载程序集。
        /// </summary>
        /// <param name="name">程序集名称。</param>
        /// <returns>程序集。</returns>
        public Assembly Load(string name)
        {
            return Assembly.Load(name);
        }

        /// <summary>
        /// 卸载程序集。
        /// </summary>
        /// <param name="name">程序集名称。</param>
        public void Unload(string name)
        {
        }
    }
}
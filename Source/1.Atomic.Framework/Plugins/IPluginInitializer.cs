﻿namespace Atomic.Plugins
{
    /// <summary>
    /// 插件初始化程序。
    /// </summary>
    public interface IPluginInitializer
    {
        /// <summary>
        /// 初始化。
        /// </summary>
        /// <param name="pluginDescriptor">插件描述信息。</param>
        void Initialize(PluginDescriptor pluginDescriptor);
    }
}
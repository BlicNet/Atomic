﻿namespace Atomic.Plugins
{
    /// <summary>
    /// 初始化信息。
    /// </summary>
    /// <remarks>
    /// 所有插件需要做初始化操作时，就需要实现此接口。
    /// </remarks>
    public interface ISetupPlugin
    {
        /// <summary>
        /// 初始化。
        /// </summary>
        void Initialize();
    }
}
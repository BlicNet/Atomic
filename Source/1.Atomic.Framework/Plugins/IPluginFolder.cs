﻿namespace Atomic.Plugins
{
    using System.Collections.Generic;

    /// <summary>
    /// 插件目录。
    /// </summary>
    public interface IPluginFolder
    {
        /// <summary>
        /// 获得可用的插件集合。
        /// </summary>
        /// <returns>插件描述信息集合。</returns>
        IEnumerable<PluginDescriptor> AvailablePlugins();
    }
}
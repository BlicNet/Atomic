﻿namespace Atomic.Plugins
{
    /// <summary>
    /// 清单文件管理器。
    /// </summary>
    public interface IManifestManager
    {
        /// <summary>
        /// 获得插件描述信息。
        /// </summary>
        /// <param name="pluginId">插件标识。</param>
        /// <param name="location">插件目录。</param>
        /// <param name="manifestPath">插件配置清单。</param>
        /// <returns>插件描述信息。</returns>
        PluginDescriptor GetPluginDescriptor(string pluginId, string location, string manifestPath);
    }
}
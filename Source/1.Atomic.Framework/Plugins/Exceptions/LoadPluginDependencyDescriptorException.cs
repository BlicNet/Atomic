﻿namespace Atomic.Plugins.Exceptions
{
    using System;

    /// <summary>
    /// 加载插件关联信息失败异常。
    /// </summary>
    [Serializable]
    public class LoadPluginDependencyDescriptorException : Exception
    {
        /// <summary>
        /// 初始化加载插件关联信息失败异常。
        /// </summary>
        public LoadPluginDependencyDescriptorException()
            : base("加载插件关联信息失败，请检查插件描述清单填写的“assembly”的值是否与“bin”目录下对应的程序集的名称一致。")
        { 
        }
    }
}
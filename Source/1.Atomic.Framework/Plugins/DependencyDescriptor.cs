﻿namespace Atomic.Plugins
{
    using System.Linq;
    using System.Collections.Generic;

    /// <summary>
    /// 插件关联项描述。
    /// </summary>
    public class DependencyDescriptor
    {
        /// <summary>
        /// 初始化插件关联项描述。
        /// </summary>
        public DependencyDescriptor()
        {
            References = Enumerable.Empty<DependencyReferenceDescriptor>();
        }

        /// <summary>
        /// 名称。
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 路径。
        /// </summary>
        public string VirtualPath { get; set; }

        /// <summary>
        /// 插件引用项的集合。
        /// </summary>
        public IEnumerable<DependencyReferenceDescriptor> References { get; set; }
    }

    /// <summary>
    /// 插件引用关联项描述。
    /// </summary>
    public class DependencyReferenceDescriptor
    {
        /// <summary>
        /// 名称。
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 路径。
        /// </summary>
        public string VirtualPath { get; set; }
    }
}
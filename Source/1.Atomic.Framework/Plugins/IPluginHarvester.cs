﻿namespace Atomic.Plugins
{
    using System.Collections.Generic;

    /// <summary>
    /// 插件获得程序。
    /// </summary>
    public interface IPluginHarvester
    {
        /// <summary>
        /// 获得插件集合。
        /// </summary>
        /// <remarks>
        /// 例：
        /// Name:内容模块
        /// version:1.0
        /// description:用于处理文章信息。
        /// website:http://www.atomic.com
        /// author:cjnmy36723
        /// assembly:Staroce.Contents.dll
        /// ----------------------------------
        /// 其中：assembly 节点是必要的。
        /// 建议“Name”，“version”，“description”，“author”，“assembly”都要填写。
        /// </remarks>
        /// <param name="paths">插件根目录集合。</param>
        /// <param name="manifestName">插件描述的文件名。</param>
        /// <returns>插件集合。</returns>
        IEnumerable<PluginDescriptor> HarvestPlugins(IEnumerable<string> paths, string manifestName);
    }
}
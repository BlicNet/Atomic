﻿namespace Atomic.Plugins
{
    using System.Collections.Generic;

    /// <summary>
    /// 插件管理器。
    /// </summary>
    public interface IPluginManager
    {
        /// <summary>
        /// 获得可用的插件集合。
        /// </summary>
        /// <returns>插件描述集合。</returns>
        IEnumerable<PluginDescriptor> AvailablePlugins();

        /// <summary>
        /// 启动所有插件。
        /// </summary>
        void EnabledPlugins();
    }
}
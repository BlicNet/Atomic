﻿namespace Atomic.Logging
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Security;
    using System.Diagnostics;
    using System.Globalization;

    /// <summary>
    /// 跟踪日志。
    /// </summary>
    public class TraceSourceLogger : ILogger
    {
        #region Members

        /// <summary>
        /// 跟踪源。
        /// </summary>
        private TraceSource source = null;

        #endregion

        #region 初始化跟踪日志

        /// <summary>
        /// 初始化跟踪日志。
        /// </summary>
        public TraceSourceLogger()
        {
            //创建一个源追踪实体。
            source = new TraceSource("Liberty");
        }

        #endregion

        #region private method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="message"></param>
        private void TraceInternal(TraceEventType eventType, string message)
        {

            if (source != null)
            {
                try
                {
                    //指定可输出的等级
                    source.Switch.Level = SourceLevels.Critical | SourceLevels.Error | SourceLevels.Warning | SourceLevels.Information | SourceLevels.Verbose;

                    //增加侦听器
                    source.Listeners.Add(new TextWriterTraceListener(this.GetLogFileStream(eventType)));

                    //写入消息
                    source.TraceEvent(eventType, (int)eventType, message);
                    source.Flush();
                    source.Close();
                }
                catch (SecurityException)
                {
                }
            }
        }

        /// <summary>
        /// 流的方式打开文件
        /// </summary>
        /// <returns>文件流</returns>
        private StreamWriter GetLogFileStream(TraceEventType eventType)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp", "log", this.GetFolderName(eventType), DateTime.Now.ToString("yyyyMMdd"));

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path = Path.Combine(path, DateTime.Now.ToString("yyyyMMddhhmmssFFF") + "." + (int)eventType + ".log");

            return File.AppendText(path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventType"></param>
        /// <returns></returns>
        private string GetFolderName(TraceEventType eventType)
        {
            string folder = string.Empty;

            switch (eventType)
            {
                case TraceEventType.Critical:
                    folder = "Critical";
                    break;
                case TraceEventType.Error:
                    folder = "Error";
                    break;
                case TraceEventType.Warning:
                    folder = "Warning";
                    break;
                case TraceEventType.Information:
                    folder = "Information";
                    break;
                case TraceEventType.Verbose:
                    folder = "Verbose";
                    break;
                default:
                    folder = "Error";
                    break;
            }

            return folder;
        }

        #endregion

        #region ILogger Members

        /// <summary>
        /// 日志信息。
        /// </summary>
        /// <param name="message">日志信息。</param>
        /// <param name="args">信息参数。</param>
        public void LogInfo(string message, params object[] args)
        {
            if (!String.IsNullOrWhiteSpace(message))
            {
                var messageToTrace = string.Format(CultureInfo.InvariantCulture, message, args);

                TraceInternal(TraceEventType.Information, messageToTrace);
            }
        }

        /// <summary>
        /// 非关键性错误日志信息。
        /// </summary>
        /// <param name="message">非关键性错误日志信息。</param>
        /// <param name="args">信息参数。</param>
        public void LogWarning(string message, params object[] args)
        {

            if (!String.IsNullOrWhiteSpace(message))
            {
                var messageToTrace = string.Format(CultureInfo.InvariantCulture, message, args);

                TraceInternal(TraceEventType.Warning, messageToTrace);
            }
        }

        /// <summary>
        /// 可恢复错误日志信息。
        /// </summary>
        /// <param name="message">可恢复错误日志信息。</param>
        /// <param name="args">信息参数。</param>
        public void LogError(string message, params object[] args)
        {
            if (!String.IsNullOrWhiteSpace(message))
            {
                var messageToTrace = string.Format(CultureInfo.InvariantCulture, message, args);

                TraceInternal(TraceEventType.Error, messageToTrace);
            }
        }

        /// <summary>
        /// 可恢复错误日志信息。
        /// </summary>
        /// <param name="message">可恢复错误日志信息。</param>
        /// <param name="exception">异常对象。</param>
        /// <param name="args">信息参数。</param>
        public void LogError(string message, Exception exception, params object[] args)
        {
            if (!String.IsNullOrWhiteSpace(message)
                &&
                exception != null)
            {
                var messageToTrace = string.Format(CultureInfo.InvariantCulture, message, args);

                // The ToString() create a string representation of the current exception
                var exceptionData = exception.ToString();

                TraceInternal(TraceEventType.Error, string.Format(CultureInfo.InvariantCulture, "{0} Exception:{1}", messageToTrace, exceptionData));
            }
        }

        /// <summary>
        /// 调试日志信息。
        /// </summary>
        /// <param name="message">调试日志信息。</param>
        /// <param name="args">信息参数。</param>
        public void Debug(string message, params object[] args)
        {
            if (!String.IsNullOrWhiteSpace(message))
            {
                var messageToTrace = string.Format(CultureInfo.InvariantCulture, message, args);

                TraceInternal(TraceEventType.Verbose, messageToTrace);
            }
        }

        /// <summary>
        /// 调试日志信息。
        /// </summary>
        /// <param name="message">调试日志信息。</param>
        /// <param name="exception">异常对象。</param>
        /// <param name="args">信息参数。</param>
        public void Debug(string message, Exception exception, params object[] args)
        {
            if (!String.IsNullOrWhiteSpace(message)
                &&
                exception != null)
            {
                var messageToTrace = string.Format(CultureInfo.InvariantCulture, message, args);

                // The ToString() create a string representation of the current exception
                var exceptionData = exception.ToString();

                TraceInternal(TraceEventType.Error, string.Format(CultureInfo.InvariantCulture, "{0} Exception:{1}", messageToTrace, exceptionData));
            }
        }

        /// <summary>
        /// 调试日志信息。
        /// </summary>
        /// <param name="item">跟踪对象。</param>
        public void Debug(object item)
        {
            if (item != null)
            {
                TraceInternal(TraceEventType.Verbose, item.ToString());
            }
        }

        /// <summary>
        /// 致命错误信息。
        /// </summary>
        /// <param name="message">致命错误信息。</param>
        /// <param name="args">信息参数。</param>
        public void Fatal(string message, params object[] args)
        {
            if (!String.IsNullOrWhiteSpace(message))
            {
                var messageToTrace = string.Format(CultureInfo.InvariantCulture, message, args);

                TraceInternal(TraceEventType.Critical, messageToTrace);
            }
        }

        /// <summary>
        /// 致命错误信息。
        /// </summary>
        /// <param name="message">致命错误信息。</param>
        /// <param name="exception">异常对象。</param>
        /// <param name="args">信息参数。</param>
        public void Fatal(string message, Exception exception, params object[] args)
        {
            if (!String.IsNullOrWhiteSpace(message)
                &&
                exception != null)
            {
                var messageToTrace = string.Format(CultureInfo.InvariantCulture, message, args);

                // The ToString() create a string representation of the current exception
                var exceptionData = exception.ToString();

                TraceInternal(TraceEventType.Critical, string.Format(CultureInfo.InvariantCulture, "{0} Exception:{1}", messageToTrace, exceptionData));
            }
        }

        #endregion
    }
}
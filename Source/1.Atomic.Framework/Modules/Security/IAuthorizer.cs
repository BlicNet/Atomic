﻿namespace Atomic.Modules.Security
{
    /// <summary>
    /// 授权者。
    /// </summary>
    public interface IAuthorizer
    {
        /// <summary>
        /// 授权。
        /// </summary>
        /// <param name="permission">权限信息。</param>
        bool Authorize(Permission permission);

        /// <summary>
        /// 授权验证。
        /// </summary>
        /// <remarks>
        /// 1、获取当前用户的信息。
        /// 1.1、如果是已登录的则获取当前登录用户的信息。
        /// 1.2、如果是未登录的则获取匿名用户的信息。
        /// 2、获取当前用户的所有角色的权限信息并且合并成一个集合。
        /// 3、判断该权限集合里是否存在指定的权限。
        /// </remarks>
        /// <param name="permissionName">权限编号。</param>
        /// <returns>已授权返回 true，未授权返回 false。</returns>
        bool Authorize(string permissionName);
    }
}
﻿namespace Atomic.Modules.Security
{
    /// <summary>
    /// 会员服务接口。
    /// </summary>
    public interface IMembershipService
    {
        /// <summary>
        /// 获得匿名用户信息。
        /// </summary>
        /// <returns>匿名用户信息。</returns>
        AnonymousUser GetAnonymousUser();

        /// <summary>
        /// 根据账号获得用户信息。
        /// </summary>
        /// <param name="account">账号。</param>
        /// <returns>用户信息。</returns>
        User GetUserByAccountNumber(string account);

        /// <summary>
        /// 根据用户编号获得用户信息。
        /// </summary>
        /// <param name="id">用户编号。</param>
        /// <returns>用户信息。</returns>
        IUser GetUser(long id);
    }
}
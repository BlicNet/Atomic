﻿namespace Atomic.Modules.Security
{
    using Atomic.Domain;

    /// <summary>
    /// 权限信息。
    /// </summary>
    public class Permission : EntityBase
    {
        /// <summary>
        /// 初始化权限信息。
        /// </summary>
        public Permission()
        {
        }

        /// <summary>
        /// 初始化权限信息。
        /// </summary>
        /// <param name="name">权限名称。</param>
        /// <param name="displayName">权限显示名称。</param>
        /// <param name="description">权限描述。</param>
        public Permission(string name, string displayName, string description)
        {
            this.Name = name;
            this.DisplayName = displayName;
            this.Description = description;
        }

        /// <summary>
        /// 权限名称。
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 权限显示名称。
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 权限描述。
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 是否空对象。
        /// </summary>
        /// <returns></returns>
        public virtual bool IsNull()
        {
            return false;
        }
    }
}
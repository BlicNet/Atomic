﻿namespace Atomic.Modules.Security
{
    using System.Text;
    using System.Text.RegularExpressions;

    using Atomic.Extensions;

    /// <summary>
    /// 用户验证。
    /// </summary>
    public class UserValidator
    {
        /// <summary>
        /// 用户验证对象。
        /// </summary>
        private static UserValidator _current = new UserValidator();

        /// <summary>
        /// 用户验证对象。
        /// </summary>
        public static UserValidator Current
        {
            get
            {
                return _current;
            }
        }

        /// <summary>
        /// 判断当前字符串是否在指定的范围内。
        /// </summary>
        /// <param name="target">当前字符串。</param>
        /// <param name="minLength">最小长度。</param>
        /// <param name="maxLength">最大长度。</param>
        /// <returns>在指定的范围内，则为 true。</returns>
        public bool Range(string target, int minLength, int maxLength)
        {
            return target.Range(minLength, maxLength);
        }

        /// <summary>
        /// 验证输入字符串是否是文字。（非特殊字符）
        /// </summary>
        /// <param name="target">目标字符串。</param>
        /// <returns>不存在特殊字符，则返回 true，否则返回 false。</returns>
        public bool IsChar(string target)
        {
            return target.IsMatch(@"^(\w)*$");
        }

        /// <summary>
        /// 是否电子邮箱。
        /// </summary>
        /// <param name="target">当前字符串。</param>
        /// <returns>是否电子邮箱。</returns>
        public bool IsEmail(string target)
        {
            return target.IsEmail();
        }

        /// <summary>
        /// 账号字符串格式验证。
        /// </summary>
        /// <remarks>
        /// 只能是数字、字母和下划线的组合。
        /// </remarks>
        /// <param name="target">目标字符串</param>
        /// <returns>是否通过。</returns>
        public bool AccountString(string target)
        {
            return target.IsMatch(@"^(\d|[_a-zA-Z0-9])*$");
        }

        /// <summary>
        /// 将当前字符串转换成 32 位 MD5 加密。
        /// </summary>
        /// <param name="target">目标字符串。</param>
        /// <returns>32 位 MD5 加密后的字符串。</returns>
        public string MD5(string target)
        {
            return target.MD5();
        }
    }
}
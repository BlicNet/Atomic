﻿namespace Atomic.Modules.Security
{
    using System.Linq;
    using System.Collections.Generic;

    using Atomic.Domain;

    /// <summary>
    /// 匿名用户。
    /// </summary>
    public class AnonymousUser : EntityBase, IUser
    {
        /// <summary>
        /// 初始化匿名用户。
        /// </summary>
        public AnonymousUser(params Role[] roles)
        {
            this.Name = "游客";
            this.Roles = roles != null ? roles.ToList() : new List<Role>();
        }

        /// <summary>
        /// 账号。
        /// </summary>
        public Account Account { get; private set; }

        /// <summary>
        /// 用户名称。
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// 邮箱。
        /// </summary>
        public string Email { get; private set; }

        /// <summary>
        /// 是否可用。
        /// </summary>
        public bool Disable { get { return true; } }

        /// <summary>
        /// 身份列表。
        /// </summary>   
        public IEnumerable<Role> Roles { get; private set; }
    }
}
﻿namespace Atomic.Modules.Security
{
    using System.Security.Principal;
    using System.Web.Security;

    /// <summary>
    /// 用户扩展服务。
    /// </summary>
    public static class UserExtensions
    {
        /// <summary>
        /// 获得当前已登录的用户的用户编号。
        /// </summary>
        /// <param name="user">当前用户。</param>
        /// <returns>账号。</returns>
        public static int GetCurrentId(this IPrincipal user)
        {
            var userData = GetUserData(user);

            if (userData != null)
            {
                return int.Parse(userData[0]);
            }

            return 0;
        }

        /// <summary>
        /// 获得当前已登录的用户的账号。
        /// </summary>
        /// <param name="user">当前用户。</param>
        /// <returns>账号。</returns>
        public static string GetCurrentAccountNumber(this IPrincipal user)
        {
            var userData = GetUserData(user);

            if (userData != null)
            {
                return userData[1];
            }

            return string.Empty;
        }

        /// <summary>
        /// 获得用户数据。
        /// </summary>
        /// <remarks>
        /// [0]用户编号。
        /// [1]用户账号。
        /// </remarks>
        /// <param name="user">当前用户。</param>
        /// <returns>用户数据。</returns>
        private static string[] GetUserData(IPrincipal user)
        {
            if (user.Identity is FormsIdentity)
            {
                string userData = ((FormsIdentity)user.Identity).Ticket.UserData;

                if (!string.IsNullOrWhiteSpace(userData))
                {
                    var userDataArray = userData.Split('|');

                    if (userDataArray != null && userDataArray.Length == 2)
                    {
                        return userDataArray;
                    }
                }
            }

            return null;
        }
    }
}
﻿namespace Atomic.Modules.Security
{
    /// <summary>
    /// 用户工厂
    /// </summary>
    public class UserFactory
    {
        /// <summary>
        /// 创建用户
        /// </summary>
        /// <param name="accountNumber">账号</param>
        /// <param name="password">密码</param>
        /// <param name="name">用户名称</param>
        /// <param name="email">电子邮箱</param>
        /// <returns>用户信息实体</returns>
        public static User Create(string accountNumber, string password, string name, string email)
        {
            var user = new User();

            user.Account = new Account(accountNumber: accountNumber, password: password);
            user.Name = name;
            user.Email = email;

            return user;
        }
    }
}
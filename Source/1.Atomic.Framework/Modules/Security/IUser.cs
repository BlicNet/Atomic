﻿namespace Atomic.Modules.Security
{
    using System.Collections.Generic;

    using Atomic.Domain;

    /// <summary>
    /// 用户信息接口。
    /// </summary>
    public interface IUser : IEntity
    {
        /// <summary>
        /// 账号。
        /// </summary>
        Account Account { get; }

        /// <summary>
        /// 用户名称。
        /// </summary>
        string Name { get; }

        /// <summary>
        /// 邮箱。
        /// </summary>
        string Email { get; }

        /// <summary>
        /// 是否可用。
        /// </summary>
        bool Disable { get; }

        /// <summary>
        /// 身份列表。
        /// </summary>   
        IEnumerable<Role> Roles { get; }
    }
}
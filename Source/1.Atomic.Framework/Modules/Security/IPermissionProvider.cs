﻿namespace Atomic.Modules.Security
{
    using System.Collections.Generic;

    /// <summary>
    /// 权限提供程序。
    /// </summary>
    public interface IPermissionProvider
    {
        /// <summary>
        /// 获得权限集合。
        /// </summary>
        /// <returns>权限集合。</returns>
        IEnumerable<Permission> GetPermissions();

        /// <summary>
        /// 根据角色编号获得角色信息。
        /// </summary>
        /// <param name="roleId">角色编号。</param>
        /// <returns>角色信息。</returns>
        Role GetRole(int roleId);

        /// <summary>
        /// 获得所有角色。
        /// </summary>
        /// <returns>所有角色信息集合。</returns>
        IEnumerable<Role> GetRoles();
    }
}
﻿namespace Atomic.Modules.Security
{
    /// <summary>
    /// 系统角色。
    /// </summary>
    public enum SystemRoles
    {
        /// <summary>
        /// 管理员。
        /// </summary>
        Admin = 1,

        /// <summary>
        /// 匿名用户。
        /// </summary>
        Anonymous = 2,

        /// <summary>
        /// 默认普通注册用户角色。
        /// </summary>
        Normal = 3,

        /// <summary>
        /// 禁封用户。
        /// </summary>
        Disable = 4
    }
}
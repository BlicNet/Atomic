﻿namespace Atomic.Modules.Security
{
    using System.Collections.Generic;

    /// <summary>
    /// 系统权限。
    /// </summary>
    public class SystemPermissions
    {
        /// <summary>
        /// 是否允许进入管理后台。
        /// </summary>
        public const string IsLoginAdmin = "IsLoginAdmin";
    }
}
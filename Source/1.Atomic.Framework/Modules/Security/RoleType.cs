﻿namespace Atomic.Modules.Security
{
    /// <summary>
    /// 角色类型。
    /// </summary>
    public enum RoleType
    {
        /// <summary>
        /// 普通。
        /// </summary>
        Normal = 0,

        /// <summary>
        /// 管理员。
        /// </summary>
        Admin = 1
    }
}
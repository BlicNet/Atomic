﻿namespace Atomic.Modules.Security
{
    /// <summary>
    /// 空权限对象。
    /// </summary>
    public class NullPermission : Permission
    {
        /// <summary>
        /// 是否空对象。
        /// </summary>
        /// <returns></returns>
        public override bool IsNull()
        {
            return true;
        }
    }
}
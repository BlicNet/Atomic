﻿namespace Atomic.Modules.Security
{
    /// <summary>
    /// 认证服务接口。
    /// </summary>
    public interface IAuthenticationService
    {
        /// <summary>
        /// 登录。
        /// </summary>
        /// <param name="user">已验证成功的用户信息。</param>
        /// <param name="createPersistentCookie"></param>
        void SignIn(IUser user, bool createPersistentCookie);

        /// <summary>
        /// 登出。
        /// </summary>
        void SignOut();

        /// <summary>
        /// 获得已认证的用户。
        /// </summary>
        /// <remarks>
        /// 该方法获得的是已登录的用户，如果是未登录的，则返回 AnonymousUser 类的实例。
        /// </remarks>
        /// <returns>用户实体。</returns>
        IUser GetAuthenticatedUser();
    }
}
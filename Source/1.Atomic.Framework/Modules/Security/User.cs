﻿namespace Atomic.Modules.Security
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Web.Security;
    using System.Security.Principal;

    using Atomic.Domain;

    /// <summary>
    /// 用户信息
    /// </summary>
    public class User : EntityBase, IUser
    {
        /// <summary>
        /// 用户名称。
        /// </summary>
        private string _name = string.Empty;

        /// <summary>
        /// 邮箱。
        /// </summary>
        private string _email = string.Empty;

        /// <summary>
        /// 账号。
        /// </summary>
        public Account Account { get; set; }

        /// <summary>
        /// 用户名称。
        /// </summary>
        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                string name = value;

                if (string.IsNullOrWhiteSpace(name))
                {
                    throw new ArgumentNullException("name", "用户名称不可以是空的。");
                }

                if (!UserValidator.Current.IsChar(name))
                {
                    throw new ArgumentException("name", "用户名称只可以是字符。");
                }

                this._name = name;
            }
        }

        /// <summary>
        /// 邮箱。
        /// </summary>
        public string Email
        {
            get
            {
                return this._email;
            }
            set
            {
                string email = value;

                if (string.IsNullOrWhiteSpace(email))
                {
                    throw new ArgumentNullException("email", "电子邮箱不可以是空的。");
                }

                if (!UserValidator.Current.IsEmail(email))
                {
                    throw new ArgumentException("email", "电子邮箱的格式不正确。");
                }

                this._email = email;
            }
        }

        /// <summary>
        /// 是否可用。
        /// </summary>
        public bool Disable { get; set; }

        /// <summary>
        /// 身份列表。
        /// </summary>   
        public IEnumerable<Role> Roles { get; private set; }

        /// <summary>
        /// 设置用户身份。
        /// </summary>
        /// <param name="roles">身份列表。</param>
        public void SetRoles(IEnumerable<Role> roles)
        {
            this.Roles = roles ?? Enumerable.Empty<Role>();
        }
    }
}
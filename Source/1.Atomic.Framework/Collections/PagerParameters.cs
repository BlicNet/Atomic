﻿namespace Atomic.Collections
{
    /// <summary>
    /// 分页参数。
    /// </summary>
    public class PagerParameters
    {
        /// <summary>
        /// 构造器。
        /// </summary>
        public PagerParameters()
            : this(1, 10)
        {
        }

        /// <summary>
        /// 初始化分页参数。
        /// </summary>
        /// <param name="index">页码。</param>
        /// <param name="size">页大小。</param>
        public PagerParameters(int index, int size)
        {
            this.Index = index;
            this.Size = size;

            if (this.Index <= 0)
            {
                this.Index = 1;
            }

            if (this.Size <= 0)
            {
                this.Size = 10;
            }
        }

        /// <summary>
        /// 页码
        /// </summary>
        public int Index { get; private set; }

        /// <summary>
        /// 页大小
        /// </summary>
        public int Size { get; private set; }
    }
}
﻿namespace Atomic.Collections
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 分页集合
    /// </summary>
    /// <typeparam name="T">对象类型。</typeparam>
    public class PageOfItems<T> : List<T>, IPageOfItems<T>
    {
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="items">列表</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="totalItemCount">总记录数</param>
        public PageOfItems(IEnumerable<T> items, int pageIndex, int pageSize, int totalItemCount)
        {
            AddRange(items);

            //页码。
            PageIndex = pageIndex;

            //页大小。
            PageSize = pageSize;

            //总记录数。
            TotalItemCount = totalItemCount;

            //计算总页数。
            TotalPageCount = (int)Math.Ceiling(totalItemCount / (double)pageSize);

            //当前页数小于 1 时，设置当前页数是 1。
            if (this.PageIndex < 1)
            {
                this.PageIndex = 1;
            }

            //页大小小于 1 时，设置页大小是 10。
            if(this.PageSize < 1)
            {
                this.PageSize = 10;
            }

            //当前页数大于最大页数时，设置当前页数等于最大页数。
            if (this.PageIndex > this.TotalPageCount)
            {
                this.PageIndex = this.TotalPageCount;
            }
        }

        #region IPageOfItems<T> Members

        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; private set; }

        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize { get; private set; }

        /// <summary>
        /// 总记录数
        /// </summary>
        public int TotalItemCount { get; private set; }

        /// <summary>
        /// 总页数
        /// </summary>
        public int TotalPageCount { get; private set; }

        #endregion
    }
}
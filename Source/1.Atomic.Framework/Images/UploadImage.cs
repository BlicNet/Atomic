﻿namespace Atomic.Images
{
    using System.IO;

    /// <summary>
    /// 上传图片信息。
    /// </summary>
    public class UploadImage
    {
        /// <summary>
        /// 文件流。
        /// </summary>
        public Stream Stream { get; set; }

        /// <summary>
        /// 原图片名称。
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 保存目录。
        /// </summary>
        public string SavePath { get; set; }

        /// <summary>
        /// 最大宽度。
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// 最大高度。
        /// </summary>
        public int Height { get; set; }
    }
}
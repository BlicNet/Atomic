﻿namespace Atomic.Images
{
    /// <summary>
    /// 输出显示上传图片信息。
    /// </summary>
    public class ViewUploadImage
    {
        /// <summary>
        /// 文件名。
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 扩展名。
        /// </summary>
        public string FileExt { get; set; }

        /// <summary>
        /// 原图片宽度。
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// 原图片高度。
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// 预览图片宽度。
        /// </summary>
        public int ViewWidth { get; set; }

        /// <summary>
        /// 预览图片高度。
        /// </summary>
        public int ViewHeight { get; set; }
    }
}
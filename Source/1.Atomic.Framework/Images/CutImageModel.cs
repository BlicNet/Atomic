﻿namespace Atomic.Images
{
    /// <summary>
    /// 裁剪图片模型信息。
    /// </summary>
    public class CutImageModel
    {
        /// <summary>
        /// 裁剪图片在原图里的左上角开始的 X 坐标。
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// 裁剪图片在原图里的左上角开始的 Y 坐标。
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// 裁剪图片宽度。
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// 裁剪图片高度。
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// 加载区域的宽度。
        /// </summary>
        /// <remarks>
        /// 加载区域表示的是显示原图片的区域。
        /// 由于窗口中不可能显示完整某些图片，
        /// 所以大于这个区域的图片会先缩小到
        /// 不超出该区域的大小之后才显示出来。
        /// </remarks>
        public int AreaWidth { get; set; }

        /// <summary>
        /// 加载区域的高度。
        /// </summary>
        public int AreaHeight { get; set; }
    }
}
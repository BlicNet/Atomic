﻿namespace Atomic.Caching
{
    using System;

    /// <summary>
    /// 缓存接口。
    /// </summary>
    /// <typeparam name="TKey">Key 类型。</typeparam>
    /// <typeparam name="TValue">Value 类型。</typeparam>
    public interface ICache<TKey, TValue>
    {
        /// <summary>
        /// 获取缓存值。
        /// </summary>
        /// <param name="key">键对象</param>
        /// <returns>值对象</returns>
        TValue Get(TKey key);

        /// <summary>
        /// 获取缓存值，如果不存在，则添加缓存。
        /// </summary>
        /// <param name="key">键对象</param>
        /// <param name="acquire">获得值对象的委托。</param>
        /// <returns>值对象。</returns>
        TValue GetOrAdd(TKey key, Func<TValue> acquire);

        /// <summary>
        /// 增加缓存值。
        /// </summary>
        /// <param name="key">键对象</param>
        /// <param name="value">值对象</param>
        void Set(TKey key, TValue value);

        /// <summary>
        /// 增加缓存值。
        /// </summary>
        /// <param name="key">键对象</param>
        /// <param name="value">值对象</param>
        /// <param name="expiredTime">过期时间(UTC)。</param>
        void Set(TKey key, TValue value, DateTime expiredTime);

        /// <summary>
        /// 移除缓存值。
        /// </summary>
        /// <param name="key">键对象</param>
        /// <param name="result">值对象</param>
        void Remove(TKey key, out TValue result);

        /// <summary>
        /// 清空缓存。
        /// </summary>
        void Clear();
    }
}
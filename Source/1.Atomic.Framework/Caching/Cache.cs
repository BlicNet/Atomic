﻿namespace Atomic.Caching
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    /// <summary>
    /// 缓存。
    /// </summary>
    /// <typeparam name="TKey">Key 类型。</typeparam>
    /// <typeparam name="TValue">Value 类型。</typeparam>
    public class Cache<TKey, TValue> : ICache<TKey, TValue>
    {
        /// <summary>
        /// 静态字典缓存数据。
        /// </summary>
        private readonly static IDictionary<TKey, CacheEntity> _cache = new Dictionary<TKey, CacheEntity>();

        /// <summary>
        /// 锁定对象。
        /// </summary>
        private static object lockobject = new object();

        /// <summary>
        /// 获取缓存值。
        /// </summary>
        /// <param name="key">键对象</param>
        /// <returns>值对象</returns>
        public TValue Get(TKey key)
        {
            if (_cache.ContainsKey(key))
            {
                var cache = _cache[key];

                if (cache.ExpiredTime > DateTime.UtcNow)
                {
                    return cache.Value;
                }
            }

            return default(TValue);
        }

        /// <summary>
        /// 获取缓存值，如果不存在，则添加缓存。
        /// </summary>
        /// <param name="key">键对象</param>
        /// <param name="acquire">获得值对象的委托。</param>
        /// <returns>值对象。</returns>
        public TValue GetOrAdd(TKey key, Func<TValue> acquire)
        {
            var result = this.Get(key);

            if (result == null)
            {
                result = acquire();

                this.Set(key, result);
            }

            return result;
        }

        /// <summary>
        /// 增加缓存值。
        /// </summary>
        /// <param name="key">键对象</param>
        /// <param name="value">值对象</param>
        public void Set(TKey key, TValue value)
        {
            this.Set(key, value, DateTime.UtcNow.AddMinutes(20));
        }

        /// <summary>
        /// 增加缓存值。
        /// </summary>
        /// <param name="key">键对象</param>
        /// <param name="value">值对象</param>
        /// <param name="expiredTime">过期时间(UTC)。</param>
        public void Set(TKey key, TValue value, DateTime expiredTime)
        {
            if (value != null)
            {
                CacheEntity cache = new CacheEntity
                {
                    Value = value,
                    ExpiredTime = expiredTime
                };

                lock (lockobject)
                {
                    _cache[key] = cache;
                }
            }
        }

        /// <summary>
        /// 移除缓存值。
        /// </summary>
        /// <param name="key">键对象</param>
        /// <param name="result">值对象</param>
        public void Remove(TKey key, out TValue result)
        {
            result = default(TValue);

            if (_cache.ContainsKey(key))
            {
                result = (TValue)_cache[key].Value;

                _cache.Remove(key);
            }
        }

        /// <summary>
        /// 清空缓存。
        /// </summary>
        public void Clear()
        {
            _cache.Clear();
        }

        /// <summary>
        /// 缓存实体。
        /// </summary>
        internal class CacheEntity
        {
            /// <summary>
            /// 缓存值。
            /// </summary>
            public TValue Value { get; set; }

            /// <summary>
            /// 过期时间（UTC）
            /// </summary>
            public DateTime ExpiredTime { get; set; }
        }
    }
}
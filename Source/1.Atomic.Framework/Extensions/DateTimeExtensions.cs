﻿namespace Atomic.Extensions
{
    using System;

    /// <summary>
    /// 时间类型扩展。
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// 获得默认时间。
        /// </summary>
        /// <param name="dateTime">当前时间对象。</param>
        public static DateTime GetDefaultTime(this DateTime dateTime)
        {
            return new DateTime(1970, 1, 1);
        }
    }
}
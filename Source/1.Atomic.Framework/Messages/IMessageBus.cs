﻿namespace Atomic.Messages
{
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// 消息总线接口。
    /// </summary>
    public interface IMessageBus
    {
        /// <summary>
        /// 发送事件通知。
        /// </summary>
        /// <remarks>interfaceName + "." + methodName, data。</remarks>
        /// <param name="messageName">消息名称（接口名.方法名）。</param>
        /// <param name="eventData">参数字典（Key：参数名,Value：参数值）。</param>
        /// <returns>操作结果集合。</returns>
        IEnumerable Notify(string messageName, IDictionary<string, object> eventData);
    }
}
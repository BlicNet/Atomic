﻿namespace Atomic.Messages
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 消息工厂。
    /// </summary>
    public class MessageFactory
    {
        #region Members

        /// <summary>
        /// 消息工厂。
        /// </summary>
        private static MessageFactory _instance = new MessageFactory();

        /// <summary>
        /// 消息总线。
        /// </summary>
        private IMessageBus _current = null;

        #endregion

        #region Properties

        /// <summary>
        /// 当前消息总线。
        /// </summary>
        public static IMessageBus Current
        {
            get
            {
                return _instance.InnerCurrent;
            }
        }

        /// <summary>
        /// 内部消息总线。
        /// </summary>
        private IMessageBus InnerCurrent
        {
            get
            {
                if(this._current == null)
                {
                    this._current = new DefaultMessageBus(GetMessageHandlers);
                }

                return _current;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 设置当前消息总线接口。
        /// </summary>
        /// <param name="messageBus">消息总线对象。</param>
        public static void SetMessageBus(IMessageBus messageBus)
        {
            _instance.InnerSetMessageBus(messageBus);
        }

        #endregion

        #region Pricate Method

        /// <summary>
        /// 内部设置消息总线。
        /// </summary>
        /// <param name="messageBus">消息总线对象。</param>
        private void InnerSetMessageBus(IMessageBus messageBus)
        {
            if (messageBus == null)
            {
                throw new ArgumentNullException("messageBus");
            }

            _current = messageBus;
        }

        /// <summary>
        /// 获得消息处理程序。
        /// </summary>
        /// <returns>消息处理程序集合。</returns>
        private static IEnumerable<IMessageHandler> GetMessageHandlers()
        {
            //TODO: 实现获得所有事件处理器的集合。
            return null;
            //return Atomic.Environment.AtomicContainer.Resolve<IEnumerable<IMessageHandler>>();
        }
        
        #endregion
    }
}
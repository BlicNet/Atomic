﻿namespace Atomic.Web
{
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// 路由配置。
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// 注册路由规则。
        /// </summary>
        /// <param name="routes">路由规则集合。</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            RouteTable.Routes.MapRoute(
                name: "Error.404",
                url: "error/404",
                defaults: new { controller = "Error", action = "Error404", id = UrlParameter.Optional }
            );
        }
    }
}
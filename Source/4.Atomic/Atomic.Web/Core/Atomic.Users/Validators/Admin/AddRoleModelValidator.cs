﻿namespace Atomic.Plugins.Users.Validators.Admin
{
    using Atomic.Mvc.Validators;
    using Atomic.Plugins.Users.Domain.Admin;

    /// <summary>
    /// 增加角色验证信息。
    /// </summary>
    public class AddRoleModelValidator : EntityValidatorBase<AddRoleModel>
    {
        /// <summary>
        /// 验证信息。
        /// </summary>
        /// <param name="model">增加角色验证信息。</param>
        public override void OnValidate(AddRoleModel model)
        {
            if (model == null || model.Id <= 0)
            {
                this.AddValidatorError("AddRole.Id", "Id", "角色编号不可以小于 0。");
            }
        }
    }
}
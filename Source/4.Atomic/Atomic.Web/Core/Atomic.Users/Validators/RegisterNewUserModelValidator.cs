﻿namespace Atomic.Plugins.Users.Validators
{
    using Atomic.Extensions;

    using Atomic.Mvc.Validators;

    using Atomic.Plugins.Users.Domain;
    using Atomic.Plugins.Users.Data;

    /// <summary>
    /// 注册新用户验证信息。
    /// </summary>
    public class RegisterNewUserModelValidator : EntityValidatorBase<RegisterNewUserModel>
    {
        /// <summary>
        /// 用户数据仓库。
        /// </summary>
        private readonly IUserRepository _userRepository = null;

        /// <summary>
        /// 初始化注册新用户验证信息。
        /// </summary>
        /// <param name="userRepository">用户数据仓库。</param>
        public RegisterNewUserModelValidator(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        /// <summary>
        /// 验证信息。
        /// </summary>
        /// <param name="model">注册新用户信息。</param>
        public override void OnValidate(RegisterNewUserModel model)
        {
            if (model == null)
            {
                this.AddValidatorError("user.null", null, "用户信息不可以是空的。");
            }

            if (string.IsNullOrWhiteSpace(model.AccountNumber) || !model.AccountNumber.Range(4, 20))
            {
                this.AddValidatorError("user.accountNumber.length", "AccountNumber", "账号的字符数必须在 4 - 20 个之间。");
            }

            if (!model.AccountNumber.IsMatch(@"^(\d|[_a-zA-Z0-9])*$"))
            {
                this.AddValidatorError("user.accountNumber.string", "AccountNumber", "账号必须是数字、字母和下划线的组合。");
            }

            if (string.IsNullOrWhiteSpace(model.Password) || !model.Password.Range(6, 20))
            {
                this.AddValidatorError("user.password.length", "Password", "密码的字符数必须在 6 - 20 个之间。");
            }

            if (!model.Password.IsMatch(@"^(\d|[_a-zA-Z0-9])*$"))
            {
                this.AddValidatorError("user.password.string", "Password", "密码必须是数字、字母和下划线的组合。");
            }

            if (model.Password != model.ConfirmPassword)
            {
                this.AddValidatorError("user.password.confirm", "ConfirmPassword", "两次输入的密码不一致。");
            }

            if (string.IsNullOrWhiteSpace(model.Name))
            {
                this.AddValidatorError("user.name.empty", "Name", "用户名称不可以是空的。");
            }

            if (!model.Name.IsMatch(@"^(\w)*$"))
            {
                this.AddValidatorError("user.name.string", "Name", "用户名称只可以是字符。");
            }

            if (string.IsNullOrWhiteSpace(model.Email))
            {
                this.AddValidatorError("user.email.empty", "Email", "电子邮箱不可以是空的。");
            }

            if (!model.Email.IsEmail())
            {
                this.AddValidatorError("user.email.invalid", "Email", "电子邮箱的格式不正确。");
            }

            if(this._userRepository.IsExistAccount(model.AccountNumber))
            {
                this.AddValidatorError("user.accountNumber.exist", "AccountNumber", "该账号已存在。");
            }

            if(this._userRepository.IsUseEmail(model.Email))
            {
                this.AddValidatorError("user.email.use", "Email", "该电子邮箱被已使用。");
            }
        }
    }
}
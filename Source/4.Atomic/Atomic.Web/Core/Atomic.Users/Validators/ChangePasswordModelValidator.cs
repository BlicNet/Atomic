﻿namespace Atomic.Plugins.Users.Validators
{
    using Atomic.Extensions;
    using Atomic.Mvc.Validators;
    using Atomic.Core.Security;
    using Atomic.Plugins.Users.Domain;

    /// <summary>
    /// 修改密码验证信息。
    /// </summary>
    public class ChangePasswordModelValidator : EntityValidatorBase<ChangePasswordModel>
    {
        /// <summary>
        /// 验证信息。
        /// </summary>
        /// <param name="model">修改密码验证信息。</param>
        public override void OnValidate(ChangePasswordModel model)
        {
            if (model == null)
            {
                this.AddValidatorError("ChangePassword.null", null, "修改密码信息不可以是空的。");
            }

            if (string.IsNullOrWhiteSpace(model.OldPassword) || !model.OldPassword.Range(6, 20))
            {
                this.AddValidatorError("ChangePassword.OldPassword.length", "OldPassword", "旧密码的字符数必须在 6 - 20 个之间。");
            }

            if (!model.OldPassword.IsMatch(@"^(\d|[_a-zA-Z0-9])*$"))
            {
                this.AddValidatorError("ChangePassword.OldPassword.string", "OldPassword", "旧密码必须是数字、字母和下划线的组合。");
            }

            if (string.IsNullOrWhiteSpace(model.NewPassword) || !model.NewPassword.Range(6, 20))
            {
                this.AddValidatorError("ChangePassword.NewPassword.length", "NewPassword", "新密码的字符数必须在 6 - 20 个之间。");
            }

            if (!model.NewPassword.IsMatch(@"^(\d|[_a-zA-Z0-9])*$"))
            {
                this.AddValidatorError("ChangePassword.NewPassword.string", "NewPassword", "新密码必须是数字、字母和下划线的组合。");
            }

            if (string.IsNullOrWhiteSpace(model.ConfirmPassword) || !model.ConfirmPassword.Range(6, 20))
            {
                this.AddValidatorError("ChangePassword.ConfirmPassword.length", "ConfirmPassword", "确认密码的字符数必须在 6 - 20 个之间。");
            }

            if (!model.ConfirmPassword.IsMatch(@"^(\d|[_a-zA-Z0-9])*$"))
            {
                this.AddValidatorError("ChangePassword.ConfirmPassword.string", "ConfirmPassword", "确认密码必须是数字、字母和下划线的组合。");
            }

            if (model.NewPassword != model.ConfirmPassword)
            {
                this.AddValidatorError("ChangePassword.NewPassword.confirm", "ConfirmPassword", "两次输入的密码不一致。");
            }
        }
    }
}
﻿namespace Atomic.Plugins.Users
{
    using System.Collections.Generic;

    using Atomic.Core.Security;

    /// <summary>
    /// 用户权限信息。
    /// </summary>
    public class Permissions
    {
        #region 用户权限

        /// <summary>
        /// 是否放开注册新用户。
        /// </summary>
        public const string User_Register = "User_Register";

        /// <summary>
        /// 是否允许修改密码。
        /// </summary>
        public const string User_ChangePassword = "User_ChangePassword";

        /// <summary>
        /// 用户修改自己的个人信息。
        /// </summary>
        public const string User_Settings = "User_Settings";

        #endregion

        #region 管理权限

        /// <summary>
        /// 是否允许查看角色列表。
        /// </summary>
        public const string Role_IsShowRoleList = "Role_IsShowRoleList";

        /// <summary>
        /// 是否允许增加角色。
        /// </summary>
        public const string Role_Add = "Role_Add";

        /// <summary>
        /// 是否允许编辑角色。
        /// </summary>
        public const string Role_Edit = "Role_Edit";

        /// <summary>
        /// 是否允许给角色授权。
        /// </summary>
        public const string Role_Authorize = "Role_Authorize";

        #endregion
    }
}
﻿namespace Atomic.Plugins.Users
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    using Atomic.Extensions;
    using Atomic.Mvc.Routes;

    /// <summary>
    /// 注册路由规则。
    /// </summary>
    public class Routes : IRouteProvider
    {
        /// <summary>
        /// 获取路由规则。
        /// </summary>
        /// <returns>路由规则描述集合。</returns>
        public IEnumerable<RouteDescriptor> GetRoutes()
        {
            List<RouteDescriptor> routes = new List<RouteDescriptor>();

            this.GetUserRoutes().ForEach(route => routes.Add(route));
            this.GetAdminRoutes().ForEach(route => routes.Add(route));

            return routes;
        }

        /// <summary>
        /// 获取用户路由规则。
        /// </summary>
        /// <returns>路由规则描述集合。</returns>
        private IEnumerable<RouteDescriptor> GetUserRoutes()
        {
            return new RouteDescriptor[]
            {
                new RouteDescriptor
                {
                    Name = "user.register",
                    Url = "user/register",
                    Defaults = new { controller = "User", action = "Register" },
                    Namespaces = new[] { "Atomic.Plugins.Users.Controllers" }
                },
                new RouteDescriptor
                {
                    Name = "user.password.change",
                    Url = "user/changepassword",
                    Defaults = new { controller = "User", action = "ChangePassword" },
                    Namespaces = new[] { "Atomic.Plugins.Users.Controllers" }
                },
                new RouteDescriptor
                {
                    Name = "user.settings.icon.load",
                    Url = "user/settings/icon/load",
                    Defaults = new { controller = "User", action = "Upload" },
                    Namespaces = new[] { "Atomic.Plugins.Users.Controllers" }
                },
                new RouteDescriptor
                {
                    Name = "user.settings.icon.change",
                    Url = "user/settings/icon",
                    Defaults = new { controller = "User", action = "Icon" },
                    Namespaces = new[] { "Atomic.Plugins.Users.Controllers" }
                },
                new RouteDescriptor
                {
                    Name = "user.index",
                    Url = "user/{accountNumber}",
                    Defaults = new { controller = "User", action = "Index", accountNumber = UrlParameter.Optional },
                    Namespaces = new[] { "Atomic.Plugins.Users.Controllers" }
                }
            };
        }

        /// <summary>
        /// 获取用户管理路由规则。
        /// </summary>
        /// <returns>路由规则描述集合。</returns>
        private IEnumerable<RouteDescriptor> GetAdminRoutes()
        {
            return new RouteDescriptor[]
            {
                new RouteDescriptor
                {
                    Name = "admin.user.role.authorize",
                    Url = "admin/user/role/authorize/{roleId}",
                    Defaults = new { controller = "AdminRole", action = "Authorize", roleId = UrlParameter.Optional },
                    Namespaces = new[] { "Atomic.Plugins.Users.Controllers.Admin" }
                },
                new RouteDescriptor
                {
                    Name = "admin.user.role.edit",
                    Url = "admin/user/role/edit/{roleId}",
                    Defaults = new { controller = "AdminRole", action = "Edit", roleId = UrlParameter.Optional },
                    Namespaces = new[] { "Atomic.Plugins.Users.Controllers.Admin" }
                },
                new RouteDescriptor
                {
                    Name = "admin.user.role.add",
                    Url = "admin/user/role/add",
                    Defaults = new { controller = "AdminRole", action = "Add" },
                    Namespaces = new[] { "Atomic.Plugins.Users.Controllers.Admin" }
                },
                new RouteDescriptor
                {
                    Name = "admin.user.role.list",
                    Url = "admin/user/role/list/{pageIndex}",
                    Defaults = new { controller = "AdminRole", action = "List", pageIndex = 1 },
                    Namespaces = new[] { "Atomic.Plugins.Users.Controllers.Admin" }
                },
                new RouteDescriptor
                {
                    Name = "admin.user.list",
                    Url = "admin/user/list/{pageIndex}",
                    Defaults = new { controller = "AdminUser", action = "List", pageIndex = 1 },
                    Namespaces = new[] { "Atomic.Plugins.Users.Controllers.Admin" }
                },
                new RouteDescriptor
                {
                    Name = "admin.user",
                    Url = "admin/user",
                    Defaults = new { controller = "AdminUser", action = "Index" },
                    Namespaces = new[] { "Atomic.Plugins.Users.Controllers.Admin" }
                }
            };
        }
    }
}
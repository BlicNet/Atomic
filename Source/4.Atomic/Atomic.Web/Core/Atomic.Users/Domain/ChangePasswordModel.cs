﻿namespace Atomic.Plugins.Users.Domain
{
    /// <summary>
    /// 修改密码信息。
    /// </summary>
    public class ChangePasswordModel
    {
        /// <summary>
        /// 用户账号。
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// 旧密码。
        /// </summary>
        public string OldPassword { get; set; }

        /// <summary>
        /// 新密码。
        /// </summary>
        public string NewPassword { get; set; }

        /// <summary>
        /// 确认密码。
        /// </summary>
        public string ConfirmPassword { get; set; }
    }
}
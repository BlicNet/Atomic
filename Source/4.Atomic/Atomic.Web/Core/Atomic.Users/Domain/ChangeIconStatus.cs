﻿namespace Atomic.Plugins.Users.Domain
{
    /// <summary>
    /// 修改头像操作状态。
    /// </summary>
    public enum ChangeIconStatus
    {
        /// <summary>
        /// 成功。
        /// </summary>
        Success = 1,

        /// <summary>
        /// 失败。
        /// </summary>
        Error = 0,

        /// <summary>
        /// 登录失效。
        /// </summary>
        LoginInvalid = -1,

        /// <summary>
        /// 未上传头像。
        /// </summary>
        NotUpload = -2,
    }
}
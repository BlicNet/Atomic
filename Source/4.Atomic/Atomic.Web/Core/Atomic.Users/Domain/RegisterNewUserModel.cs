﻿namespace Atomic.Plugins.Users.Domain
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// 注册新用户信息。
    /// </summary>
    public class RegisterNewUserModel
    {
        /// <summary>
        /// 账号
        /// </summary>
        [Display(Name = "账号")]
        [Required(ErrorMessage = "账号不可以是空。")]
        [MinLength(4, ErrorMessage = "账号不可以小于 4 个字符。")]
        [MaxLength(20, ErrorMessage = "账号不可以大于 20 个字符。")]
        public string AccountNumber { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Display(Name = "密码")]
        [Required(ErrorMessage = "密码不可以是空。")]
        [MinLength(4, ErrorMessage = "账号不可以小于 6 个字符。")]
        [MaxLength(20, ErrorMessage = "账号不可以大于 20 个字符。")]
        public string Password { get; set; }

        /// <summary>
        /// 确认密码
        /// </summary>
        [Display(Name = "确认密码")]
        [Required(ErrorMessage = "确认密码不可以是空。")]
        [MinLength(4, ErrorMessage = "确认密码不可以小于 6 个字符。")]
        [MaxLength(20, ErrorMessage = "确认密码不可以大于 20 个字符。")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        [Display(Name = "用户名称")]
        [Required(ErrorMessage = "用户名称不可以是空。")]
        public string Name { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [Display(Name = "邮箱")]
        [Required(ErrorMessage = "邮箱不可以是空。")]
        public string Email { get; set; }
    }
}
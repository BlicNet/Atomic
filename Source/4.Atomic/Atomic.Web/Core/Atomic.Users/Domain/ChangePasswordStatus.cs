﻿namespace Atomic.Plugins.Users.Domain
{
    /// <summary>
    /// 修改密码的操作状态。
    /// </summary>
    public enum ChangePasswordStatus
    {
        /// <summary>
        /// 成功。
        /// </summary>
        Success = 1,

        /// <summary>
        /// 修改密码失败。
        /// </summary>
        Error = 0,

        /// <summary>
        /// 旧密码错误。
        /// </summary>
        OldPasswordError = -1,

        /// <summary>
        /// 新密码与确认密码不一致。
        /// </summary>
        ConfirmPasswordError = -2,
    }
}
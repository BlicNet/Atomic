﻿namespace Atomic.Plugins.Users.Domain
{
    /// <summary>
    /// 注册新用户的操作状态。
    /// </summary>
    public enum RegisterNewUserStatus
    {
        /// <summary>
        /// 成功
        /// </summary>
        Success = 1,

        /// <summary>
        /// 失败。
        /// </summary>
        Error = 0,

        /// <summary>
        /// 账号字符数不正确。
        /// </summary>
        AccountLengthInvalid = -1,

        /// <summary>
        /// 账号无效
        /// </summary>
        AccountInvalid = -2,

        /// <summary>
        /// 账号已存在
        /// </summary>
        AccountExist = -3,

        /// <summary>
        /// 密码无效
        /// </summary>
        PasswordInvalid = -4,

        /// <summary>
        /// 电子邮箱无效
        /// </summary>
        EmailInvalid = -5,

        /// <summary>
        /// 用户名称已存在
        /// </summary>
        NameExist = -6,

        /// <summary>
        /// 显示名称已存在
        /// </summary>
        DisplayExist = -7,

        /// <summary>
        /// 电子邮箱已存在
        /// </summary>
        EmailExist = -8,

        /// <summary>
        /// 密码字符数不正确。
        /// </summary>
        PasswordLengthInvalid = -9,

        /// <summary>
        /// 用户名称无效
        /// </summary>
        NameInvalid = -10,

        /// <summary>
        /// 显示名称无效
        /// </summary>
        DisplayNameInvalid = -11
    }
}
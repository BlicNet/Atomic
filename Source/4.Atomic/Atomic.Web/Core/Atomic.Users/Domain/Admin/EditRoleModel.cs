﻿namespace Atomic.Plugins.Users.Domain.Admin
{
    using Atomic.Modules.Security;

    /// <summary>
    /// 编辑角色模型。
    /// </summary>
    public class EditRoleModel
    {
        /// <summary>
        /// 角色编号。
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 唯一名称。
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 显示名称。
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 角色类型。
        /// </summary>
        public RoleType Type { get; set; }
    }
}
﻿namespace Atomic.Plugins.Users.Domain.Admin
{
    /// <summary>
    /// 编辑角色的操作状态。
    /// </summary>
    public enum EditRoleStatus
    {
        /// <summary>
        /// 成功。
        /// </summary>
        Success = 1,

        /// <summary>
        /// 编辑失败。
        /// </summary>
        Error = 0,

        /// <summary>
        /// 唯一名称已存在。
        /// </summary>
        NameExist = -1,

        /// <summary>
        /// 角色不存在。
        /// </summary>
        RoleNotExist = -2,
    }
}
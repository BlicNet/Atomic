﻿namespace Atomic.Plugins.Users.Domain.Admin
{
    using System.ComponentModel.DataAnnotations;

    using Atomic.Modules.Security;

    /// <summary>
    /// 增加角色模型。
    /// </summary>
    public class AddRoleModel
    {
        /// <summary>
        /// 编号。
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 唯一名称。
        /// </summary>
        [Required(ErrorMessage = "名称不可以是空。")]
        [MaxLength(25, ErrorMessage = "名称不可以大于 25 个字符。")]
        public string Name { get; set; }

        /// <summary>
        /// 显示名称。
        /// </summary>
        [Required(ErrorMessage = "显示名称不可以是空。")]
        [MaxLength(25, ErrorMessage = "显示名称不可以大于 25 个字符。")]
        public string DisplayName { get; set; }

        /// <summary>
        /// 角色类型。
        /// </summary>
        public RoleType Type { get; set; }
    }
}
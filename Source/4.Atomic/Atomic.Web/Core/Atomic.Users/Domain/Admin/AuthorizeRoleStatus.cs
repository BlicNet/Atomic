﻿namespace Atomic.Plugins.Users.Domain.Admin
{
    /// <summary>
    /// 角色授权的操作状态。
    /// </summary>
    public enum AuthorizeRoleStatus
    {
        /// <summary>
        /// 成功。
        /// </summary>
        Success = 1,

        /// <summary>
        /// 编辑失败。
        /// </summary>
        Error = 0,
    }
}
﻿namespace Atomic.Plugins.Users.Domain.Admin
{
    using System.Collections.Generic;

    using Atomic.Modules.Security;

    /// <summary>
    /// 角色授权模型。
    /// </summary>
    public class AuthorizeRoleModel
    {
        /// <summary>
        /// 角色信息。
        /// </summary>
        public Role Role { get; set; }

        /// <summary>
        /// 权限集合。
        /// </summary>
        public IEnumerable<Permission> Permissions { get; set; }
    }
}
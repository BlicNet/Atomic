﻿namespace Atomic.Plugins.Users.Domain.Admin
{
    /// <summary>
    /// 增加角色的操作状态。
    /// </summary>
    public enum AddRoleStatus
    {
        /// <summary>
        /// 成功。
        /// </summary>
        Success = 1,

        /// <summary>
        /// 增加失败。
        /// </summary>
        Error = 0,

        /// <summary>
        /// 编号已存在。
        /// </summary>
        NumberExist = -1,

        /// <summary>
        /// 唯一名称已存在。
        /// </summary>
        NameExist = -2,
    }
}
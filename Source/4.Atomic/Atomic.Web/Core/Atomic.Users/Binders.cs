﻿namespace Atomic.Plugins.Users
{
    using System.Collections.Generic;

    using Atomic.Mvc.ModelBinders;
    using Atomic.Plugins.Users.ModelBinders;
    using Atomic.Plugins.Users.Domain.Admin;

    public class Binders : IModelBinderProvider
    {
        public IEnumerable<ModelBinderDescriptor> GetModelBinders()
        {
            return new ModelBinderDescriptor[]
            {
                new ModelBinderDescriptor
                {
                    Type = typeof(AuthorizeRoleModel),
                    ModelBinder = new AuthorizeRoleModelBinder()
                }
            };
        }
    }
}
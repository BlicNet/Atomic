﻿namespace Atomic.Plugins.Users.Exceptions
{
    using Atomic.Exceptions;

    /// <summary>
    /// 角色授权异常。
    /// </summary>
    public class AuthorizeRoleException : AtomicException
    {
        public AuthorizeRoleException(string errorMessage)
            : base(errorMessage)
        { }
    }
}
﻿namespace Atomic.Plugins.Users.Application
{
    using System;
    using System.Collections.Generic;
    using Atomic.Caching;
    using Atomic.Collections;
    using Atomic.Modules.Security;
    using Atomic.Plugins.Users.Data;
    using Atomic.Plugins.Users.Models;
    using Atomic.Plugins.Users.Domain.Admin;

    /// <summary>
    /// 角色管理服务。
    /// </summary>
    public class RoleAdminService : IRoleAdminService
    {
        /// <summary>
        /// 权限提供程序。
        /// </summary>
        private readonly IPermissionProvider _permissionProvider = null;

        /// <summary>
        /// 角色管理数据访问器。
        /// </summary>
        private readonly IRoleAdminRepository _roleAdminRepository = null;

        /// <summary>
        /// 初始化角色管理服务。
        /// </summary>
        /// <param name="permissionProvider">权限提供程序。</param>
        /// <param name="roleAdminRepository">角色管理数据访问器。</param>
        public RoleAdminService(IPermissionProvider permissionProvider, IRoleAdminRepository roleAdminRepository)
        {
            this._permissionProvider = permissionProvider;
            this._roleAdminRepository = roleAdminRepository;
        }

        /// <summary>
        /// 增加角色。
        /// </summary>
        /// <param name="addRoleModel">增加角色模型。</param>
        /// <returns>增加角色的操作状态。</returns>
        public AddRoleStatus AddRole(AddRoleModel addRoleModel)
        {
            if (addRoleModel == null || addRoleModel.Id <= 0)
            {
                throw new ArgumentNullException("addRoleModel 对象不可以是 null。");
            }

            if (string.IsNullOrWhiteSpace(addRoleModel.Name))
            {
                throw new ArgumentException("addRoleModel.Name 不可以是空的。");
            }

            if (string.IsNullOrWhiteSpace(addRoleModel.DisplayName))
            {
                throw new ArgumentException("addRoleModel.DisplayName 不可以是空的。");
            }

            if (this._roleAdminRepository.ExistRoleId(addRoleModel.Id))
            {
                return AddRoleStatus.NumberExist;
            }

            if (!this._roleAdminRepository.ExistRoleName(addRoleModel.Name))
            {
                var result = this._roleAdminRepository.AddRole(addRoleModel);

                if (result)
                {
                    CacheManager.Current.Clear();

                    return AddRoleStatus.Success;
                }

                return AddRoleStatus.Error;
            }
            else
            {
                return AddRoleStatus.NameExist;
            }
        }

        /// <summary>
        /// 编辑角色。
        /// </summary>
        /// <param name="roleId">角色编号。</param>
        /// <param name="editRoleModel">编辑角色模型。</param>
        /// <returns>编辑角色的操作状态。</returns>
        public EditRoleStatus EditRole(int roleId, EditRoleModel editRoleModel)
        {
            if (roleId <= 0)
            {
                throw new ArgumentException("roleId", "角色编号不合法。");
            }

            if (editRoleModel == null)
            {
                throw new ArgumentNullException("editRoleModel", "editRoleModel 对象不可以是 null。");
            }

            if (!this._roleAdminRepository.ExistRoleId(roleId))
            {
                return EditRoleStatus.RoleNotExist;
            }

            if (!this._roleAdminRepository.ExistRoleName(roleId, editRoleModel.Name))
            {
                var result = this._roleAdminRepository.EditRole(roleId, editRoleModel);

                if (result)
                {
                    CacheManager.Current.Clear();

                    return EditRoleStatus.Success;
                }

                return EditRoleStatus.Error;
            }
            else
            {
                return EditRoleStatus.NameExist;
            }
        }

        /// <summary>
        /// 角色授权。
        /// </summary>
        /// <param name="authorizeRoleModel">授权角色模型。</param>
        /// <returns>授权角色的操作状态。</returns>
        public AuthorizeRoleStatus RoleAuthorize(AuthorizeRoleModel authorizeRoleModel)
        {
            if (authorizeRoleModel == null)
            {
                throw new ArgumentNullException("authorizeRoleModel", "authorizeRoleModel 对象不可以是 null。");
            }

            if (authorizeRoleModel.Role == null || authorizeRoleModel.Role.Id <= 0)
            {
                throw new ArgumentException("authorizeRoleModel.Role 不可以是空的。");
            }

            var result = this._roleAdminRepository.RoleAuthorize(authorizeRoleModel.Role.Id, authorizeRoleModel.Role.Permissions);

            if (result)
            {
                CacheManager.Current.Clear();

                return AuthorizeRoleStatus.Success;
            }

            return AuthorizeRoleStatus.Error;
        }

        /// <summary>
        /// 获得权限集合。
        /// </summary>
        /// <returns>权限集合。</returns>
        public IEnumerable<Permission> GetPermissions()
        {
            return this._permissionProvider.GetPermissions();
        }

        /// <summary>
        /// 根据角色编号获得角色信息。
        /// </summary>
        /// <param name="roleId">角色编号。</param>
        /// <returns>角色信息。</returns>
        public Role GetRole(int roleId)
        {
            return this._permissionProvider.GetRole(roleId);
        }

        /// <summary>
        /// 获得角色信息分页列表。
        /// </summary>
        /// <param name="pagerParameters">分页参数。</param>
        /// <returns>角色信息分页列表。</returns>
        public IPageOfItems<ViewRole> GetRoles(PagerParameters pagerParameters)
        {
            return this._roleAdminRepository.GetRoles(pagerParameters.Index, pagerParameters.Size);
        }
    }
}
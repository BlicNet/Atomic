﻿namespace Atomic.Plugins.Users.Application
{
    using System.Collections.Generic;

    using Atomic.Collections;
    using Atomic.Modules.Security;
    using Atomic.Plugins.Users.Models;
    using Atomic.Plugins.Users.Domain.Admin;

    /// <summary>
    /// 角色管理服务。
    /// </summary>
    public interface IRoleAdminService
    {
        /// <summary>
        /// 增加角色。
        /// </summary>
        /// <param name="addRoleModel">增加角色模型。</param>
        /// <returns>增加角色的操作状态。</returns>
        AddRoleStatus AddRole(AddRoleModel addRoleModel);

        /// <summary>
        /// 编辑角色。
        /// </summary>
        /// <param name="roleId">角色编号。</param>
        /// <param name="editRoleModel">编辑角色模型。</param>
        /// <returns>编辑角色的操作状态。</returns>
        EditRoleStatus EditRole(int roleId, EditRoleModel editRoleModel);

        /// <summary>
        /// 角色授权。
        /// </summary>
        /// <param name="authorizeRoleModel">授权角色模型。</param>
        /// <returns>授权角色的操作状态。</returns>
        AuthorizeRoleStatus RoleAuthorize(AuthorizeRoleModel authorizeRoleModel);

        /// <summary>
        /// 获得权限集合。
        /// </summary>
        /// <returns>权限集合。</returns>
        IEnumerable<Permission> GetPermissions();

        /// <summary>
        /// 根据角色编号获得角色信息。
        /// </summary>
        /// <param name="roleId">角色编号。</param>
        /// <returns>角色信息。</returns>
        Role GetRole(int roleId);

        /// <summary>
        /// 获得角色信息分页列表。
        /// </summary>
        /// <param name="pagerParameters">分页参数。</param>
        /// <returns>角色信息分页列表。</returns>
        IPageOfItems<ViewRole> GetRoles(PagerParameters pagerParameters);
    }
}
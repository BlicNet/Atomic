﻿namespace Atomic.Plugins.Users.Application
{
    using Atomic.Collections;
    using Atomic.Core.Security;
    using Atomic.Plugins.Users.Models;

    /// <summary>
    /// 用户管理服务。
    /// </summary>
    public interface IUserAdminService
    {
        /// <summary>
        /// 获得用户信息分页列表。
        /// </summary>
        /// <param name="pager">分页参数。</param>
        /// <returns>用户信息分页列表。</returns>
        IPageOfItems<ViewUser> GetUsers(PagerParameters pager);
    }
}
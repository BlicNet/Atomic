﻿namespace Atomic.Plugins.Users.Application
{
    using Atomic.Images;
    using Atomic.Core.Security;
    using Atomic.Plugins.Users.Domain;

    /// <summary>
    /// 用户应用服务。
    /// </summary>
    public interface IUserAppService
    {
        /// <summary>
        /// 获得用户详细信息。
        /// </summary>
        /// <param name="userId">用户编号。</param>
        /// <returns>用户详细信息。</returns>
        UserDescriptor GetUser(int userId);

        /// <summary>
        /// 获得用户详细信息。
        /// </summary>
        /// <param name="accountNumber">用户怅号。</param>
        /// <returns>用户详细信息。</returns>
        UserDescriptor GetUser(string accountNumber);

        /// <summary>
        /// 注册新用户。
        /// </summary>
        /// <param name="registerNewUserModel">新用户注册信息。</param>
        /// <returns>是否注册成功。</returns>
        bool RegisterNewUser(RegisterNewUserModel registerNewUserModel);

        /// <summary>
        /// 修改用户密码。
        /// </summary>
        /// <param name="changePasswordModel">修改密码信息。</param>
        /// <returns>修改用户密码操作状态。</returns>
        ChangePasswordStatus ChangePassword(ChangePasswordModel changePasswordModel);

        /// <summary>
        /// 上传头像。
        /// </summary>
        /// <param name="userId">用户编号。</param>
        /// <param name="uploadImage">上传图片信息。</param>
        /// <returns>上传头像图片信息。</returns>
        ViewIconImage UploadIcon(int userId, UploadImage uploadImage);

        /// <summary>
        /// 修改头像。
        /// </summary>
        /// <param name="accountNumber">用户账号。</param>
        /// <param name="cutImage">裁剪图片输入信息。</param>
        ChangeIconStatus ChangeIcon(string accountNumber, CutImageModel cutImage);
    }
}
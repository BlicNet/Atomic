﻿namespace Atomic.Plugins.Users.Application
{
    using Atomic.Collections;
    using Atomic.Core.Security;
    using Atomic.Plugins.Users.Data;
    using Atomic.Plugins.Users.Models;

    /// <summary>
    /// 用户管理服务。
    /// </summary>
    public class UserAdminService : IUserAdminService
    {
        /// <summary>
        /// 用户管理数据访问器。
        /// </summary>
        private readonly IUserAdminRepository _userAdminRepository = null;

        /// <summary>
        /// 初始化用户管理服务。
        /// </summary>
        /// <param name="userAdminRepository">用户管理数据访问器。</param>
        public UserAdminService(IUserAdminRepository userAdminRepository)
        {
            this._userAdminRepository = userAdminRepository;
        }

        /// <summary>
        /// 获得用户信息分页列表。
        /// </summary>
        /// <param name="pager">分页参数。</param>
        /// <returns>用户信息分页列表。</returns>
        public IPageOfItems<ViewUser> GetUsers(PagerParameters pager)
        {
            return this._userAdminRepository.GetUsers(pager.Index, pager.Size);
        }
    }
}
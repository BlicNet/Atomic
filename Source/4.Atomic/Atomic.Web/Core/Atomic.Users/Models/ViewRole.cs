﻿namespace Atomic.Plugins.Users.Models
{
    /// <summary>
    /// 显示角色信息。
    /// </summary>
    public class ViewRole
    {
        /// <summary>
        /// 唯一标识。
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 唯一名称。
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 显示名称。
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 角色类型。
        /// </summary>
        public string Type { get; set; }
    }
}
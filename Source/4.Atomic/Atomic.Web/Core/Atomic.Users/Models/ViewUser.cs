﻿namespace Atomic.Plugins.Users.Models
{
    /// <summary>
    /// 用户显示模型。
    /// </summary>
    public class ViewUser
    {
        /// <summary>
        /// 编号。
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 账号。
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// 名称。
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 邮箱。
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 是否可用。
        /// </summary>
        public bool Disable { get; set; }
    }
}
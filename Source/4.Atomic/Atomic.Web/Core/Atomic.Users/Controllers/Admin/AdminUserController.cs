﻿namespace Atomic.Plugins.Users.Controllers.Admin
{
    using System.Web.Mvc;

    using Atomic.Collections;
    using Atomic.Logging;
    using Atomic.Modules.Security;

    using Atomic.Mvc.Filters;
    using Atomic.Mvc.Settings;

    using Atomic.Core.Security;
    
    using Atomic.Plugins.Users.Application;

    /// <summary>
    /// 用户管理后台控制器。
    /// </summary>
    [Login]
    [AtomicAuthorize(SystemPermissions.IsLoginAdmin)]
    [Layout("_AdminLayout")]
    public class AdminUserController : Controller
    {
        #region 字段

        /// <summary>
        /// 站点信息。
        /// </summary>
        private readonly ISite _site = null;

        /// <summary>
        /// 用户管理服务。
        /// </summary>
        private readonly IUserAdminService _userAdminService = null;

        #endregion

        #region 初始化

        /// <summary>
        /// 初始化用户管理后台控制器。
        /// </summary>
        /// <param name="userAdminService">用户管理服务。</param>
        public AdminUserController(IUserAdminService userAdminService)
        {
            this._userAdminService = userAdminService;

            this._site = SiteFactory.Current;
            this.Logger = LoggerFactory.Current.Create();
        }

        #endregion

        #region 日志

        /// <summary>
        /// 日志工具。
        /// </summary>
        private ILogger Logger { get; set; }

        #endregion

        #region 首页

        /// <summary>
        /// 首页。
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region 用户列表

        /// <summary>
        /// 用户列表。
        /// </summary>
        /// <param name="pageIndex">页码。</param>
        /// <returns></returns>
        public ActionResult List(int pageIndex)
        {
            var items = this._userAdminService.GetUsers(new PagerParameters(index: pageIndex, size: this._site.PageSize));

            return View(items);
        }

        #endregion
    }
}
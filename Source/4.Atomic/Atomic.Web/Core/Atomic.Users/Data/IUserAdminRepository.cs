﻿namespace Atomic.Plugins.Users.Data
{
    using Atomic.Collections;
    using Atomic.Core.Security;
    using Atomic.Plugins.Users.Models;

    /// <summary>
    /// 用户管理数据访问器接口。
    /// </summary>
    public interface IUserAdminRepository
    {
        /// <summary>
        /// 获得用户信息分页列表。
        /// </summary>
        /// <param name="pageIndex">页码。</param>
        /// <param name="pageSize">页大小。</param>
        /// <returns>用户信息分页列表。</returns>
        IPageOfItems<ViewUser> GetUsers(int pageIndex, int pageSize);
    }
}
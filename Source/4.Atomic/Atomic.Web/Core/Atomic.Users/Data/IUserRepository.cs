﻿namespace Atomic.Plugins.Users.Data
{
    using Atomic.Modules.Security;
    using Atomic.Core.Security;

    /// <summary>
    /// 用户数据访问器接口。
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// 创建一个用户。
        /// </summary>
        /// <param name="user">用户信息。</param>
        /// <param name="roles">角色编号列表。</param>
        /// <returns>是否成功。</returns>
        bool Create(IUser user, string roles);

        /// <summary>
        /// 是否存在该账号。
        /// </summary>
        /// <param name="accountNumber">账号。</param>
        /// <returns>存在返回 true，否则返回 false。</returns>
        bool IsExistAccount(string accountNumber);

        /// <summary>
        /// 是否存在该名称。
        /// </summary>
        /// <param name="name">用户名称。</param>
        /// <returns>存在返回 true，否则返回 false。</returns>
        bool IsExistName(string name);

        /// <summary>
        /// 是否已使用过该邮箱。
        /// </summary>
        /// <param name="email">邮箱地址。</param>
        /// <returns>使用过返回 true，否则返回 false。</returns>
        bool IsUseEmail(string email);

        /// <summary>
        /// 是否存在用户。
        /// </summary>
        /// <param name="id">用户标识。</param>
        /// <returns>存在返回 true，否则返回 false。</returns>
        bool IsExistUser(long id);

        /// <summary>
        /// 修改密码。
        /// </summary>
        /// <param name="id">用户标识。</param>
        /// <param name="password">新密码。</param>
        /// <returns>是否成功。</returns>
        bool ChangePassword(long id, string password);

        /// <summary>
        /// 获得用户详细信息。
        /// </summary>
        /// <param name="userId">用户编号。</param>
        /// <returns>用户详细信息。</returns>
        UserDescriptor GetUser(int userId);

        /// <summary>
        /// 获得用户详细信息。
        /// </summary>
        /// <param name="accountNumber">用户怅号。</param>
        /// <returns>用户详细信息。</returns>
        UserDescriptor GetUser(string accountNumber);

        /// <summary>
        /// 修改头像。
        /// </summary>
        /// <param name="userId">用户编号。</param>
        /// <param name="image">头像后缀名。</param>
        /// <returns>是否成功。</returns>
        bool ChangeIcon(int userId, string image);
    }
}
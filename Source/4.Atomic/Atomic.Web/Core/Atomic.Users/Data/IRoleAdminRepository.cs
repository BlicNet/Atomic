﻿namespace Atomic.Plugins.Users.Data
{
    using System.Collections.Generic;
    using Atomic.Collections;
    using Atomic.Modules.Security;
    using Atomic.Plugins.Users.Models;
    using Atomic.Plugins.Users.Domain.Admin;

    /// <summary>
    /// 角色管理数据访问器接口。
    /// </summary>
    public interface IRoleAdminRepository
    {
        /// <summary>
        /// 判断角色是否存在。
        /// </summary>
        /// <param name="roleId">角色编号。</param>
        /// <returns>已存在则返回 true，否则返回 false。</returns>
        bool ExistRoleId(int roleId);

        /// <summary>
        /// 判断角色名称是否存在。
        /// </summary>
        /// <param name="roleName">角色名称。</param>
        /// <returns>已存在则返回 true，否则返回 false。</returns>
        bool ExistRoleName(string roleName);

        /// <summary>
        /// 判断角色名称是否存在（除指定的角色编号外）。
        /// </summary>
        /// <param name="roleName">角色编号。</param>
        /// <param name="roleName">角色名称。</param>
        /// <returns>已存在则返回 true，否则返回 false。</returns>
        bool ExistRoleName(int roleId, string roleName);

        /// <summary>
        /// 增加角色。
        /// </summary>
        /// <param name="addRoleModel">增加角色模型。</param>
        /// <returns>操作成功返回 true，否则返回 false。</returns>
        bool AddRole(AddRoleModel addRoleModel);

        /// <summary>
        /// 编辑角色。
        /// </summary>
        /// <param name="roleId">角色编号。</param>
        /// <param name="editRoleModel">增加角色模型。</param>
        /// <returns>操作成功返回 true，否则返回 false。</returns>
        bool EditRole(int roleId, EditRoleModel editRoleModel);

        /// <summary>
        /// 授权角色。
        /// </summary>
        /// <param name="roleId">角色编号。</param>
        /// <param name="permissions">权限集合。</param>
        /// <returns>操作成功返回 true，否则返回 false。</returns>
        bool RoleAuthorize(int roleId, IEnumerable<Permission> permissions);

        /// <summary>
        /// 获得角色信息分页列表。
        /// </summary>
        /// <param name="pageIndex">页码。</param>
        /// <param name="pageSize">页大小。</param>
        /// <returns>角色信息分页列表。</returns>
        IPageOfItems<ViewRole> GetRoles(int pageIndex, int pageSize);
    }
}
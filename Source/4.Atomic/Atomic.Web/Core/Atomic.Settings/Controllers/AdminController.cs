﻿namespace Atomic.Plugins.Settings.Controllers
{
    using System.Web.Mvc;

    using Atomic.Logging;
    using Atomic.Modules.Security;

    using Atomic.Mvc.Filters;
    using Atomic.Mvc.Settings;
    using Atomic.Core.Security;

    using Atomic.Plugins.Settings.Application;

    /// <summary>
    /// 管理后台控制器。
    /// </summary>
    [Login]
    [AtomicAuthorize(SystemPermissions.IsLoginAdmin)]
    [Layout("_AdminLayout")]
    public class AdminController : Controller
    {
        /// <summary>
        /// 站点应用服务。
        /// </summary>
        private readonly ISiteAppService _siteAppService = null;

        /// <summary>
        /// 初始化管理后台控制器。
        /// </summary>
        /// <param name="siteAppService">站点应用服务。</param>
        public AdminController(ISiteAppService siteAppService)
        {
            this._siteAppService = siteAppService;

            this.Logger = LoggerFactory.Current.Create();
        }

        /// <summary>
        /// 日志工具。
        /// </summary>
        private ILogger Logger { get; set; }

        /// <summary>
        /// 首页。
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        #region 站点信息配置

        /// <summary>
        /// 站点信息配置。
        /// </summary>
        /// <returns></returns>
        public ActionResult Settings()
        {
            var site = SiteFactory.Current as SafeModeSite;

            return View(site);
        }

        /// <summary>
        /// 站点信息配置。
        /// </summary>
        /// <param name="site">配置信息。</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Settings(SafeModeSite site)
        {
            if (ModelState.IsValid)
            {
                var result = this._siteAppService.SaveSite(site);

                if (result)
                {
                    this.Logger.LogInfo("管理员({0})修改了站点信息配置。", User.Identity.Name);

                    return View("Success");
                }
            }

            return View(site);
        }

        #endregion
    }
}
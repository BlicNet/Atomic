﻿namespace Atomic.Plugins.Settings.Controllers
{
    using System.Web.Mvc;

    using Atomic.Mvc.Settings;
    using Atomic.Mvc.Filters;

    [Layout]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Name = SiteFactory.Current.Name;
            return View();
        }
    }
}
﻿namespace Atomic.Plugins.Settings.Repositories
{
    using Atomic.Mvc.Settings;

    /// <summary>
    /// 站点信息配置访问器接口。
    /// </summary>
    public interface ISiteRepository
    {
        /// <summary>
        /// 获得站点信息。
        /// </summary>
        /// <returns>站点信息。</returns>
        ISite Get();

        /// <summary>
        /// 保存站点信息。
        /// </summary>
        /// <param name="site">站点信息。</param>
        /// <returns>操作是否成功。</returns>
        bool Save(ISite site);
    }
}
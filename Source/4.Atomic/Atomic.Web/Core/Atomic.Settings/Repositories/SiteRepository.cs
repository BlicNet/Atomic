﻿namespace Atomic.Plugins.Settings.Repositories
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.SqlClient;

    using Atomic.Extensions;
    using Atomic.Data.AdoNet;
    using Atomic.Mvc.Settings;

    public class SiteRepository : RepositoryBase, ISiteRepository
    {
        /// <summary>
        /// 表名。
        /// </summary>
        /// <remarks>
        /// Name:对应实体属性名。
        /// Value:对应实体属性值。
        /// </remarks>
        private const string TABLE_NAME = "atomic_sys_settings";

        /// <summary>
        /// 获得站点信息。
        /// </summary>
        /// <returns>站点信息。</returns>
        public ISite Get()
        {
            ISite site = new SafeModeSite();

            string sql = "SELECT Name, Value FROM " + TABLE_NAME;

            using (IDataReader dataReader = this.DataProvider.SqlQuery(sql))
            {
                while (dataReader.Read())
                {
                    string name = dataReader.GetString(0);
                    string value = dataReader.GetString(1);

                    if (name.Equals("Name", StringComparison.OrdinalIgnoreCase))
                    {
                        site.Name = string.IsNullOrWhiteSpace(value) ? "Atomic" : value;
                    }

                    if (name.Equals("Theme", StringComparison.OrdinalIgnoreCase))
                    {
                        site.Theme = string.IsNullOrWhiteSpace(value) ? "Default" : value;
                    }

                    if (name.Equals("PageSize", StringComparison.OrdinalIgnoreCase))
                    {
                        site.PageSize = value.IsNumeric() ? int.Parse(value) : 10;
                    }

                    if (name.Equals("HomePage", StringComparison.OrdinalIgnoreCase))
                    {
                        site.HomePage = string.IsNullOrWhiteSpace(value) ? "/" : value;
                    }

                    if (name.Equals("LoginPage", StringComparison.OrdinalIgnoreCase))
                    {
                        site.LoginPage = string.IsNullOrWhiteSpace(value) ? "/account/login" : value;
                    }

                    if (name.Equals("Error403Page", StringComparison.OrdinalIgnoreCase))
                    {
                        site.Error403Page = string.IsNullOrWhiteSpace(value) ? "/error/403" : value;
                    }

                    if (name.Equals("Error404Page", StringComparison.OrdinalIgnoreCase))
                    {
                        site.Error404Page = string.IsNullOrWhiteSpace(value) ? "/error/404" : value;
                    }

                    if (name.Equals("Error500Page", StringComparison.OrdinalIgnoreCase))
                    {
                        site.Error500Page = string.IsNullOrWhiteSpace(value) ? "/error/500" : value;
                    }

                    if (name.Equals("ImageUrl", StringComparison.OrdinalIgnoreCase))
                    {
                        site.ImageUrl = string.IsNullOrWhiteSpace(value) ? "/" : value;
                    }

                    if (name.Equals("IconDefaultUrl", StringComparison.OrdinalIgnoreCase))
                    {
                        site.IconDefaultUrl = string.IsNullOrWhiteSpace(value) ? "/Content/Default/Icon.jpg" : value;
                    }

                    if (name.Equals("IconAreaWidth", StringComparison.OrdinalIgnoreCase))
                    {
                        site.IconAreaWidth = value.IsNumeric() ? int.Parse(value) : 400;
                    }

                    if (name.Equals("IconAreaHeight", StringComparison.OrdinalIgnoreCase))
                    {
                        site.IconAreaHeight = value.IsNumeric() ? int.Parse(value) : 300;
                    }

                    if (name.Equals("SaveIconPath", StringComparison.OrdinalIgnoreCase))
                    {
                        site.SaveIconPath = string.IsNullOrWhiteSpace(value) ? string.Empty : value;
                    }
                }
            }

            return site;
        }

        /// <summary>
        /// 保存站点信息。
        /// </summary>
        /// <param name="site">站点信息。</param>
        /// <returns>操作是否成功。</returns>
        public bool Save(ISite site)
        {
            StringBuilder sqlBuilder = new StringBuilder();

            sqlBuilder.AppendLine("UPDATE " + TABLE_NAME + " SET Value = @Name WHERE Name = 'Name';");
            sqlBuilder.AppendLine("UPDATE " + TABLE_NAME + " SET Value = @Theme WHERE Name = 'Theme';");
            sqlBuilder.AppendLine("UPDATE " + TABLE_NAME + " SET Value = @PageSize WHERE Name = 'PageSize';");
            sqlBuilder.AppendLine("UPDATE " + TABLE_NAME + " SET Value = @HomePage WHERE Name = 'HomePage';");
            sqlBuilder.AppendLine("UPDATE " + TABLE_NAME + " SET Value = @LoginPage WHERE Name = 'LoginPage';");
            sqlBuilder.AppendLine("UPDATE " + TABLE_NAME + " SET Value = @Error403Page WHERE Name = 'Error403Page';");
            sqlBuilder.AppendLine("UPDATE " + TABLE_NAME + " SET Value = @Error404Page WHERE Name = 'Error404Page';");
            sqlBuilder.AppendLine("UPDATE " + TABLE_NAME + " SET Value = @Error500Page WHERE Name = 'Error500Page';");

            IDataParameter[] parameters = 
                    { 
                        new SqlParameter("@Name", SqlDbType.NVarChar, 200),
                        new SqlParameter("@Theme", SqlDbType.NVarChar, 200),
                        new SqlParameter("@PageSize", SqlDbType.NVarChar, 200),
                        new SqlParameter("@HomePage", SqlDbType.NVarChar, 200),
                        new SqlParameter("@LoginPage", SqlDbType.NVarChar, 200),
                        new SqlParameter("@Error403Page", SqlDbType.NVarChar, 200),
                        new SqlParameter("@Error404Page", SqlDbType.NVarChar, 200),
                        new SqlParameter("@Error500Page", SqlDbType.NVarChar, 200),
                    };

            parameters[0].Value = site.Name ?? string.Empty;
            parameters[1].Value = site.Theme ?? "Default";
            parameters[2].Value = site.PageSize.ToString();
            parameters[3].Value = site.HomePage ?? "/";
            parameters[4].Value = site.LoginPage ?? "/account/login";
            parameters[5].Value = site.Error403Page ?? "/error/403";
            parameters[6].Value = site.Error404Page ?? "/error/404";
            parameters[7].Value = site.Error500Page ?? "/error/500";

            return this.DataProvider.ExecuteSqlCommand(sqlBuilder.ToString(), parameters) > 0;
        }
    }
}
﻿namespace Atomic.Plugins.Settings
{
    using System.Collections.Generic;

    using Atomic.Mvc.Routes;

    /// <summary>
    /// 注册路由规则。
    /// </summary>
    public class Routes : IRouteProvider
    {
        public IEnumerable<RouteDescriptor> GetRoutes()
        {
            return new RouteDescriptor[]
            {
                new RouteDescriptor
                {
                    Name = "admin.settings",
                    Url = "admin/settings",
                    Defaults = new { controller = "Admin", action = "Settings" },
                    Namespaces = new[] { "Atomic.Plugins.Settings.Controllers" },
                },
                new RouteDescriptor
                {
                    Name = "admin",
                    Url = "admin",
                    Defaults = new { controller = "Admin", action = "Index" },
                    Namespaces = new[] { "Atomic.Plugins.Settings.Controllers" },
                },
                new RouteDescriptor
                {
                    Name = "home.default",
                    Url = "",
                    Defaults = new { controller = "Home", action = "Index" },
                    Namespaces = new[] { "Atomic.Plugins.Settings.Controllers" },
                }
            };
        }
    }
}
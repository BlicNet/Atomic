﻿namespace Atomic.Plugins.Settings
{
    using System.Collections.Generic;

    using Atomic.Mvc.ViewEngines;
    using Atomic.Mvc.Themes;

    /// <summary>
    /// 初始化信息。
    /// </summary>
    public class Layouts : ILayoutProvider
    {
        /// <summary>
        /// 获得布局信息集合。
        /// </summary>
        /// <returns>布局信息集合。</returns>
        public IEnumerable<LayoutDescriptor> GetLayouts()
        {
            return new LayoutDescriptor[]
            {
                new LayoutDescriptor("_Layout", "Atomic.Settings", "Core", "~/Views/Shared/{0}.cshtml", true),
                new LayoutDescriptor("_AdminLayout", "Atomic.Settings", "Core", "~/Views/Shared/{0}.cshtml", true)
            };
        }
    }
}
﻿namespace Atomic.Plugins.Settings.Application
{
    using System;
    using System.Transactions;

    using Atomic.Mvc.Settings;
    using Atomic.Plugins.Settings.Repositories;

    /// <summary>
    /// 站点应用服务。
    /// </summary>
    public class SiteAppService : ISiteAppService
    {
        /// <summary>
        /// 站点信息配置访问器。
        /// </summary>
        private readonly ISiteRepository _siteRepository = null;

        /// <summary>
        /// 初始化站点应用服务。
        /// </summary>
        /// <param name="siteRepository">站点信息配置访问器。</param>
        public SiteAppService(ISiteRepository siteRepository)
        {
            this._siteRepository = siteRepository;
        }

        /// <summary>
        /// 保存站点配置。
        /// </summary>
        /// <param name="site">站点信息。</param>
        /// <returns>操作是否成功。</returns>
        public bool SaveSite(ISite site)
        {
            if (site == null)
            {
                throw new ArgumentNullException("site", "站点信息不可以是空的。");
            }

            using (TransactionScope unitOfWork = new TransactionScope())
            {
                var result = this._siteRepository.Save(site);

                if (result)
                {
                    unitOfWork.Complete();
                }

                return result;
            }
        }
    }
}
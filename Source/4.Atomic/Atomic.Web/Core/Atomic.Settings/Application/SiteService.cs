﻿namespace Atomic.Plugins.Settings.Application
{
    using Atomic.Mvc.Settings;

    using Atomic.Plugins.Settings.Repositories;

    /// <summary>
    /// 站点服务。
    /// </summary>
    public class SiteService : ISiteService
    {
        /// <summary>
        /// 站点信息配置访问器。
        /// </summary>
        private readonly ISiteRepository _siteRepository = null;

        /// <summary>
        /// 初始化站点服务。
        /// </summary>
        /// <param name="siteRepository">站点信息配置访问器。</param>
        public SiteService(ISiteRepository siteRepository)
        {
            this._siteRepository = siteRepository;
        }

        /// <summary>
        /// 获得站点信息。
        /// </summary>
        /// <returns>站点信息。</returns>
        public ISite Get()
        {
            return this._siteRepository.Get();
        }
    }
}
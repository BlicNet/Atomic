﻿namespace Atomic.Plugins.Settings.Application
{
    using Atomic.Mvc.Settings;

    /// <summary>
    /// 站点应用服务。
    /// </summary>
    public interface ISiteAppService
    {
        /// <summary>
        /// 保存站点配置。
        /// </summary>
        /// <param name="site">站点信息。</param>
        /// <returns>操作是否成功。</returns>
        bool SaveSite(ISite site);
    }
}
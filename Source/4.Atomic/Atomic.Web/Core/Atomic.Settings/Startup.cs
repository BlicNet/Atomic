﻿namespace Atomic.Plugins.Settings
{
    using Atomic.Mvc;
    using Atomic.Mvc.Settings;

    /// <summary>
    /// 初始化插件。
    /// </summary>
    public class Startup : ISetupPlugin
    {
        /// <summary>
        /// 初始化插件信息。
        /// </summary>
        public void Initialize()
        {
            SiteFactory.SetSiteService(AtomicContainer.Resolve<ISiteService>());
        }
    }
}
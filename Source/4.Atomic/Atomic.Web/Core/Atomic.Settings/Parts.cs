﻿namespace Atomic.Plugins.Settings
{
    using System.Collections.Generic;

    using Atomic.Mvc.Themes;

    public class Parts : IPartProvider
    {
        public IEnumerable<IPart> GetParts()
        {
            return new IPart[]
            {
                new Part("_Login", "Atomic.Settings", "Core", "~/Themes/Default/Views/Shared/_Login.cshtml")
            };
        }
    }
}
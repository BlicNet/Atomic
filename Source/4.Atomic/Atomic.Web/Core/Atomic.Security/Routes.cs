﻿namespace Atomic.Plugins.Security
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    using Atomic.Mvc.Routes;

    /// <summary>
    /// 注册路由规则。
    /// </summary>
    public class Routes : IRouteProvider
    {
        /// <summary>
        /// 获取路由规则。
        /// </summary>
        /// <returns>路由规则描述集合。</returns>
        public IEnumerable<RouteDescriptor> GetRoutes()
        {
            return new RouteDescriptor[]
            {
                new RouteDescriptor
                {
                    Name = "account.login",
                    Url = "account/login",
                    Defaults = new { controller = "Account", action = "Login" },
                    Namespaces = new[] { "Atomic.Users.Controllers" }
                },
                new RouteDescriptor
                {
                    Name = "account.logOff",
                    Url = "account/logoff",
                    Defaults = new { controller = "Account", action = "LogOff" },
                    Namespaces = new[] { "Atomic.Users.Controllers" }
                }
            };
        }
    }
}
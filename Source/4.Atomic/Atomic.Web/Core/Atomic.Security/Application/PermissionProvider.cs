﻿namespace Atomic.Plugins.Security.Application
{
    using System.Collections.Generic;

    using Atomic.Modules.Security;
    using Atomic.Plugins.Security.Repositories;

    /// <summary>
    /// 权限提供程序。
    /// </summary>
    public class PermissionProvider : IPermissionProvider
    {
        /// <summary>
        /// 权限信息访问器。
        /// </summary>
        private readonly IPermissionRepository _permissionRepository = null;

        /// <summary>
        /// 初始化权限提供程序。
        /// </summary>
        /// <param name="permissionRepository">权限信息访问器。</param>
        public PermissionProvider(IPermissionRepository permissionRepository)
        {
            this._permissionRepository = permissionRepository;
        }

        /// <summary>
        /// 获得权限集合。
        /// </summary>
        /// <returns>权限集合。</returns>
        public IEnumerable<Permission> GetPermissions()
        {
            return this._permissionRepository.GetPermissions();
        }

        /// <summary>
        /// 根据角色编号获得角色信息。
        /// </summary>
        /// <param name="roleId">角色编号。</param>
        /// <returns>角色信息。</returns>
        public Role GetRole(int roleId)
        {
            return this._permissionRepository.GetRole(roleId);
        }

        /// <summary>
        /// 获得所有角色。
        /// </summary>
        /// <returns>所有角色信息集合。</returns>
        public IEnumerable<Role> GetRoles()
        {
            return this._permissionRepository.GetRoles();
        }
    }
}
﻿namespace Atomic.Plugins.Security.Application
{
    using Atomic.Plugins.Security.Domain;

    /// <summary>
    /// 账户应用服务。
    /// </summary>
    public interface IAccountAppService
    {
        /// <summary>
        /// 登录用户。
        /// </summary>
        /// <param name="accountNumber">账号。</param>
        /// <param name="password">密码。</param>
        /// <returns>操作状态。</returns>
        LoginUserStatus Login(string accountNumber, string password);

        /// <summary>
        /// 注销用户。
        /// </summary>
        void Logout();
    }
}
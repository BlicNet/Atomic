﻿namespace Atomic.Plugins.Security.Application
{
    using Atomic.Modules.Security;
    using Atomic.Plugins.Security.Repositories;

    /// <summary>
    /// 会员服务。
    /// </summary>
    public class MembershipService : IMembershipService
    {
        /// <summary>
        /// 用户信息访问器。
        /// </summary>
        private readonly IUserRepository _userRepository = null;

        /// <summary>
        /// 权限信息访问器。
        /// </summary>
        private readonly IPermissionRepository _permissionRepository = null;

        /// <summary>
        /// 初始化会员服务。
        /// </summary>
        /// <param name="userRepository">用户信息访问器。</param>
        /// <param name="permissionRepository">权限信息访问器。</param>
        public MembershipService(IUserRepository userRepository, IPermissionRepository permissionRepository)
        {
            this._userRepository = userRepository;
            this._permissionRepository = permissionRepository;
        }

        /// <summary>
        /// 获得匿名用户信息。
        /// </summary>
        /// <returns>匿名用户信息。</returns>
        public AnonymousUser GetAnonymousUser()
        {
            var role = this._permissionRepository.GetRole((int)SystemRoles.Anonymous);

            return new AnonymousUser(role);
        }

        /// <summary>
        /// 根据账号获得用户信息。
        /// </summary>
        /// <param name="account">账号。</param>
        /// <returns>用户信息。</returns>
        public User GetUserByAccountNumber(string account)
        {
            return this._userRepository.GetUserByAccountNumber(account);
        }

        /// <summary>
        /// 根据用户编号获得用户信息。
        /// </summary>
        /// <param name="id">用户编号。</param>
        /// <returns>用户信息。</returns>
        public IUser GetUser(long id)
        {
            return this._userRepository.GetUser(id);
        }
    }
}
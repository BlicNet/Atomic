﻿namespace Atomic.Plugins.Security.Validators
{
    using Atomic.Extensions;
    using Atomic.Mvc.Validators;

    using Atomic.Plugins.Security.Domain;

    public class LoginUserModelValidator : EntityValidatorBase<LoginUserModel>
    {
        public override void OnValidate(LoginUserModel loginUserModel)
        {
            if (loginUserModel == null)
            {
                base.AddValidatorError("loginUserModel.null", string.Empty, "提交登录信息失败。");
            }

            if (string.IsNullOrWhiteSpace(loginUserModel.AccountNumber) || !loginUserModel.AccountNumber.Range(4, 20))
            {
                this.AddValidatorError("loginUserModel.accountNumber.length", "AccountNumber", "账号的字符数必须在 4 - 20 个之间。");
            }

            if (!loginUserModel.AccountNumber.IsMatch(@"^(\d|[_a-zA-Z0-9])*$"))
            {
                this.AddValidatorError("loginUserModel.accountNumber.string", "AccountNumber", "账号必须是数字、字母和下划线的组合。");
            }

            if (string.IsNullOrWhiteSpace(loginUserModel.Password) || !loginUserModel.Password.Range(6, 20))
            {
                this.AddValidatorError("loginUserModel.password.length", "Password", "密码的字符数必须在 6 - 20 个之间。");
            }

            if (!loginUserModel.Password.IsMatch(@"^(\d|[_a-zA-Z0-9])*$"))
            {
                this.AddValidatorError("loginUserModel.password.string", "Password", "密码必须是数字、字母和下划线的组合。");
            }
        }
    }
}
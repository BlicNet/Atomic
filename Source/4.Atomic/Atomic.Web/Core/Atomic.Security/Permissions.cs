﻿namespace Atomic.Plugins.Security
{
    using System.Collections.Generic;

    using Atomic.Core.Security;

    /// <summary>
    /// 账户权限信息。
    /// </summary>
    public class Permissions
    {
        /// <summary>
        /// 是否允许用户登录。
        /// </summary>
        public const string User_SignIn = "User_SignIn";
    }
}
﻿namespace Atomic.Plugins.Security.Domain
{
    /// <summary>
    /// 登录信息。
    /// </summary>
    public class LoginUserModel
    {
        /// <summary>
        /// 账号。
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// 密码。
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 登录操作状态。
        /// </summary>
        public LoginUserStatus Status { get; set; }
    }
}
﻿namespace Atomic.Plugins.Security.Domain
{
    /// <summary>
    /// 登录状态。
    /// </summary>
    public enum LoginUserStatus
    {
        /// <summary>
        /// 成功。
        /// </summary>
        Success = 1,

        /// <summary>
        /// 失败。
        /// </summary>
        Error = 0,

        /// <summary>
        /// 用户信息不存在。
        /// </summary>
        UserInvalid = -1
    }
}
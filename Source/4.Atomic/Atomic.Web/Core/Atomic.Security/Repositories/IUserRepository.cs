﻿namespace Atomic.Plugins.Security.Repositories
{
    using Atomic.Modules.Security;

    /// <summary>
    /// 用户信息访问器接口。
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// 根据账号获得用户信息。
        /// </summary>
        /// <param name="accountNumber">账号。</param>
        /// <returns>用户信息。</returns>
        User GetUserByAccountNumber(string accountNumber);

        /// <summary>
        /// 根据用户编号获得用户信息。
        /// </summary>
        /// <param name="id">用户编号。</param>
        /// <returns>用户信息。</returns>
        User GetUser(long id);
    }
}
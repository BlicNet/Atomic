﻿namespace Atomic.Web
{
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Autofac;

    using Atomic.Core.Security;

    using Atomic.Mvc;
    using Atomic.Mvc.Settings;

    using Atomic.Modules.Security;

    /// <summary>
    /// Mvc 应用程序。
    /// </summary>
    public class MvcApplication : HttpApplication
    {
        /// <summary>
        /// 应用程序启动。
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            Starter.CreateStarter(RegisterContainer).OnApplicationStart(this);
        }

        /// <summary>
        /// 注册外部信息到容器。
        /// </summary>
        /// <param name="builder">容器生成器。</param>
        private void RegisterContainer(ContainerBuilder builder)
        {
            //注册授权组件。
            builder.RegisterType<AuthenticationService>().As<IAuthenticationService>();
            builder.RegisterType<Authorizer>().As<IAuthorizer>();
        }
    }
}
﻿namespace Atomic.Core.Security
{
    using System.Web.Mvc;

    using Atomic.Mvc.Settings;

    /// <summary>
    /// 登录过滤器。
    /// </summary>
    public class LoginAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// 认证登录，未登录则跳转到登录页面。
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult(SiteFactory.Current.LoginPage);
            }
        }
    }
}
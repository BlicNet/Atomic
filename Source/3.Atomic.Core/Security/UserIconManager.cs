﻿namespace Atomic.Core.Security
{
    using System.IO;
    using Atomic.Mvc.Settings;

    public class UserIconManager
    {
        private static readonly UserIconManager _current = new UserIconManager();

        public static UserIconManager Current
        {
            get
            {
                return _current;
            }
        }

        /// <summary>
        /// 获得用户头像。
        /// </summary>
        /// <param name="userId">用户编号。</param>
        /// <param name="fileName">头像名称（带后缀）。</param>
        /// <param name="size">头像尺寸。</param>
        /// <returns></returns>
        public string GetIcon(int userId, string fileName, IconSize size)
        {
            string image = string.Empty;
            string icon = string.Empty;
            string uid = userId.ToString();

            int len = uid.Length;
            if (len < 11)
            {
                for (int i = 0; i < 11 - len; i++)
                {
                    uid = "0" + uid;
                }
            }

            uid = uid.Substring(0, 3) + "/" + uid.Substring(3, 3) + "/" + uid.Substring(6, 3) + "/" + uid.Substring(9, 2);

            image = SiteFactory.Current.IconDefaultUrl.EndsWith("/") ? SiteFactory.Current.IconDefaultUrl + uid + "/" : SiteFactory.Current.IconDefaultUrl + "/" + uid + "/";

            string ext = Path.GetExtension(fileName);

            switch (size)
            {
                case IconSize.Normal:
                    icon = userId.ToString() + ext;
                    break;
                case IconSize.Big:
                    icon = userId.ToString() + "_" + ((int)IconSize.Big) + ext;
                    break;
                case IconSize.Small:
                    icon = userId.ToString() + "_" + ((int)IconSize.Small) + ext;
                    break;
                default:
                    icon = userId.ToString() + "_" + ((int)IconSize.Big) + ext;
                    break;
            }

            return image + icon;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetSaveDirectory(int userId)
        {
            string uid = userId.ToString();
            int len = uid.Length;
            if (len < 11)
            {
                for (int i = 0; i < 11 - len; i++)
                {
                    uid = "0" + uid;
                }
            }

            uid = uid.Substring(0, 3) + "\\" + uid.Substring(3, 3) + "\\" + uid.Substring(6, 3) + "\\" + uid.Substring(9, 2);

            return SiteFactory.Current.SaveIconPath.EndsWith("\\") ? SiteFactory.Current.SaveIconPath + uid + "\\" : SiteFactory.Current.SaveIconPath + "\\" + uid + "\\";
        }
    }

    /// <summary>
    /// 输出显示上传头像信息。
    /// </summary>
    public class ViewIconImage
    {
        /// <summary>
        /// 文件名（带后缀）。
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 原图片宽度。
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// 原图片高度。
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// 预览图片宽度。
        /// </summary>
        public int ViewWidth { get; set; }

        /// <summary>
        /// 预览图片高度。
        /// </summary>
        public int ViewHeight { get; set; }

        /// <summary>
        /// 原图头像地址。
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// 小图片头像地址。
        /// </summary>
        public string SmallImage { get; set; }

        /// <summary>
        /// 大图片头像地址。
        /// </summary>
        public string BigImage { get; set; }
    }
}
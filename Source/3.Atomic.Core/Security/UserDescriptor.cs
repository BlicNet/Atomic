﻿namespace Atomic.Core.Security
{
    using System;

    using Atomic.Extensions;
    using Atomic.Modules.Security;
    using Atomic.Mvc.Settings;

    /// <summary>
    /// 用户详细描述。
    /// </summary>
    public class UserDescriptor : User
    {
        /// <summary>
        /// 密码保护问题。
        /// </summary>
        private string _passwordQuestion = string.Empty;

        /// <summary>
        /// 密码保护答案。
        /// </summary>
        private string _passwordAnswer = string.Empty;

        /// <summary>
        /// 生日。
        /// </summary>
        private DateTime _birthday = new DateTime(1970, 1, 1);

        /// <summary>
        /// 密码保护问题。
        /// </summary>
        public string PasswordQuestion
        {
            get
            {
                return this._passwordQuestion;
            }
            set
            {
                var passwordQuestion = value;

                if (string.IsNullOrWhiteSpace(passwordQuestion))
                {
                    throw new ArgumentNullException("passwordQuestion", "密码保护问题不可以是空的。");
                }

                this._passwordQuestion = passwordQuestion;
            }
        }

        /// <summary>
        /// 密码保护答案。
        /// </summary>
        public string PasswordAnswer
        {
            get
            {
                return this._passwordAnswer;
            }
            set
            {
                var passwordAnswer = value;

                if(string.IsNullOrWhiteSpace(passwordAnswer))
                {
                    throw new ArgumentNullException("passwordAnswer", "密码保护答案不可以是空的。");
                }

                this._passwordAnswer = passwordAnswer;
            }
        }

        /// <summary>
        /// 头像(后缀)。
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// 生日。
        /// </summary>
        /// <remarks>
        /// 出生日期必须在 1900 年 1 月 1 日以上。
        /// </remarks>
        public DateTime Birthday
        {
            get
            {
                if (this._birthday < (new DateTime(1900, 1, 1)))
                {
                    this._birthday = new DateTime(1970, 1, 1);
                }

                return this._birthday;
            }
            set
            {
                if (value >= (new DateTime(1900, 1, 1)))
                {
                    this._birthday = value;
                }
            }
        }

        /// <summary>
        /// 性别。
        /// </summary>
        public Sex Sex { get; set; }

        /// <summary>
        /// 简介。
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 注册时间。
        /// </summary>
        public DateTime RegisterTime { get; set; }

        /// <summary>
        /// 获得性别。
        /// </summary>
        /// <returns>获得性别文字。</returns>
        public string GetSex()
        {
            string sex = string.Empty;

            switch(this.Sex)
            {
                case Security.Sex.Boy:
                    sex = "男";
                    break;
                case Security.Sex.Girl:
                    sex = "女";
                    break;
                default:
                    sex = "保密";
                    break;
            }

            return sex;
        }

        /// <summary>
        /// 获得用户头像地址。
        /// </summary>
        /// <param name="size">头像尺寸。</param>
        /// <returns>用户头像地址。</returns>
        public string GetIcon(IconSize size)
        {
            return UserIconManager.Current.GetIcon(this.Id, this.Image, size);
        }
    }

    /// <summary>
    /// 性别。
    /// </summary>
    public enum Sex
    {
        /// <summary>
        /// 保密。
        /// </summary>
        Secret = 0,

        /// <summary>
        /// 男。
        /// </summary>
        Boy = 1,

        /// <summary>
        /// 女。
        /// </summary>
        Girl = 2
    }

    /// <summary>
    /// 头像大小。
    /// </summary>
    public enum IconSize
    {
        /// <summary>
        /// 通常。
        /// </summary>
        Normal = 0,

        /// <summary>
        /// 大图。
        /// </summary>
        Big = 110,

        /// <summary>
        /// 缩略图。
        /// </summary>
        Small = 37
    }
}